
# Crossref Tools Collection

An experimental collection of tools for use with Crossref services, purely experimental and not supported.


## Framework
This tool is built using streamlit and is accessible via the URL [https://labs-toolkit.fly.dev/](https://labs-toolkit.fly.dev/).

If running locally then you can clone the repo and then follow the instructions below:

* install the requirements.txt file (you would need pip or something equivalent installed)
* for pip use: `pip install -r requirements.txt`
* navigate to the clone directory on terminal/commandline `streamlit run app.py`
* You should then see it open in a browser with URL `http://localhost:8501` if you have no other streamlit apps running at that time.
* You should also see the link in the terminal window which you can hold &#8984; (CMD) and click to open if it does not automatically open in your browser.
* Or paste that URL into a new browser tab


## Tools

A list of the public tools below and a brief description.  


|- Help File|Explains the functions of each tool in the collection.|
|:----|:----|
|API Query URL Download tool|Use your own API query URL in this tool to be able to download the results to .csv.|
|Invoice Splitter|Tool to split invoices from Crossref into a line and prefix breakdwon there invoices by line and download into CSV|
|Multiple Resolution Tool (Secondary URLs)|A tool to create the XML file to add the secondary URL and label against a DOI|
|Multiple Resolution Tool (Unlock DOIs)|A tool to create the XML file to unlock the DOIs for Multiple Resolution|
|Search on DOIs|From a list of DOIs choose elements from the REST API to be returned and option to download to .csv.|
|Suffix Generator|Generate random suffixes for your DOIs to use in content registration|



