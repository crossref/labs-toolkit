import datetime
import streamlit as st
import pandas as pd


def timestamp_now():
    return "{:%Y-%m-%d}".format(datetime.datetime.now())


def yesterday():
    return "{:%Y-%m-%d}".format(datetime.datetime.now() - datetime.timedelta(days=1))


def year_earlier(date):
    return "{:%Y-%m-%d}".format(date - datetime.timedelta(days=365))


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def credentials():
    return {
        "usr": f"{st.session_state.email}/root",
        "pwd": st.session_state.password,
    }


def load_file(fn):
    with open(fn) as f:
        return f.read()


def split_ids(ids):
    ids = ids.replace("-", "").replace(" ", "").replace(",", " ").split()
    return ids


def results_to_csv(results, columns, index):
    df = pd.DataFrame(results, columns=columns)
    csv_data = df.to_csv(encoding="utf-8", index=index)
    return csv_data


def results_to_csv_from_dict(results, index):
    df = pd.DataFrame.from_dict(results, orient="index")
    df.index.name = index
    csv_data = df.to_csv(encoding="utf-8")
    return csv_data


def headers(app_name):
    return {
        "User-Agent": f"labs-toolkit-{app_name}; mailto=support@crossref.org",
        "Content-Type": "application/x-www-form-urlencoded",
    }
