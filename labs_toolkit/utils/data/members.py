import streamlit as st
import requests
import labs_toolkit.utils.data.two_letter_country_codes as country_codes
import time


def get_members_list_from_api(email="public_user@email.com"):
    print("Fetching members list from API (cached).")
    params = {"rows": "all", "mailto": email}
    r = requests.get("https://api.labs.crossref.org/members", params=params)

    if r.status_code == 200:
        return r.json()["message"]
    else:
        st.error(
            f"The members list is unavailable, returning {r.status_code} error.\n"
            "Some tools might not be available at this time."
        )
        return None


def labs_api_member_list():
    now = time.perf_counter()
    with st.spinner("Retrieving full members list. This might take a moment..."):
        response = get_members_list_from_api(st.session_state.email)
        if response is not None:
            end = time.perf_counter()
            print(f"Time taken: API list: {end-now}")
            return response


def create_country_drop_down(member_details: dict):
    now = time.perf_counter()
    country_drop_down = {}
    for k, v in member_details.items():
        try:
            country_code = v["cr-labs-member-profile"]["shipping-address-country"]
            if not country_code:
                country_code = v["cr-labs-member-profile"]["billing-address-country"]
        except KeyError:
            country_code = "No member profile"
        country = country_codes.countries_and_codes.get(
            country_code, "No member profile"
        )
        if country in country_drop_down:
            country_drop_down[country].append(k)
        else:
            country_drop_down[country] = [k]
    end = time.perf_counter()
    print(f"Time taken: Create country drop down: {end-now}")
    return country_drop_down


def create_member_list_json(item_list):
    return {p["value"]: p["name"] for i in item_list for p in i["prefix"]}


def member_dict(item_list):
    now = time.perf_counter()
    member_list = {}
    for i in item_list:
        member_list[i["id"]] = i
    end = time.perf_counter()
    print(f"Time taken: Member list: {end-now}")
    return member_list


@st.cache_resource(show_spinner=False, ttl=None)
def download_members_list():
    print("Trying to download members list")
    response = labs_api_member_list()
    item_list = response["items"]
    member_list = member_dict(item_list)
    return (
        response["total-results"],
        create_member_list_json(item_list),
        create_country_drop_down(member_list),
        member_list,
    )
