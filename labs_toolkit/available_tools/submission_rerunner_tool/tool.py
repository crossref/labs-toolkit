import streamlit as st
import requests


DETAILS = {
    "tool-name": "Submission Rerunner",
    "description": "Add a list of submission IDs and rerun them via the admin tool.",
}


def rerun_subs(submissions):
    results = st.empty()
    counter = 0
    successes = []
    failures = []
    url = "https://doi.crossref.org/servlet/login"
    s = requests.Session()
    credentials = {
        "usr": f"{st.session_state.email}/root",
        "pwd": st.session_state.password,
        "role": "root",
    }
    s.post(url, data=credentials)
    for submission in submissions:
        counter += 1
        sub_url = "https://doi.crossref.org/servlet/submissionAdmin"
        rerun = s.post(
            sub_url,
            data={
                "sf": "rerun",
                "submissionID": submission,
            },
        )
        if rerun.status_code == 200:
            rerun_status = "Successful"
            successes.append(submission)
        else:
            rerun_status = "Unsuccessful"
            failures.append(submission)
        st.success(f"{submission} rerun: {rerun_status}")
        results.success(
            f"Files rerun: {counter}  \nSuccessful: {len(successes)}\
                    \nFailed: {len(failures)}"
        )


def split_submissions(submissions):
    submissions = submissions.split()
    return submissions


def main():
    tool_body = st.container()
    submission = tool_body.text_area(
        "Enter the submission IDs here", key="submission_ids"
    ).strip()
    if submission:
        submissions = split_submissions(st.session_state.submission_ids)
        rerun_subs(submissions)
