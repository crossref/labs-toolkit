import streamlit as st
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup as bs

DETAILS = {
    "tool-name": "Cited by matches retrieval tool",
    "description": "Return cited by matches from a list of DOIs.",
}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def split_dois(dois):
    dois = dois.split()
    return dois


def format_table(dois):
    df = pd.DataFrame(dois, columns=["DOI"])
    df.set_index("DOI")
    df.index += 1
    return df


def return_doi_list(dois):
    if len(dois) > 100:
        st.info(f"List not shown as over 100 DOIs  \n Total DOIs {len(dois)}")


def headers():
    return {"User-Agent": "labs-toolkit-flt; mailto:support@crossref.org"}


def get_forward_links(dois, role):
    for doi in dois:
        doi_list = []
        url = f"https://doi.crossref.org/servlet/getForwardLinks?usr={st.session_state.email}/{role}&pwd={st.session_state.password}&doi={doi}"
        r = requests.get(url, headers=headers())
        if r.status_code == 200:
            soup = bs(r.text, "html.parser")
            matches = soup.find_all("forward_link")
            for match in matches:
                for d in match.find_all("doi"):
                    doi_list.append(d.text)
            with st.expander(f"{doi} -- Matches = {len(matches)}"):
                st.table(format_table(doi_list))

        elif r.status_code == 401:
            st.warning(
                f"Username **{role}** does not have permissions to \
                    view matches against prefix **{r.text.split()[-1]}**"
            )


def get_dataframe():
    False


def main():
    dois_input = st.text_area("Add DOIs to check cited by matches against here")
    if dois_input:
        st.session_state.dois = split_dois(dois_input)
        return_doi_list(st.session_state.dois)
        # if check_details({'Email':st.session_state.email,\
        # 'Password':st.session_state.password}) == True:
        run_tool = st.button("Get the cited by matches")
        if run_tool:
            with st.spinner("Getting the data..."):
                get_forward_links(st.session_state.dois, st.session_state.role)


if __name__ == "__main__":
    main()
