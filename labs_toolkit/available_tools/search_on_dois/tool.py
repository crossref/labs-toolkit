from json import JSONDecodeError
import streamlit as st
import requests
import datetime
import pandas as pd
from labs_toolkit.settings import DEPOSITOR_REPORT

DETAILS = {
    "tool-name": "Search on DOIs",
    "description": "From a list of DOIs choose elements from the "
    + "REST API to be returned and option to download to .csv.",
}
works_url = "works?"
nl = "\n"


# EMPTY LISTS
dois = []
failed_dois = []
success_dois = []
not_reg = []
filename_list = ["download"]
column_list = []
data_list = []
custom_list = {
    "Title": "title",
    "Container title": "container-title",
    "Indexed date": "indexed",
    "Prefix": "prefix",
    "Issue": "issue",
    "Publisher": "publisher",
    "Type": "type",
    "Created date": "created",
    "Reference-count": "reference-count",
    "Page": "page",
    "Cited by count": "is-referenced-by-count",
    "Volume": "volume",
    "Authors": "author",
    "Member ID": "member",
    "Full text links": "link",
    "Published": "published",
    "Crossmark Domain": "content-domain",
    "Resource URL": "resource",
    "ISSN": "ISSN",
    "Subject": "subject",
    "Crossmark Policy DOI": "update-policy",
    "Timestamp": "indexed",
    "Funding Data": "funder",
    "ISBN": "ISBN",
    "Subtitle": "subtitle",
    "License": "license",
    "Issue Date": "journal-issue",
}


def convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    df.index.name = "DOIs"
    return df


def headers():
    return {"User-Agent": "labs-toolkit-sodois; mailto:support@crossref.org"}


def split_dois(dois):
    dois = dois.split()
    return dois


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "published-online":
            return str(datetime.date(*v["date-parts"][0], 1))
        elif k == "date-parts":
            return str(datetime.date(*v[0], 1))
        elif k == "timestamp":
            return v
        elif k == "domain":
            return v
        elif k == "primary":
            return v["URL"]


def check_handle(doi):
    handle_url = f"https://doi.org/api/handles/{doi}"
    req = requests.get(handle_url, headers=headers())
    if req.status_code == 200:
        body = req.json()["values"][0]
        url = body["data"]["value"]
        return url
    else:
        return False


def depositor_report_params(cite_id):
    return {"pubid": cite_id}


def get_dois(cite_id):
    r = requests.get(DEPOSITOR_REPORT, params=depositor_report_params(cite_id)).text
    df = pd.DataFrame([x.split() for x in r.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    dois = list(df["DOI"])
    if len(dois) > 2:
        del dois[0:1]
        dois.remove(None)
        return dois
    else:
        error = "That doesn't look like a valid cite ID, any DOIs against \
            the title or an ID from a deleted title. Please try a different ID"
        return error


def custom_route(dois, custom_choice):
    output_dict = {}
    progress_bar = st.empty()
    counter = 0
    for doi in dois:
        counter += 1
        data_dict = {}
        try:
            api_call = "http://api.crossref.org/works/" + doi
            results = requests.get(api_call, headers=headers()).json()["message"]
            for k, v in custom_list.items():
                for c in custom_choice:
                    if c == k:
                        try:
                            data = results.get(v)
                            if isinstance(data, list):
                                if len(data) < 1:
                                    data = "No data"
                                    data_dict[k] = data
                                else:
                                    if c == "ISSN":
                                        data = data
                                    else:
                                        data = data[0]
                                    data_dict[k] = data
                            elif isinstance(data, dict):
                                data = convert_from_dict(data)
                                data_dict[k] = data
                            else:
                                data_dict[k] = data
                        except (ValueError, KeyError, NameError, AttributeError):
                            data = "No data (e)"
                            data_dict[k] = data
                        output_dict[doi] = data_dict
        except (KeyError, JSONDecodeError):
            handle_data = check_handle(doi)
            if not handle_data:
                data_dict["DOI"] = f"{doi} - Not registered"
                output_dict[doi] = data_dict
            else:
                data_dict["DOI"] = doi
                data_dict["Resource URL"] = handle_data
                output_dict[doi] = data_dict
        progress_bar.success(
            f"DOI checked = {counter}/{len(dois)} 		:white_check_mark: "
        )
    return output_dict


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8", index=True)
    return csv_data


def get_results(results, filename):
    with st.expander(f"Here are the results: {len(st.session_state.dois)}"):
        st.write(results)
        data = data_to_csv(results)
        download_data = st.download_button(
            "📥  Download Dataframe", data, mime="text/csv", file_name=filename
        )
        if download_data:
            st.toast("File Downloaded")
        return download_data


def init_form():
    dois_input = st.text_area("Enter DOIs here")
    st.text(
        "Or enter a cite ID below (including content type letter e.g. B12345 or J12345)"
    )
    cite_id = st.text_input("Enter the cite ID of a title here")
    if cite_id:
        st.session_state.dois = get_dois(cite_id)
    if dois_input:
        st.session_state.dois = split_dois(dois_input)


def main():
    st.session_state.dois = []
    timestamp = "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())
    init_form()
    if len(st.session_state.dois) > 0:
        custom_choice = st.multiselect(
            "What data do you want to return?", sorted(custom_list.keys())
        )
        run_tool = st.button("Get the Data")
        if run_tool:
            with st.spinner("Getting the data..."):
                if len(custom_choice) > 0:
                    results_dict = custom_route(st.session_state.dois, custom_choice)
                    results = convert_to_table(results_dict)
                    get_results(results, f"data_from_dois_{timestamp}.csv")
                else:
                    st.write(
                        "No data requested for return, please \
                            select elements from list and try again"
                    )
