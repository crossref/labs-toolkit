import streamlit as st
import requests
import datetime
import pandas as pd
import time
from bs4 import BeautifulSoup as bs

DETAILS = {
    "tool-name": "ISSN/ISBN Query Tool",
    "description": "Search on a list of ISSNs/ISBNs and retrieve number of DOIs and link to depositor report.",
}


def dict_convert_to_table(results):
    pd.set_option("display.colheader_justify", "center")
    df = pd.DataFrame.from_dict(
        results,
        orient="index",
        columns=[
            "Title",
            "Cite ID",
            "Total DOIs",
            "Type",
            "Depositor Report",
            "Owner prefix(admin)",
            "Content Type",
        ],
    )
    df.columns.name = "ISSN/ISBN"
    df = df.to_html(escape=False)
    return df


def headers():
    return {"User-Agent": "labs-toolkit-iiqt; mailto:support@crossref.org"}


def split_ids(ids):
    ids = ids.replace("-", "").replace(" ", "").split()
    return ids


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "date-parts":
            data = datetime.date(*v[0])
    return data


def data_to_csv(results):
    df = pd.DataFrame.from_dict(
        results,
        orient="index",
        columns=[
            "Title",
            "Cite ID",
            "Total DOIs",
            "Type",
            "Depositor Report",
            "Owner prefix(admin)",
            "Content Type",
        ],
    )
    df.index.name = "ISSN/ISBN"
    csv_data = df.to_csv(encoding="utf-8")
    return csv_data


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def get_depo_list(r):
    df = pd.DataFrame([x.split() for x in r.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    owner_prefixes = list(df["OWNER"])
    dois = list(df["DOI"])
    if len(owner_prefixes) > 2:
        del owner_prefixes[0:1]
        del dois[0:1]
        owner_prefixes.remove(None)
        dois.remove(None)
        return dois
    else:
        error = "That doesn't look like a valid cite ID, any DOIs against \
            the title or an ID from a deleted title. Please try a different ID"
        return error


def check_id(cite_id, content_type):
    r = requests.get(
        f"https://data.crossref.org/depositorreport?pubid={content_type}{cite_id}"
    )
    if r.status_code == 200:
        total_dois = get_depo_list(r.text)
        return len(total_dois), r.url
    elif r.status_code == 404:
        total_dois = "Not registered"
        depo_link = "Not registered"
        return total_dois, depo_link
    elif r.status_code == 504:
        st.warning("The API could be down, try again shortly")
        pass


def id_returns_params(id: str, admin_code: str):
    return {
        "start": "0",
        "size": "20",
        "usr": f"{st.session_state.email}/root",
        "pwd": f"{st.session_state.password}",
        "func": "modify",
        "issns": id,
        "btype": admin_code,
    }


def get_title(id, admin_code):
    data_list = []
    r = requests.post(
        "https://doi.crossref.org/servlet/metadataAdmin?func=search",
        data=id_returns_params(id, admin_code),
    )
    page = bs(r.text, "html.parser")
    total_results = page.find("font", attrs={"color": "red"}).text
    if total_results != "0":
        for f in page.find_all("td", attrs={"class": "bafsa"}):
            data_list.append(f.text)
        try:
            cite_id = (data_list[0]).strip()
            title = data_list[1].strip()
            if admin_code == "0":
                prefix = data_list[5].strip()
                con_type = "Journal"
            else:
                prefix = data_list[7].strip()
                con_type = data_list[6].strip()
            return title, cite_id, prefix, con_type
        except IndexError:
            title = "no title"
            cite_id = f"Check ID {id}"
            prefix = "no prefix returned"
            con_type = "n/a"
        return title, cite_id, prefix, con_type
    else:
        title = "not registered"
        cite_id = f"Check ID {id}"
        prefix = "not registered"
        con_type = "n/a"
        return title, cite_id, prefix, con_type


def id_check(ids):
    counter = 0
    params_dict = {}
    progress = st.empty()
    for id in ids:
        counter += 1
        progress.write(f"Processing {counter} of {len(ids)}")
        time.sleep(0.1)
        if len(id) > 8:
            id_type = "ISBN"
            depo_report_type = "B"
            admin_code = "1"
        else:
            id_type = "ISSN"
            depo_report_type = "J"
            admin_code = "0"
        title, cite_id, prefix, con_type = get_title(id, admin_code)
        if title == "not registered":
            params_dict[id] = [
                title,
                cite_id,
                "0",
                id_type,
                "Not registered",
                prefix,
                "n/a",
            ]
        else:
            total_dois, depo_report_link = check_id(cite_id, depo_report_type)
            params_dict[id] = [
                title,
                cite_id,
                total_dois,
                id_type,
                make_clickable(depo_report_link, f"Depositor report:  {cite_id}"),
                prefix,
                con_type,
            ]
    return params_dict


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def main():
    ids_input = st.text_area("Enter ISSN/ISBNs here")
    if ids_input:
        st.session_state.ids = split_ids(ids_input)
        run_tool = st.button("Return titles")
        if run_tool:
            with st.spinner("Running the report..."):
                results = id_check(st.session_state.ids)
                with st.spinner("Getting the data..."):
                    st.write(dict_convert_to_table(results), unsafe_allow_html=True)
                    st.download_button(
                        "Download data",
                        data_to_csv(results),
                        file_name=f"id_checker_{timestamp()}.csv",
                    )
        else:
            if st.session_state.ids == 0:
                st.write("No ISSNs/ISBNs added, please add to text area and try again")
