import streamlit as st
import requests
import datetime
import pandas as pd
from labs_toolkit.settings import API_URI
from bs4 import BeautifulSoup as bs

DETAILS = {
    "tool-name": "Reports Tool",
    "description": "Use this to run reports for a series of data points until a given date.",
}


def headers():
    return {"User-Agent": "labs-toolkit-reports; mailto:support@crossref.org"}


query_dict = {
    "Number of unique records with funding data": "works?filter=has-funder:true",
    "Number of unique records with Funder IDs": "works?filter=has-funder-doi:true",
    "Records with license and full text links": "works?filter=has-license:true,has-full-text:true",
    "Records with one or more authors with ORCID iDs": "works?filter=has-orcid:t",
    "Number of preprint-to-article links": "works?filter=relation.type:is-preprint-of",
    "Number of works with references": "works?filter=has-references:true",
    "Crossmark status updates": "works?filter=is-update:true",
    "Crossmark content items": "works?filter=has-update-policy:true",
    "Records with clinical trial numbers": "works?filter=has-clinical-trial-number:true",
}


facet_dict = {
    "Members registering license and full text links": "works?filter=has-license:true,has-full-text:true&facet=publisher-name:*&rows=0",
    "Members registering ORCID iDs": "works?filter=has-orcid:true&facet=publisher-name:*&rows=0",
    "Members registering references": "works?filter=has-references:true&facet=publisher-name:*&rows=0",
    "Members with open references": "works?filter=has-references:true,reference-visibility:open&facet=publisher-name:*&rows=0",
    "Members registering clinical trial numbers": "works?filter=has-clinical-trial-number:true&facet=publisher-name:*&rows=0",
}


def type_data():
    type_list_dict = {}
    for i, v in zip(
        [
            i["label"]
            for i in requests.get(f"{API_URI}types").json()["message"]["items"]
        ],
        [i["id"] for i in requests.get(f"{API_URI}types").json()["message"]["items"]],
    ):
        type_list_dict[i] = v
    return type_list_dict


def clean_up_dict(content_type_dict):
    del content_type_dict["Other"]
    del content_type_dict["Part"]
    del content_type_dict["Track"]
    del content_type_dict["Entry"]
    del content_type_dict["Book Series"]
    del content_type_dict["Book Set"]
    del content_type_dict["Proceedings Series"]
    del content_type_dict["Section"]
    return content_type_dict


def content_type_counts():
    content_type_url = f"{API_URI}works?facet=type-name:*&rows=0&filter={st.session_state.until_created_date}"
    st.session_state.url_list.append(content_type_url)
    r = requests.get(content_type_url, headers=headers()).json()["message"]
    total_results = r["total-results"]
    no_of_content_types = r["facets"]["type-name"]["value-count"]
    content_type_dict = r["facets"]["type-name"]["values"]
    clean_up_dict(content_type_dict)
    return total_results, no_of_content_types, content_type_dict


def get_query_counts(query_url):
    url = f"{API_URI}{query_url},{st.session_state.until_created_date}"
    st.session_state.url_list.append(url)
    r = requests.get(url, headers=headers()).json()["message"]
    total_results = r["total-results"]
    return total_results


def query_counts_created_date(query_url, until_created_date, rows):
    url = f"{API_URI}{query_url},{until_created_date}{rows}"
    st.session_state.url_list.append(url)
    r = requests.get(url).json()["message"]
    total_results = r["total-results"]
    return total_results


def total_query_counts():
    results_dict = {}
    for k, v in query_dict.items():
        results = get_query_counts(v)
        results_dict[k] = results
    return results_dict


def counts_by_date(user_input_date, rows):
    created_results_dict = {}
    for k, v in query_dict.items():
        results = query_counts_created_date(v, user_input_date, rows)
        created_results_dict[k] = results
    return created_results_dict


def member_counts_per_type(content_type):
    member_counts_per_type_dict = {}
    for i, v in content_type.items():
        r = requests.get(
            f"{API_URI}types/{v}/works?facet=publisher-name:*&rows=0&filter={st.session_state.until_created_date}"
        ).json()
        data = r["message"]["facets"]["publisher-name"]["value-count"]
        member_counts_per_type_dict[f"No of members depositing {i}"] = data
    return member_counts_per_type_dict


def convert_output(output):
    data = pd.DataFrame(output)
    csv_data = data.to_csv(index=False)
    return csv_data


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    return df


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def get_results(results):
    st.table(results)
    data = data_to_csv(results)
    download_data = st.download_button(
        "📥  Download Dataframe",
        data,
        mime="text/csv",
        file_name=st.session_state.filename,
    )
    return download_data


def scrape_dashboard():
    items = []
    r = requests.get("https://data.crossref.org/reports/statusReport.html")
    soup = bs(r.content, "html.parser")
    for table in soup.find_all("tr"):
        items.append(table.find_all("td")[5:52])
    return create_item_list(items)


def create_item_list(items):
    more_items = []
    for item in items[0]:
        more_items.append(item.text)
    i = iter(more_items)
    item_dict = dict(zip(i, i))
    return item_dict


def main():
    st.session_state.url_list = []
    created_date_url = "until-created-date:"
    rows = "&rows=0"
    timestamp = "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())
    # type_list = [i['id']for i in requests.get(f"{API_URI}types").
    # json()['message']['items']]
    st.date_input("What date would you like the data up until: ", key="user_input_date")
    st.session_state.until_created_date = (
        f"{created_date_url}{st.session_state.user_input_date}"
    )
    get_data = st.button("Get Data")
    if get_data:
        with st.spinner("Getting the data"):
            st.session_state.filename = (
                f"reports_until_{st.session_state.user_input_date}_{timestamp}.csv"
            )
            (
                total_results,
                no_of_content_types,
                content_type_dict,
            ) = content_type_counts()
            results_dict = total_query_counts()
            full_results_dict = (
                content_type_dict
                | counts_by_date(st.session_state.until_created_date, rows)
                | member_counts_per_type(type_data())
                | results_dict
            )
            dashboard_dict = scrape_dashboard()
            dashboard = dict_convert_to_table(dashboard_dict)
            results = dict_convert_to_table(full_results_dict)
            with st.expander("API Query URLS"):
                st.write(st.session_state.url_list)
            with st.expander("Results"):
                get_results(dashboard)
                get_results(results)
            with st.expander("Metrics"):
                col1, col2, col3 = st.columns(3)
                for k, v in dashboard_dict.items():
                    st.metric(k, v)
