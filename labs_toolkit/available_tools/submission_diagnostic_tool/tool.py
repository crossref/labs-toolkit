import streamlit as st
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup as bs
from labs_toolkit.settings import ADMIN_BASE_URL
import re
import labs_toolkit.common.cr_common_func as common
import labs_toolkit.utils.data.members as member_list_tool

DETAILS = {
    "tool-name": "Submission Diagnostic Tool",
    "description": "Return the success and failure statistics "
    + "and error messages from a submission log ID.",
}


def timestamp():
    return "{:%Y-%m-%d}".format(datetime.datetime.now())


def yesterday():
    return "{:%Y-%m-%d}".format(datetime.datetime.now() - datetime.timedelta(days=1))


def week_before():
    return "{:%Y-%m-%d}".format(datetime.datetime.now() - datetime.timedelta(days=8))


def form_data(email, password):
    credentials = {
        "operation": "doMDUpload",
        "login_id": f"{email}/root",
        "login_passwd": password,
    }
    return credentials


def rerun_submission(submission):
    sub_url = "https://doi.crossref.org/servlet/submissionAdmin"
    return requests.post(sub_url, data={"sf": "rerun", "submissionID": submission})


def check_timestamps(doi_list):
    for doi in doi_list:
        checking = False
        while checking:
            url = f"https://hdl.handle.net/api/handles/{doi}"
            req = requests.get(url)
            if req.status_code == 200:
                handle_timestamp = req.json()["values"][1]["data"]["value"]
                if handle_timestamp == timestamp():
                    return True


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    return df


def get_results(results, filename, doi_list, submission, successes, failures):
    with st.expander(
        f"Here are the results for: {submission}:   \tSuccess:\ \
            {successes}   \tFailures: {failures}"
    ):
        st.write(results)
        data = data_to_csv(results)
        download_data = st.download_button(
            "📥  Download Dataframe", data, mime="text/csv", file_name=filename
        )
        joined_list = "  \n".join(doi_list)
        st.info(f"**DOI List from submission:**  {len(doi_list)}  \n{joined_list}")
        return download_data


def show_file_results(file, dois):
    with st.expander(f"Here are the DOIs for: {file}: {len(dois)} DOIs"):
        joined_list = "  \n".join(dois)
        st.info(f"**DOI List from submission:**  \n{joined_list}")


def headers():
    return {
        "User-Agent": "labs-toolkit-sdiag; mailto=support@crossref.org",
    }


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def split_submissions(submissions):
    submissions = submissions.split()
    return submissions


def params(prefix):
    return {"start": "0", "size": "20", "sf": "search", "doi": prefix}


def credentials(role):
    return {
        "usr": f"{st.session_state.email}/{role}",
        "pwd": f"{st.session_state.password}",
    }


def sub_type():
    if st.session_state.selected_types == "Metadata":
        return "X"
    elif st.session_state.selected_types == "Resource only":
        return "C"
    elif st.session_state.selected_types == "Bulk URL Update":
        return "T"
    elif st.session_state.selected_types == "Conflicts":
        return "U"
    elif st.session_state.selected_types == "Query":
        return "Q"
    else:
        return None


def admin_system_headers():
    return {
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "labs-toolkit",
    }


def get_admin_id(prefix):
    r = requests.post(
        "https://doi.crossref.org/servlet/publisherUserAdmin",
        data=params(prefix) | credentials("root"),
        headers=admin_system_headers(),
    )
    rows = bs(r.text).find_all(
        "table",
        attrs={"border": "0", "cellpadding": "3", "cellspacing": "0", "width": "100%"},
    )
    for tr in rows:
        td_list = list(tr.find_all("td", attrs={"class": "bgWhite"}))
        for td in td_list[1]:
            st.session_state.member_id = td.strip()
            get_usernames(st.session_state.member_id)


def get_username(row):
    return [re.split("Remote User|Members", x) for x in row]


def return_submissions_params(user_id):
    return {
        "sf": "search",
        "finishedDateStart": week_before(),
        "finishedDateEnd": yesterday(),
        "depositor": user_id,
        "start": 0,
        "maxResultSize": st.session_state.number_submissions,
        "SubTypeKey": sub_type(),
    }


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def return_submissions(user):
    sub_list = []
    user_id = st.session_state.id_dict.get(user)
    r = requests.get(
        f"{ADMIN_BASE_URL}/submissionAdmin?",
        params=return_submissions_params(user_id) | credentials("root"),
    )
    soup = bs(r.content, "html.parser")
    submissions_table = (soup.find_all("table")[5])("a")[0::3]
    for submission in submissions_table:
        sub_list.append((submission.text).strip())
    results_section(user, sub_list)


def results_section(user, sub_list):
    with st.expander(
        f"Last {st.session_state.number_submissions} submissions for user **{user}**"
    ):
        for s in sub_list:
            st.write(
                f"[{s}](https://doi.crossref.org/servlet/submissionAdmin?sf=detail&submissionID={s})"
            )


def trace_submission(searchable):
    if st.session_state.file_or_batchid == "Filename":
        search = f"file_name={searchable}"
    else:
        search = f"doi_batch_id={searchable}"
    r = requests.get(
        f"{ADMIN_BASE_URL}/submissionDownload?{search}&type=result",
        params=credentials(st.session_state.role),
    )
    st.write(r.url)
    st.write(r.text)


def get_usernames(id):
    st.session_state.id_dict = {}
    r = requests.get(
        f"{ADMIN_BASE_URL}/publisherUserAdmin?sf=viewMembers&publisherID={id}",
        params=credentials("root"),
    )
    soup = bs(r.text)
    table = soup.find_all("table")[5].text
    table_rows = table.split(".")
    del table_rows[0:2]
    for row in get_username(table_rows):
        st.session_state.id_dict[row[1].strip()] = row[0].strip()


def submission_url_params(submission):
    return {"sf": "detail", "submissionID": submission}


def main():
    _, member_list_json, _, member_list = member_list_tool.download_members_list()
    if "members_list" not in st.session_state:
        member_list.members_list = []
    st.write("**Trace submission from filename or batch ID**")
    filename_batch = st.text_input("Enter filename or batch ID")
    st.radio(
        "Search on filename or batch ID",
        horizontal=True,
        options=["Filename", "Batch ID"],
        key="file_or_batchid",
    )
    if filename_batch:
        trace_submission(filename_batch)
    st.markdown("***")
    st.write(
        f"**Return submission logs against {len(member_list_json)} available prefixes**"
    )
    member = st.multiselect(
        "Select prefix/member to return submissions from",
        member_list_json.items(),
        format_func=lambda x: f"{x[0]} : {x[1]}",
        default=None,
        max_selections=1,
    )
    c1, c2 = st.columns([6, 1])

    if member:
        get_admin_id(member[0][0])
        st.multiselect(
            "Which usernames do you want to search against",
            options=st.session_state.id_dict.keys(),
            key="selected_usernames",
        )
        c1.radio(
            "Submission types to return",
            options=sorted(
                [
                    "Metadata",
                    "Query",
                    "Bulk URL Update",
                    "Conflicts",
                    "Resource only",
                    "All",
                ]
            ),
            key="selected_types",
            horizontal=True,
        )
        c2.number_input(
            "Submissions to return", min_value=1, max_value=20, key="number_submissions"
        )
        get_submissions = st.button("Get submissions")
        if get_submissions:
            for user in st.session_state.selected_usernames:
                return_submissions(user)

    st.markdown("***")
    st.write("**Or add submission logs to below**")
    submissions = st.text_area(
        "Enter the submission ID to diagnose", key="submission_ids"
    )
    submission_button = st.button("Retrieve Submission Information")
    st.markdown("***")
    st.write("**Or upload XML files to analyze**")
    st.markdown("###")
    st.file_uploader(
        "Upload XML files to diagnose", key="files", accept_multiple_files=True
    )
    xml_file_button = st.button("Scan XML files")
    if submission_button:
        submissions = split_submissions(st.session_state.submission_ids)
        for submission in submissions:
            doi_list = []
            doi_report_dict = {}
            xml = requests.get(
                f"{ADMIN_BASE_URL}/submissionAdmin",
                params=submission_url_params(submission),
                headers=headers(),
                data=common.credentials(),
            )
            st.write(xml.url)
            if xml.status_code == 200:
                soup = bs(xml.text, "html.parser")
                td = soup.find("textarea", attrs={"name": "noteMessage"})
                message = f"{td}"
                successes = td.find("success_count").text
                failures = td.find("failure_count").text
                soup2 = bs(message, "html.parser")
                for rd in soup2.find_all("record_diagnostic"):
                    status = rd.get("status")
                    msg = rd.find("msg").text
                    doi = rd.find("doi").text
                    doi_report_dict[doi] = [status, msg]
                    url2 = (
                        f"{ADMIN_BASE_URL}/submissionAdmin?sf=content"
                        + f"&submissionID={submission}&usr={st.session_state.email}/"
                        + f"root&pwd={st.session_state.password}"
                    )
                    xml2 = requests.get(url2, headers=headers())
                    soup2 = bs(xml2.content, "html.parser")
                    for item in soup2.find_all("doi_data"):
                        xml_doi = item.find("doi").text
                        if xml_doi not in doi_list:
                            doi_list.append(xml_doi)
                with st.spinner("Getting the data..."):
                    results = dict_convert_to_table(doi_report_dict)
                    get_results(
                        results,
                        f"submission_diag_tool_{timestamp}",
                        doi_list,
                        submission,
                        successes,
                        failures,
                    )
    if xml_file_button:
        for file in st.session_state.files:
            doi_list = []
            soup2 = bs(file, "html.parser")
            for item in soup2.find_all("doi_data"):
                xml_doi = item.find("doi").text
                doi_list.append(xml_doi)
            show_file_results(file.name, doi_list)
