import streamlit as st
import requests
import datetime
import pandas as pd
import plotly.graph_objects as go
from labs_toolkit.settings import CR_PRIMARY_COLORS
from labs_toolkit.settings import API_URI, CLEARBIT_API
import random
import tldextract
import streamlit.components.v1 as components
import pyperclip

DETAILS = {
    "tool-name": "Similarity Check Checker",
    "description": "Run through the Similarity Check application process automation",
}


def headers():
    return {"User-Agent": "labs-toolkit-scc; mailto:support@crossref.org"}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def request_date():
    return "{:%Y-%m-%d}".format(datetime.datetime.now())


def convert_output(output):
    data = pd.DataFrame(output)
    csv_data = data.to_csv(index=False, encoding="utf_8")
    return csv_data


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def dataframe_from_list(list):
    df = pd.DataFrame(
        data=[list],
        columns=[
            "Request Date",
            "CR Staff Initials",
            "v1/v2",
            "Member Name",
            "Member ID",
            "Intacct ID",
            "DOI Prefix",
            "Sponsoring Organisation",
            "Country",
            "City",
            "Administrator Contact Name",
            "Administrator Contact Email",
            "Technical Contact Name",
            "Technical Contact Email",
            "MTS",
        ],
    )
    df.set_index("Request Date", inplace=True)
    return df


def has_error(url: str, error_message):
    st.error(f"An error has occurred  \n{url}")
    st.code(f"Error message: {error_message}")


def get_data(url):
    item_list = []
    cursor = "*"
    rows = 1000
    url_check = requests.head(f"{url}", headers=headers()).status_code
    if url_check == 200:
        while True:
            res = requests.get(f"{url}&rows={rows}&cursor={cursor}", headers=headers())
            try:
                body = res.json()["message"]
                items = body["items"]
                if not items:
                    break
                cursor = str(body["next-cursor"])
                for i in items:
                    item_list.append(i)
            except TypeError as e:
                st.warning(
                    f"The API query is not valid or no results \
                        available, please clear form and try again  \n{e}"
                )
    elif str(url_check.status_code).startswith("5"):
        st.error(
            f"{url_check.status_code} Error returned...the \
                API could be down or the query is not valid"
        )
    else:
        st.warning(
            f"Error code: {url_check.status_code}  \n \
                   with URL {url}"
        )
        st.warning("Valid JSON has not been retrieved")
    return item_list


def get_total_results(url):
    results = requests.get(url, headers=headers()).json()["message"]["total-results"]
    return results


def get_urls():
    st.session_state.sim_dois_urls = []
    st.session_state.dois_urls = []
    for i in st.session_state.all_data:
        doi = i["DOI"]
        try:
            link_section = i["link"]
            if link_section:
                for line in link_section:
                    application = line["intended-application"]
                    if application == "similarity-checking":
                        url = line["URL"]
                        st.session_state.sim_dois_urls.append(f"{doi}\t{url}")
            else:
                url = "Null"
        except Exception:
            url = "Null"
        st.session_state.dois_urls.append(f"{doi}\t{url}")


def get_logo():
    try:
        if len(st.session_state.all_data) > 0:
            link = st.session_state.all_data[0]["resource"]["primary"]["URL"]
            domain = tldextract.extract(link).registered_domain
            r = requests.get(f"{CLEARBIT_API}{domain}")
            if r.status_code == 200:
                return st.image(r.url)
            else:
                return st.image("app/utils/no-image.png")

        else:
            st.warning("This member has no DOIs registered with us")
            return st.image("no-image.png")
    except Exception as e:
        st.write(f"{e}")


def sim_check_params():
    return "filter=has-full-text:t,full-text.application:similarity-checking,type:journal-article"


def non_sim_check_params():
    return "filter=has-full-text:f,type:journal-article"


def all_data_params():
    return "filter=type:journal-article"


def sim_check_check():
    c1, c2 = st.columns([3, 2])
    c1.info(
        f"Total DOIs **{st.session_state.all_data_total}**  \n\
            DOIs with SimCheck URLs **{st.session_state.sim_true_total}**"
    )
    if not st.session_state.sim_true_total == st.session_state.all_data_total:
        sim_check_total = (
            st.session_state.sim_true_total / st.session_state.all_data_total
        ) * 100
        if sim_check_total > 90:
            st.success("Over 90% with Similarity check URLs")
        else:
            st.warning("Under 90% with Similarity check URLs")
        fig = go.Figure(
            go.Indicator(
                mode="number+gauge",
                value=int(sim_check_total),
                number={
                    "suffix": "%",
                    "font": {
                        "color": CR_PRIMARY_COLORS["CR_PRIMARY_YELLOW"],
                        "size": 18,
                    },
                },
                gauge={
                    "shape": "bullet",
                    "axis": {"range": [None, 100]},
                    "bar": {
                        "color": CR_PRIMARY_COLORS["CR_PRIMARY_LT_GREY"],
                        "line": {"width": 0},
                        "thickness": 1,
                    },
                    "bgcolor": "#ffffff",
                },
                title={
                    "text": "Percentage",
                    "font": {
                        "color": CR_PRIMARY_COLORS["CR_PRIMARY_YELLOW"],
                        "size": 16,
                    },
                },
            ),
            layout={
                "autosize": False,
                "width": 300,
                "height": 80,
                "margin": dict(l=80, r=20, b=5, t=5, pad=4),
                # "paper_bgcolor": CR_PRIMARY_COLORS["CR_PRIMARY_DK_GREY"],
                "xaxis_title": "Wangfo",
            },
        )

        c2.plotly_chart(fig)

    else:
        st.success(
            f"{st.session_state.member} has Similarity check URLs against all DOIs"
        )
        sim_check_total = 0


def get_member_data(id):
    r = requests.get(
        f"https://api.labs.crossref.org/members/{id}", headers=headers()
    ).json()["message"]
    return r["cr-labs-member-profile"]


def find_member(id):
    st.session_state.member_id_type = "prefix" if id.startswith("10.") else "member"
    try:
        if st.session_state.member_id_type == "prefix":
            r = requests.get(f"{API_URI}prefixes/{id}", headers=headers())
            st.session_state.member = r.json()["message"]["name"]
            st.session_state.member_id = (r.json()["message"]["member"]).split("/")[-1]
        else:
            r = requests.get(f"{API_URI}members/{id}", headers=headers())
            st.session_state.member = r.json()["message"]["primary-name"]
            st.session_state.member_id = id
        st.session_state.api_url = (
            f"{API_URI}members/{st.session_state.member_id}/works?"
        )
        return f"**{st.session_state.member}**   Search ID: **{id}**"
    except Exception as e:
        st.session_state.has_error = True
        return f"Error {e}, no member name against {id}"


def create_member_list(member_dict, member_id):
    st.session_state.member_details_list = [
        request_date(),
        (st.session_state.email[:2]).upper(),
        "v1/v2",
        st.session_state.member,
        st.session_state.member_id,
        member_dict.get("accounting-id"),
        member_id,
        member_dict.get("sponsor-name"),
        "Country",
        "City",
        "Admin Contact Name",
        "Admin Contact Email",
        "Tech Contact Name",
        "Tech Contact Email",
        "MTS",
    ]


def check_sponsored(member_dict):
    if member_dict.get("account-type") == "REPRESENTED_MEMBER":
        st.write("Sponsored member")
        components.iframe(
            "https://docs.google.com/document/d/134UOCSjvB1m4m9L8o24aP8eo6wf8T8eONohG1Au2VMA/edit#heading=h.qf00e4akqdgz",
            height=600,
        )
        st.markdown(
            make_clickable(
                "https://docs.google.com/document/d/134UOCSjvB1m4m9L8o24aP8eo6wf8T8eONohG1Au2VMA/edit#heading=h.qf00e4akqdgz",
                "Link to Sponsored member flowchart",
            ),
            unsafe_allow_html=True,
        )
    else:
        st.write("Member")
        components.iframe(
            "https://docs.google.com/document/d/134UOCSjvB1m4m9L8o24aP8eo6wf8T8eONohG1Au2VMA/edit#heading=h.10hn7vr4scgs",
            height=600,
        )
        st.markdown(
            make_clickable(
                "https://docs.google.com/document/d/134UOCSjvB1m4m9L8o24aP8eo6wf8T8eONohG1Au2VMA/edit#heading=h.10hn7vr4scgs",
                "Link to member flowchart",
            ),
            unsafe_allow_html=True,
        )


def main():
    counter = 0
    st.session_state.doi_urls = {}
    st.session_state.has_error = False
    st.text("Enter prefix or member ID to search on below")
    col1, col2, col3 = st.columns([3, 1, 5])
    member_id = col1.text_input(
        "Add member ID or prefix here",
        key="id",
        placeholder="Examples in the help section ❓",
        help="prefix = 10.5555 or memberID = 1002",
        label_visibility="collapsed",
    )
    filename = f"sim_check_{member_id}_{timestamp()}.csv"
    if member_id:
        col3.info(find_member(member_id))
        with st.spinner("Getting member data and similarity check numbers..."):
            st.session_state.sim_true_total = get_total_results(
                f"{st.session_state.api_url}{sim_check_params()}&rows=0"
            )
            st.session_state.sim_false_total = get_total_results(
                f"{st.session_state.api_url}{non_sim_check_params()}&rows=0"
            )
            st.session_state.all_data_total = get_total_results(
                f"{st.session_state.api_url}{all_data_params()}&rows=0"
            )
            st.session_state.all_data = get_data(
                f"{st.session_state.api_url}{all_data_params()}"
            )
            with col2:
                get_logo()
            member_dict = get_member_data(st.session_state.member_id)
            if not member_dict:
                st.warning("No member data in Labs API yet")
                get_urls()
                if not st.session_state.has_error:
                    sim_check_button = st.button("Check Similarity Check Score")
                    check_sponsored(member_dict)
                    if sim_check_button:
                        sim_check_check()
                        if len(st.session_state.sim_dois_urls) > 10:
                            sample = random.sample(st.session_state.sim_dois_urls, 10)
                            st.info("Here are some random URLs from the list to check")
                            for samp in sample:
                                counter += 1
                                try:
                                    r = requests.head(samp.split()[1])
                                    if r.status_code in [200, 301]:
                                        st.success(f"{counter}: {samp}")
                                    else:
                                        st.warning(
                                            f"{counter}: {samp} is \
                                                returning {r.status_code}"
                                        )
                                        continue
                                except Exception as e:
                                    st.warning(f"{counter}: {samp} is returning {e}")
                                    continue
                        else:
                            st.text("Here are the URLs from the list to check")
                            st.write(f"1{st.session_state.sim_dois_urls}")
                            for item in st.session_state.sim_dois_urls:
                                counter += 1
                                r = requests.head(item.split()[1])
                                if r.status_code == 200:
                                    st.success(f"{counter}: {item}")
                                else:
                                    st.warning(
                                        f"DOI/URL {item} returning {r.status_code}"
                                    )
                                    continue
                        st.download_button(
                            label="List of DOIs and URLs if available",
                            data=convert_output(st.session_state.dois_urls),
                            mime="csv",
                            file_name=filename,
                        )
                else:
                    st.warning("No member against that ID")
            else:
                if member_dict.get("suspended") or member_dict.get("inactive") in [
                    "No",
                    "",
                ]:
                    st.info(
                        f"Member Type: {member_dict.get('account-type')}  \n\
						Annual Fee: \${member_dict.get('annual-fee')}  \n\
						Similarity Check Fee: \
                            \${float(member_dict.get('annual-fee'))/5}"
                    )
                    create_member_list(member_dict, member_id)
                    get_urls()
                    st.dataframe(
                        dataframe_from_list(st.session_state.member_details_list)
                    )
                    copy = st.button("Copy data to clipboard")
                    if copy:
                        pyperclip.copy(",".join(st.session_state.member_details_list))
                    check_sponsored(member_dict)
                    if not st.session_state.has_error:
                        sim_check_button = st.button("Check Similarity Check Score")
                        if sim_check_button:
                            sim_check_check()
                            if len(st.session_state.sim_dois_urls) > 10:
                                sample = random.sample(
                                    st.session_state.sim_dois_urls, 10
                                )
                                st.info(
                                    "Here are some random URLs from the list to check"
                                )
                                for samp in sample:
                                    counter += 1
                                    try:
                                        r = requests.head(samp.split()[1])
                                        if r.status_code in [200, 301]:
                                            st.success(f"{counter}: {samp}")
                                        else:
                                            st.warning(
                                                f"{counter}: {samp} is \
                                                    returning {r.status_code}"
                                            )
                                            continue
                                    except Exception as e:
                                        st.warning(
                                            f"{counter}: {samp} is returning {e}"
                                        )
                                        continue
                            else:
                                st.text("Here are the URLs from the list to check")
                                st.write(f"1{st.session_state.sim_dois_urls}")
                                for item in st.session_state.sim_dois_urls:
                                    counter += 1
                                    r = requests.head(item.split()[1])
                                    if r.status_code == 200:
                                        st.success(f"{counter}: {item}")
                                    else:
                                        st.warning(
                                            f"DOI/URL {item} returning {r.status_code}"
                                        )
                                        continue
                            st.download_button(
                                label="List of DOIs and URLs if available",
                                data=convert_output(st.session_state.dois_urls),
                                mime="csv",
                                file_name=filename,
                            )
                    else:
                        st.warning("No member against that ID")
                else:
                    st.write(
                        f"Member {st.session_state.member} is suspended or inactive"
                    )
    st.markdown(
        """<a href="https://clearbit.com">Logos provided by Clearbit</a>""",
        unsafe_allow_html=True,
    )
