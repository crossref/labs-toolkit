import streamlit as st
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup as bs


DETAILS = {
    "tool-name": "Cite ID Retrieval Tool",
    "description": "Enter a list of DOIs and retrieve the Crossref title IDs from them.",
}


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index", columns=["TYPE", "CITE ID"])
    df.columns.name = "DOI"
    df = df.to_html(escape=False)
    return df


def headers():
    return {"User-Agent": "labs-toolkit-cirt; mailto:support@crossref.org"}


def split_dois(dois):
    dois = dois.split()
    return dois


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "date-parts":
            data = datetime.date(*v[0])
    return data


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def results_list(tag, data):
    with st.expander(f"{tag} DOIs - {len(data)}"):
        df = pd.DataFrame(data)
        st.dataframe(df)
        st.download_button(
            f"📥  Download {tag} DOIs",
            data_to_csv(data),
            mime="text/csv",
            file_name=f"{tag}_dois_{timestamp}",
            key=f"{tag}",
        )


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def main():
    results_dict = {}
    dois_input = st.text_area(
        "Enter DOIs here",
        placeholder="Example DOIs in the help icon ❓",
        help="10.5555/12345678  \n10.29011/2575-8241.000102",
    )
    if dois_input:
        st.session_state.dois = split_dois(dois_input)
        for doi in st.session_state.dois:
            # time.sleep(0.1)
            url = f"https://api.crossref.org/works/{doi}/transform/application/vnd.crossref.unixsd+xml"
            xml = requests.get(url)
            soup = bs(xml.text, "html.parser")
            crm_items = soup.find_all("crm-item")
            if not soup.find_all(attrs={"name": "prefix-name"}):
                title_id = crm_items[3]
            else:
                title_id = crm_items[4]
            content_type = title_id.get("name")
            results_dict[doi] = [content_type.upper(), title_id.text]
        with st.spinner("Getting the data..."):
            st.write(dict_convert_to_table(results_dict), unsafe_allow_html=True)
