import streamlit as st
import datetime
import pandas as pd
import xml.etree.cElementTree as ET
from bs4 import BeautifulSoup as bs

DETAILS = {
    "tool-name": "Multiple Resolution Tool (Unlock DOIs)",
    "description": "A tool to create the XML file to unlock the DOIs for Multiple Resolution",
}


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    return df


def headers():
    return {"User-Agent": "labs-toolkit-mrt; mailto:support@crossref.org"}


def split_dois(dois):
    dois = dois.split()
    for doi in dois:
        doi.strip()
        if not doi.startswith("10."):
            if doi.lower().startswith(("https", "http")):
                st.warning(
                    "Please only include the DOI starting with 10. rather than the full DOI link"
                )
            else:
                st.warning(f"This is not a valid DOI {doi}")
            st.error("**Process stopped** - Please enter valid DOIs and try again")
            st.stop()
    return dois


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "date-parts":
            data = datetime.date(*v[0])
    return data


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def declaration():
    return {
        "xmlns": "http://www.crossref.org/doi_resources_schema/4.4.2",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "version": "4.4.2",
        "xsi:schemaLocation": "http://www.crossref.org/doi_resources_schema/4.4.2 \
            http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd",
    }


def write_bs():
    pretty_xml = bs(open(st.session_state.filename), "html.parser").prettify()
    download_xml(open(st.session_state.filename))
    with st.expander("Here is the XML"):
        st.code(pretty_xml)


def download_xml(xml):
    st.success("The XML has been created, click the download button.")
    return st.download_button("Download XML", xml, file_name=st.session_state.filename)


def generate_xml(doi_batch_id):
    doi_batch = ET.Element("doi_batch", declaration())
    tree = ET.ElementTree(doi_batch)
    head = ET.SubElement(doi_batch, "head")
    ET.SubElement(head, "doi_batch_id").text = doi_batch_id
    depositor = ET.SubElement(head, "depositor")
    ET.SubElement(depositor, "depositor_name").text = st.session_state.depositor
    ET.SubElement(depositor, "email_address").text = st.session_state.depositor_email
    body = ET.SubElement(doi_batch, "body")
    for doi in st.session_state.dois:
        doi_resources = ET.SubElement(body, "doi_resources")
        ET.SubElement(doi_resources, "doi").text = doi
        collection = ET.SubElement(doi_resources, "collection")
        collection.set("multi-resolution", "unlock")
        collection.set("property", "list-based")
    tree.write(st.session_state.filename, encoding="UTF-8", xml_declaration=True)
    return tree


def main():
    st.info("**Please complete all fields below**")
    dois_input = st.text_area(
        "Enter DOIs here",
        placeholder="Example value in help icon ❓ ",
        help="https://api.crossref.org/members?rows=10",
    )
    depositor = st.text_input("Add the depositor name here", key="depositor")
    dep_email = st.text_input(
        "Add the depositor email address here", key="depositor_email"
    )
    if dois_input and depositor and dep_email:
        with st.spinner("Generating the unlock XML file"):
            st.session_state.dois = split_dois(dois_input)
            doi_batch_id = f"{st.session_state.email}_{timestamp()}"
            st.session_state.filename = f"mr_unlock_{timestamp()}.xml"
            generate_xml(doi_batch_id)
            write_bs()
