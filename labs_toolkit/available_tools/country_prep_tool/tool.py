import streamlit as st
import pandas as pd
import plotly.express as px
import countryflag
from labs_toolkit.settings import CR_PRIMARY_COLORS
import labs_toolkit.common.cr_common_func as common
import labs_toolkit.utils.data.members as member_list_tool
from collections import defaultdict

DETAILS = {
    "tool-name": "Country Participation Report",
    "description": "Report that gets the average participation reports based on location",
}

RESOURCE_TYPES_TO_COLLECT_SCORES = [
    "journal-article",
    "report",
    "proceedings-article",
    "dataset",
    "book-chapter",
    "posted-content",
    "reference-book",
    "edited-book",
    "grant",
    "monograph",
    "dissertation",
]

DEFAULT_PREP_DICT = {
    key: [0]
    for key in [
        "affiliations",
        "abstracts",
        "orcids",
        "licenses",
        "references",
        "funders",
        "similarity-checking",
        "award-numbers",
        "ror-ids",
        "update-policies",
        "resource-links",
        "descriptions",
    ]
}


def get_flag(country_code):
    if country_code == "No member profile":
        return "noflag"
    try:
        return countryflag.getflag([country_code])
    except (ValueError, TypeError):
        return "noflag"


def create_selection(country_dict):
    st.multiselect(
        "Country Selectbox",
        options=sorted(country_dict.keys()),
        key="selection",
        format_func=lambda x: f"{x} {get_flag(x)} : {len(country_dict[x])}",
        max_selections=5,
    )


def get_member_prep_data_download(ids, country, member_dict):
    full_member_coverage = {}
    my_bar = st.progress(0, text=f"Processing {country}'s {len(ids)} IDs")
    for idx, id in enumerate(ids):
        member_coverage = process_member_data(id, member_dict)
        my_bar.progress(
            (idx + 1) / len(ids), text=f"Processing member {idx + 1}/{len(ids)}"
        )
        full_member_coverage.update(member_coverage)
    return full_member_coverage


def process_member_data(id, member_dict):
    member_details = member_dict.get(id)
    member_name = member_details["primary-name"]
    try:
        total_dois = member_details["counts"]["total-dois"]
    except KeyError:
        total_dois = 0
    if total_dois < 1 and st.session_state.zero_dois:
        st.session_state.members_with_zero_dois[id] = {"Member Name": member_name}
    else:
        return process_coverage(member_details, id, member_name, total_dois)


def process_coverage(item, id, member_name, total_dois):
    member_coverage = {}
    member_coverage[id] = {
        "Member Name": member_name,
        "Total DOIs": total_dois,
        "Report": DEFAULT_PREP_DICT.copy(),
    }
    try:
        resource_types = item["counts-type"]["all"]
    except KeyError:
        resource_types = []
    if resource_types:
        member_coverage[id]["Report"] = process_resource_types(resource_types, item, id)
    return member_coverage


def process_resource_types(resource_types, item, id):
    resource_type_coverage = {}
    for resource_type in RESOURCE_TYPES_TO_COLLECT_SCORES:
        if resource_type in resource_types:
            rt_coverage = item["coverage-type"]["all"].get(resource_type, {})
            rt_coverage.pop("last-status-check-time", None)
            resource_type_coverage[resource_type] = rt_coverage
            # TODO find out what to do with members only depositing journals with no metadata
    if resource_type_coverage:
        return collate_resource_type_reports(resource_type_coverage)
    else:
        return DEFAULT_PREP_DICT


def collate_resource_type_reports(data_dict):
    collated = {}
    for resource_type, data in data_dict.items():
        for k, v in data.items():
            collated.setdefault(k, []).append(v)
    return collated


def collate_data_into_lists(dictionary):
    collated_data = {}
    for data in dictionary.values():
        try:
            for k, v in data["Report"].items():
                collated_data.setdefault(k, []).extend(v)
        except AttributeError:
            pass
    return collated_data


def collect_csv_file_data(dictionary):
    csv_data = {}
    for id, data in dictionary.items():
        member_sum_dict = {
            "Member Name": data["Member Name"],
            "Total DOIs": data["Total DOIs"],
        }
        try:
            for k, v in data["Report"].items():
                member_sum_dict[k] = get_average_percent(v)
                csv_data[id] = member_sum_dict
        except Exception:
            csv_data[id] = member_sum_dict
    return csv_data


def get_average_percent(numbers):
    return int(sum(numbers) / len(numbers) * 100)


def average_the_lists(data_dict, title):
    return {
        f"average_{title}": {k: get_average_percent(v) for k, v in data_dict.items()}
    }


def csv_totals_row(csv_data):
    totals_row = defaultdict(list)
    for record in csv_data.values():
        for k, v in record.items():
            totals_row[k].append(v)
    result = {"Member Name": "Totals"}
    for k, v in totals_row.items():
        if k == "Total DOIs":
            result[k] = sum(v)
        elif k != "Member Name":
            result[k] = int(get_average_percent(v) / 100)
    return result


def display_graph(data, index, ids):
    df = pd.DataFrame(data=data)
    df.index = index
    fig = px.bar(
        df,
        barmode="group",
        width=1000,
        height=500,
        text_auto=True,
        orientation="h",
        range_x=[0, 100],
        labels={"index": "Metadata Field", "value": "Percentage"},
        color_discrete_sequence=[
            CR_PRIMARY_COLORS["CR_PRIMARY_RED"],
            CR_PRIMARY_COLORS["CR_PRIMARY_BLUE"],
            CR_PRIMARY_COLORS["CR_PRIMARY_YELLOW"],
            CR_PRIMARY_COLORS["CR_PRIMARY_DK_GREY"],
            CR_PRIMARY_COLORS["CR_PRIMARY_LT_GREY"],
        ],
    )
    fig.update_traces(textangle=0)
    for index, id in enumerate(ids):
        fig.data[index].name = f"{fig.data[index].name} ({id})"

    st.plotly_chart(fig)


def main():
    total_members, _, country_drop_down, member_list = (
        member_list_tool.download_members_list()
    )
    if country_drop_down is None:
        st.error("Members list could not be loaded. Try refreshing the page.")
    elif not country_drop_down:
        st.warning("Members list is empty. Try refreshing the page.")
    else:
        st.info(f"Total Members = {total_members}")
        st.text(
            "Percentages are averages based on members by country and not per DOI by country"
        )
        st.session_state.members_with_zero_dois = {}
        member_coverage = {}
        create_selection(country_drop_down)
        st.toggle(
            "Don't include members with zero DOIs in graph and download",
            key="zero_dois",
        )
        if st.button("Run average participation report"):
            if len(st.session_state.selection) == 0:
                st.warning("Please select at least one country")
                return
            selection_dict, id_list_lengths = {}, []
            for country in st.session_state.selection:
                csv_data = {}
                ids = country_drop_down[country]
                member_coverage = get_member_prep_data_download(
                    ids, country, member_list
                )
                if st.session_state.zero_dois:
                    id_list_lengths.append(
                        len(ids) - len(st.session_state.members_with_zero_dois)
                    )
                else:
                    id_list_lengths.append(len(ids))
                member_report = collate_data_into_lists(member_coverage)
                csv_data = collect_csv_file_data(member_coverage)
                csv_data["Totals"] = csv_totals_row(csv_data)
                average_report = average_the_lists(member_report, country)
                st.write(average_report)
                selection_dict[country] = list(
                    average_report[f"average_{country}"].values()
                )
                index = average_report[f"average_{country}"].keys()
                st.download_button(
                    f"Download {country} member report",
                    data=common.results_to_csv_from_dict(csv_data, "Member ID"),
                    file_name=f"{country}_members_prep_download.csv",
                )
                if st.session_state.zero_dois:
                    st.warning(
                        f"There were {len(st.session_state.members_with_zero_dois)} members with zero DOIs left out of the graph data and download"
                    )
            display_graph(selection_dict, index, id_list_lengths)
