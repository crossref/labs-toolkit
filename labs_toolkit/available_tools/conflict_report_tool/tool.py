import streamlit as st
import requests
import pandas as pd
from bs4 import BeautifulSoup as bs
import re

DETAILS = {
    "tool-name": "Conflict Report Tool",
    "description": "Return all of the conflicts from prefix.",
}


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def main():
    prefix = st.text_input("Enter the prefix to check conflicts against", key="prefix")
    button = st.button("Check conflicts")
    if button or prefix:
        with st.spinner("Checking for conflicts..."):
            url = f"https://data.crossref.org/reports/conflictReport/{prefix}_conflicts.xml"
            r = requests.get(url)
            if r.status_code == 200:
                soup = bs(r.text, "html.parser")
                report_date = soup.find("conflict_report").get("date")
                matches = soup.find_all("conflict", attrs={"created": re.compile("\d")})
                if len(matches) < 100:
                    st.info(
                        f"-- Conflicts:  **{len(matches)}**  \n -- \
                            Report last updated:  **{report_date}**"
                    )
                    for match in matches:
                        conflict_id = match.get("id")
                        conflict_created_date = match.get("created")[:-10]
                        conflict_list = []
                        for d in match.find_all("doi"):
                            conflict_list.append(d.text)
                        with st.expander(
                            f"Conflict ID {conflict_id} - \
                                Created date: {conflict_created_date}"
                        ):
                            df = pd.DataFrame(conflict_list, columns=["Conflicts"])
                            st.table(df.assign(blank="").set_index("blank"))
                    st.markdown(
                        """<style>.streamlit-expanderHeader \
                            {font-size: large;}</style>""",
                        unsafe_allow_html=True,
                    )
                else:
                    depo_link = make_clickable(
                        f"https://data.crossref.org/reports/conflictReport/{prefix}_conflicts.xml",
                        f"Conflict report:  {prefix}",
                    )
                    st.warning(
                        "You have over 100 conflicts, please use link below instead."
                    )
                    st.write(depo_link, unsafe_allow_html=True)
            elif r.status_code == 404:
                st.warning(f"Prefix **{prefix}** does not have any conflicts")
