"""
Created on Mon Mar  7 12:27:49 2022

@author: pauldavis
"""

import streamlit as st
import requests
import datetime
import pandas as pd
import urllib.parse
from labs_toolkit.settings import API_URI, PUB_THRESHOLD, INT_THRESHOLD
import logging

# st.write(st.__version__)
logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)


DETAILS = {
    "tool-name": "API Item Downloader",
    "description": "Create your own API queries using a UI and selections"
    + ", show results in table and download to .csv file.",
}


def works():
    return "works?"


def element_lists():
    route_names = ["Works Route", "Journal Route", "Prefix Route", "Member Route"]
    select_names = [
        "DOI",
        "ISBN",
        "ISSN",
        "URL",
        "abstract",
        "accepted",
        "alternative-id",
        "approved",
        "archive",
        "article-number",
        "assertion",
        "author",
        "chair",
        "clinical-trial-number",
        "container-title",
        "content-created",
        "content-domain",
        "created",
        "degree",
        "deposited",
        "editor",
        "event",
        "funder",
        "group-title",
        "indexed",
        "is-referenced-by-count",
        "issn-type",
        "issue",
        "issued",
        "license",
        "link",
        "member",
        "original-title",
        "page",
        "posted",
        "prefix",
        "published",
        "published-online",
        "published-print",
        "publisher",
        "publisher-location",
        "reference",
        "references-count",
        "relation",
        "score",
        "short-container-title",
        "short-title",
        "standards-body",
        "subject",
        "subtitle",
        "title",
        "translator",
        "type",
        "update-policy",
        "update-to",
        "updated-by",
        "volume",
    ]
    facet_names = [
        "Choose a Facet",
        "affiliation",
        "archive",
        "assertion",
        "assertion-group",
        "category-name",
        "container-title",
        "funder-doi",
        "funder-name",
        "issn",
        "journal-issue",
        "journal-volume",
        "license",
        "link-application",
        "orcid",
        "published",
        "publisher-name",
        "relation-type",
        "ror-id",
        "source",
        "type-name",
        "update-type",
    ]
    sort_names = sorted(
        [
            "",
            "created",
            "deposited",
            "indexed",
            "is-referenced-by-count",
            "issued",
            "published",
            "published-online",
            "published-print",
            "references-count",
            "relevance",
            "score",
            "updated",
        ]
    )
    bool_filter_names = [
        "has-abstract",
        "has-affiliation",
        "has-archive",
        "has-assertion",
        "has-authenticated-orcid",
        "has-award",
        "has-clinical-trial-number",
        "has-content-domain",
        "has-description",
        "has-domain-restriction",
        "has-event",
        "has-full-text",
        "has-funder",
        "has-funder-doi",
        "has-license",
        "has-orcid",
        "has-references",
        "has-relation",
        "has-ror-id",
        "has-update",
        "has-update-policy",
        "is-update",
    ]
    date_filter_names = [
        "from-accepted-date",
        "from-approved-date",
        "from-awarded-date",
        "from-created-date",
        "from-deposit-date",
        "from-event-end-date",
        "from-event-start-date",
        "from-index-date",
        "from-issued-date",
        "from-online-pub-date",
        "from-posted-date",
        "from-print-pub-date",
        "from-pub-date",
        "from-update-date",
        "until-accepted-date",
        "until-approved-date",
        "until-awarded-date",
        "until-created-date",
        "until-deposit-date",
        "until-event-end-date",
        "until-event-start-date",
        "until-index-date",
        "until-issued-date",
        "until-online-pub-date",
        "until-posted-date",
        "until-print-pub-date",
        "until-pub-date",
        "until-update-date",
    ]
    option_filter_names = ["", "type"]
    type_names = sorted(
        [
            "book-section",
            "monograph",
            "report",
            "peer-review",
            "book-track",
            "journal-article",
            "book-part",
            "other",
            "book",
            "journal-volume",
            "book-set",
            "reference-entry",
            "proceedings-article",
            "journal",
            "component",
            "book-chapter",
            "proceedings-series",
            "report-series",
            "proceedings",
            "standard",
            "reference-book",
            "posted-content",
            "journal-issue",
            "dissertation",
            "grant",
            "dataset",
            "book-series",
            "edited-book",
            "standard-series",
        ]
    )
    return (
        route_names,
        select_names,
        facet_names,
        sort_names,
        bool_filter_names,
        date_filter_names,
        option_filter_names,
        type_names,
    )


# LISTS TO FILL
def global_lists():
    global filter_list, filename_list, url_list, facet_list, sort_list
    filter_list = []
    filename_list = ["download"]
    url_list = []
    facet_list = []
    sort_list = []


def headers():
    return {"User-Agent": "labs-toolkit-ad; mailto:support@crossref.org"}


def url_encode_params():
    return urllib.parse.urlencode(st.session_state.params, safe=":+&,*")


def data_request(url):
    url_check = requests.get(
        st.session_state.full_url, params=url_encode_params(), headers=headers()
    )
    if url_check.status_code == 200:
        st.session_state.total_results = url_check.json()["message"]["total-results"]
        if st.session_state.total_results > st.session_state.threshold:
            st.session_state.error = True
            st.warning(
                f"This tool can return result sets of no more than\
                      {st.session_state.threshold}\
                        \nThis query returns {st.session_state.total_results}"
            )
        else:
            item_list = []
            cursor = "*"
            if st.session_state.total_results >= 1000:
                st.session_state.params["cursor"] = cursor
                st.session_state.params["rows"] = 1000
                st.success("Got JSON...checking results")
                while True:
                    st.session_state.params["cursor"] = cursor
                    logger.info(f">>>> {headers()}")
                    logger.info(f">>>> {url}")
                    # st.write(st.session_state.params)
                    res = requests.get(
                        st.session_state.full_url,
                        headers=headers(),
                        params=url_encode_params(),
                    )
                    logger.info(f">>>>> {res.status_code}")
                    try:
                        body = res.json()["message"]
                        if not st.session_state.facets == "Choose a facet":
                            st.session_state.facets_list = body["facets"]
                        items = body["items"]
                        if not items:
                            break
                        cursor = str(body["next-cursor"])
                        # cursor = urllib.parse.quote(cursorRaw)
                        for i in items:
                            item_list.append(i)
                    except TypeError:
                        st.warning(
                            "The API query is not valid or no results available, \
                                please clear form and try again"
                        )
                        st.session_state.error = True
                        break
            else:
                st.session_state.params["rows"] = 1000
                try:
                    # st.session_state.params["rows"] = st.session_state.total_results
                    res = requests.get(
                        st.session_state.full_url,
                        headers=headers(),
                        params=url_encode_params(),
                    )
                    st.success("Got JSON...checking results")
                    body = res.json()["message"]
                    items = body["items"]
                    for i in items:
                        item_list.append(i)
                except TypeError:
                    st.warning(
                        "The API query is not valid or no results available, \
                            please clear form and try again"
                    )
                    st.session_state.error = True
            return item_list
    elif str(url_check.status_code).startswith("5"):
        st.session_state.error = True
        st.error(
            f"{url_check.status_code} Error returned...the API could be \
                down or the query is not valid"
        )
    else:
        st.warning(f"Error code: {url_check.status_code}")
        st.warning("Valid JSON has not been retrieved")
        st.session_state.error = True


def init_form():
    filter_list = []
    bool_filters = st.multiselect("Filters", element_lists()[4])
    if bool_filters:
        for option in bool_filters:
            boolean_option = st.radio(
                "",
                options=["true", "false"],
                key=option,
                format_func=lambda x: f"{option} {str(x)}",
            )
            if boolean_option == "false":
                choice = "f"
            else:
                choice = "t"
            filter_list.append(f"{option}:{choice}")
        st.session_state.params["filter"] = ",".join(filter_list)

    date_filters = st.multiselect("Date Filters", element_lists()[5])
    date_filter_list = []
    if date_filters:
        for date in date_filters:
            date_entry = st.date_input(f"Enter {date}", key=date)
            date_filter_list.append(f"{date}:{date_entry}")
        st.session_state.params["filter"] = ",".join(date_filter_list)

    choice_filters = st.multiselect("Choice Filters", element_lists()[6])
    if choice_filters:
        for c in choice_filters:
            if c == "type":
                choice_option = st.radio(
                    "Which type do you want to filter with",
                    element_lists()[7],
                    horizontal=True,
                )
                filter_list.append(f"type:{choice_option}")
                st.session_state.params["filter"] = ",".join(filter_list)
            else:
                pass

    query_input = st.text_input("Query Text", max_chars=100)
    if query_input:
        st.session_state.params["query"] = query_input

    st.subheader("Facets & sorting")
    col1, col2 = st.columns([1, 1])
    # with col1.expander("Facets"):
    facet = col1.selectbox(
        "Check the Facet you would like to use",
        element_lists()[2],
        key="facets",
        label_visibility="collapsed",
    )

    with col2.expander("Sort"):
        st.radio(
            "What would you like to sort by",
            element_lists()[3],
            key="sorting",
            horizontal=True,
        )
        st.select_slider(
            "Select the order of the sort",
            options=["Ascending", "Descending"],
            key="order",
        )
    st.subheader("Selects")
    selects = st.multiselect(
        "Select the elements to retrieve", element_lists()[1], key="selects"
    )
    if selects:
        st.session_state.params["select"] = ",".join(selects)

    if facet == "Choose a Facet":
        pass
    else:
        st.session_state.params["facet"] = f"{facet}:%2A"

    if st.session_state.sorting:
        if st.session_state.sorting[0] == "":
            pass
        else:
            sorting_url = "sort=" + st.session_state.sorting
            if st.session_state.order == "Ascending":
                order_url = "order=asc"
            else:
                order_url = "order=desc"
            url_list.append(sorting_url)
            url_list.append(order_url)


def route_builder(route_name, route_value):
    st.session_state.full_url = f"{API_URI}{route_name}/{route_value}/{works()}"


def route_option():
    if st.session_state.route == "Journal Route":
        st.subheader("Journal route selected, add an ISSN below")
        issn = st.text_input("ISSN", help="Enter a valid ISSN here")
        if issn:
            route_builder("journals", issn)
            filename_list.append("journal" + issn)

    if st.session_state.route == "Prefix Route":
        st.subheader("Prefix route selected, add a prefix below")
        prefix = st.text_input("Prefix", help="Enter a Crossref prefix here")
        if prefix:
            route_builder("prefixes", prefix)
            filename_list.append("prefix_" + prefix)

    if st.session_state.route == "Member Route":
        st.subheader("Member route selected, add a memberID below")
        mem_id = st.text_input("Member ID", help="Enter a Crossref memberID here")
        if mem_id:
            route_builder("members", mem_id)

    if st.session_state.route == "Works Route":
        st.subheader("Works route selected")
        url = "https://api.crossref.org/works?"
        if "full_url" not in st.session_state:
            st.session_state.full_url = url


def select_url(names):
    select_list = []
    for name in names:
        select_list.append(name)
    return select_list


def facet_url(names):
    facet_list = []
    for name in names:
        facet_list.append(name + ":%2A")
    return facet_list


def convert_output(output):
    data = pd.DataFrame(output)
    csv_data = data.to_csv(index=False, encoding="utf-8")
    return csv_data


def determine_threshold():
    if st.session_state.login_type in ["public", "member"]:
        st.session_state.threshold = PUB_THRESHOLD
    elif st.session_state.login_type == "internal":
        st.session_state.threshold = INT_THRESHOLD


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def toast():
    st.toast("File Downloaded")


def main():
    st.session_state.error = False
    st.session_state.total_results = 0
    determine_threshold()
    global_lists()
    st.markdown("***")
    st.session_state.route = st.radio(
        "Which API route to use", element_lists()[0], horizontal=True
    )
    st.session_state.params = dict()
    route_option()
    init_form()
    st.markdown(
        "Enter a query string to search against in the API. \
            Each word will be searched against individually"
    )
    col1, col2, col3 = st.columns([1, 1, 1])
    run_query = col2.button("Run API Query")
    if run_query:
        with st.spinner("Retrieving your JSON..."):
            res = requests.get(
                st.session_state.full_url, headers=headers(), params=url_encode_params()
            )
            st.write(f"Here is link to the API query you are running: {res.url}")
            output = data_request(res.url)
            if not st.session_state.error:
                if not st.session_state.facets == "Choose a Facet":
                    with st.expander("Facet section of your results"):
                        st.json(st.session_state.facets_list)
                if not st.session_state.total_results > st.session_state.threshold:
                    st.subheader("Your results are here")
                    with st.expander(f"Results : {st.session_state.total_results}"):
                        st.json(output)
                    filename = "_".join(filename_list) + "_" + timestamp() + ".csv"
                    data = convert_output(output)
                    coll, colm, colr = st.columns([1, 1, 1])
                    colm.download_button(
                        "Download Ouput",
                        data,
                        mime="text/csv",
                        file_name=filename,
                        on_click=toast,
                    )
