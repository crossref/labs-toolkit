import streamlit as st
import datetime
import pdfplumber

DETAILS = {
    "tool-name": "Quarterly Reports Downloader PDF",
    "description": "Add the PDF file from the invoice to return all of the Quarterly reports against it.",
}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def extract_data(feed):
    data = []

    with pdfplumber.open(feed) as pdf:
        pages = pdf.pages
        for p in pages:
            items = p.extract_table()
            if items:
                p.extract_text()
                data.append(p.chars)
                search_invoice(items)


def search_invoice(item_lists):
    item_codes = [
        "40801",
        "40802",
        "40870",
        "40851",
        "40850",
        "40821",
        "40816",
        "40830",
        "40861",
    ]
    # progress = st.empty()
    counter = 0
    for item_list in item_lists:
        counter += 1
        if any(item in item_codes for item in item_list):
            # progress.write(f"Working through line {counter} of {len(item_lists)}")
            # item_id, info_line, quantity, each_price, total_row_price = (
            info_line = (
                # item_list[0],
                item_list[1],
                # item_list[3],
                # item_list[4],
                # item_list[5],
            )
            split_info_line(info_line)


def split_info_line(line):
    un_exlude = ["CY", "BY", "Book", "Chapters", "Titles"]
    list_from_line = line.replace("(", "").replace(")", "").split(":")
    # st.write(list_from_line)
    prefix = list_from_line[1].strip()
    if prefix not in st.session_state.prefixes:
        st.session_state.prefixes.append(prefix)
    username = list_from_line[-1].strip()
    username_list = username.split(" ")
    date = list_from_line[0].split("/")
    month = date[0][-2:]
    year = date[-1].strip(" ")
    full_date, quarter = full_date_func(month, year)
    for u in username_list:
        u = u.strip()
        if any(x in u for x in un_exlude):
            pass
        else:
            create_urls(u, full_date, quarter)


def create_urls(username, date, quarter):
    url = f"http://qs3.crossref.org:8085/depreport?mode=titlesByQuarter&user={username}&month={date}&format=csv"
    if url in st.session_state.file_urls:
        pass
    else:
        st.session_state.file_urls[
            f"Quarterly Report for {username} {quarter} {date[:4]}"
        ] = url


def full_date_func(month, year):
    if month in ["01", "02", "03"]:
        date = f"{year}-02-01"
        quarter = "Q1"
    elif month in ["04", "05", "06"]:
        date = f"{year}-05-01"
        quarter = "Q2"
    elif month in ["07", "08", "09"]:
        date = f"{year}-08-01"
        quarter = "Q3"
    else:
        date = f"{year}-11-01"
        quarter = "Q4"
    return date, quarter


def main():
    st.session_state.prefixes = []
    st.session_state.file_urls = {}
    st.session_state.reports = []
    st.info(
        'Make sure that you are logged into the\
              VPN before clicking **"Get Reports"**'
    )
    file_upload = st.file_uploader(
        "Upload the PDF invoice to generate Quarterly Report Link",
        key="file_upload",
        accept_multiple_files=False,
        type="pdf",
    )
    get_reports = st.button("Get Reports")
    if get_reports:
        with st.spinner("Trying to get report links..."):
            if file_upload is not None:
                # zip_name = f"quarterly_reports_dl_{timestamp()}.zip"
                # try:
                # 	with ZipFile(zip_name, 'w') as zip_file:
                extract_data(file_upload)
                with st.expander("The reports needed from the PDF are:", expanded=True):
                    st.warning(
                        "**Check you are logged onto the VPN before clicking link**"
                    )
                    for data, url in st.session_state.file_urls.items():
                        st.markdown(f"[{data}]({url})")
                    st.text("The list of prefixes in the invoice are")
                    for p in st.session_state.prefixes:
                        st.text(p)
