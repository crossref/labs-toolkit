import streamlit as st
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup as bs
from labs_toolkit.settings import ADMIN_BASE_URL

DETAILS = {
    "tool-name": "Membership Tools",
    "description": "Tools for the membership team.",
}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def credentials():
    return {
        "usr": f"{st.session_state.email}/root",
        "pwd": st.session_state.password,
    }


def dict_convert_to_table(results, columns: list, column_name):
    pd.set_option("display.colheader_justify", "center")
    df = pd.DataFrame.from_dict(results, orient="index", columns=columns)
    df.columns.rename("ID", inplace=True)
    return df


def headers():
    return {"User-Agent": "labs-toolkit-rtl; mailto:support@crossref.org"}


def format_id(id):
    id = id.replace("-", "").replace(" ", "")
    return id


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "date-parts":
            data = datetime.date(*v[0])
    return data


def data_to_csv(results):
    df = pd.DataFrame.from_dict(
        results,
        orient="index",
        columns=[
            "Title",
            "Cite ID",
            "Total DOIs",
            "Type",
            "Depositor Report",
            "Owner prefix(admin)",
        ],
    )
    df.index.name = "ISSN/ISBN"
    csv_data = df.to_csv(encoding="utf-8")
    return csv_data


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def get_depo_list(r):
    df = pd.DataFrame([x.split() for x in r.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    owner_prefixes = list(df["OWNER"])
    dois = list(df["DOI"])
    if len(owner_prefixes) > 2:
        del owner_prefixes[0:1]
        del dois[0:1]
        owner_prefixes.remove(None)
        dois.remove(None)
        return dois
    else:
        error = "That doesn't look like a valid cite ID, any DOIs against \
            the title or an ID from a deleted title. Please try a different ID"
        return error


def search_title_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def get_title(id, admin_code):
    data_list = []
    r = requests.post(
        f"{ADMIN_BASE_URL}/metadataAdmin?func=search",
        data=id_returns_params(id, admin_code) | credentials(),
    )
    page = bs(r.text, "html.parser")
    total_results = page.find("font", attrs={"color": "red"}).text
    if total_results != "0":
        for f in page.find_all("td", attrs={"class": "bafsa"}):
            data_list.append(f.text)
        try:
            cite_id = (data_list[0]).strip()
            title = data_list[1].strip()
            if admin_code == "0":
                prefix = data_list[5].strip()
            else:
                prefix = data_list[7].strip()
            return title, cite_id, prefix
        except IndexError:
            title = "no title"
            cite_id = f"Check ID {id}"
            prefix = "no prefix returned"
        return title, cite_id, prefix
    else:
        title = "not registered"
        cite_id = f"Check ID {id}"
        prefix = "not registered"
        return title, cite_id, prefix


def sort_title_dictionary(id, id_type, depo_report_type, admin_code):
    title, st.session_state.cite_id, prefix = get_title(id, admin_code)
    if title == "not registered":
        st.session_state.params_dict[id] = [
            title,
            f"{depo_report_type}{st.session_state.cite_id}",
            "0",
            id_type,
            "Not registered",
            prefix,
            "0",
            admin_code,
        ]
    else:
        total_dois, depo_report_link = check_id(
            f"{depo_report_type}{st.session_state.cite_id}"
        )
        st.session_state.params_dict[id] = [
            title,
            f"{depo_report_type}{st.session_state.cite_id}",
            total_dois,
            id_type,
            prefix,
            get_pub_id(prefix),
            admin_code,
        ]
    return st.session_state.params_dict


def get_elements(rows):
    del rows[0]
    return select_pub_id([item.find_all("td") for item in rows])


def get_table_rows(table):
    return get_elements([(row) for row in table("tr")])


def select_pub_id(rows):
    return rows[0][1].text.strip()


def get_pub_id(prefix):
    r = requests.post(
        f"{ADMIN_BASE_URL}/publisherUserAdmin",
        params=search_title_params(prefix) | credentials(),
    )
    return get_table_rows(bs(r.text, "html.parser").find_all("table")[5])


def get_title_params(pub_id, btype):
    return {
        "start": "0",
        "size": "20",
        "sf": "search",
        "type": "Search",
        "depositor": pub_id,
        "btype": btype,
        "searchType": "contains",
        "search": "Search",
        "maxResultSize": "30",
    }


def sort_book_journal(admin_code):
    return "J" if admin_code == "0" else "B"


def collect_titles(title_block):
    title_details = []
    del title_block.contents[::2]
    try:
        if (
            "DELETED" in title_block.contents[1].text
            or "(DELETED)" in title_block.contents[1].text
        ):
            pass
        else:
            for index, item in enumerate(title_block.contents):
                if index == 0:
                    cite_id = item.text.strip()
                elif index in [2, 3]:
                    title_details.append(format_id(item.text.strip()))
                elif index in [1, 4, 5]:
                    title_details.append(item.text.strip())
                elif index in [6]:
                    cite_id_precursor = sort_book_journal(item.text)
                    if st.session_state.select_title:
                        title_details.append(
                            f"{sort_book_journal(st.session_state.select_title[5])}{cite_id}"
                        )
                    else:
                        title_details.append(
                            f"{sort_book_journal(st.session_state.params_dict[format_id(st.session_state.id_input)][6])}{cite_id}"
                        )
                    title_details.append(check_id(f"{cite_id_precursor}{cite_id}")[0])
                st.session_state.columns = [
                    "Title",
                    "pISSN",
                    "eISSN",
                    "pISBN",
                    "eISBN",
                    "Cite ID",
                    "No of DOIs",
                ]
    except IndexError:
        pass
    return title_details


def id_check(id):
    if len(id) > 8:
        id_type = "ISBN"
        depo_report_type = "B"
        admin_code = "101"
    else:
        id_type = "ISSN"
        depo_report_type = "J"
        admin_code = "0"
    return id, id_type, depo_report_type, admin_code


def check_id(cite_id):
    r = requests.get(f"https://data.crossref.org/depositorreport?pubid={cite_id}")
    if r.status_code == 200:
        total_dois = get_depo_list(r.text)
        return len(total_dois), r.url
    elif r.status_code == 404:
        total_dois = "Not registered"
        depo_link = "Not registered"
        return total_dois, depo_link
    elif r.status_code == 504:
        st.warning("The API could be down, try again shortly")
        pass


def get_title_rows(table):
    title_list = []
    title_rows = [(row) for row in table("tr")]
    del title_rows[0:2]
    for row in title_rows:
        title_list.append(collect_titles(row))
    del title_list[-1]
    df = pd.DataFrame(title_list, columns=st.session_state.columns)
    df.index += 1
    return df


def get_titles(pub_id, admin_code):
    r = requests.post(
        f"{ADMIN_BASE_URL}/metadataAdmin?func=search",
        params=get_title_params(pub_id, admin_code) | credentials(),
    )
    soup = bs(r.text, "html.parser")
    titles = soup.find_all("table")[4]
    st.info("Here are the other titles against the publisher (Max results 30)")
    st.table(get_title_rows(titles))


def id_returns_params(id: str, admin_code: str):
    return {
        "start": "0",
        "size": "20",
        "func": "modify",
        "issns": id,
        "btype": admin_code,
        "search": "Search",
        "searchType": "contains",
    }


def prefix_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def get_member_details(member_id):
    pub_details = []
    prefixes = find_prefixes(member_id)
    for prefix in prefixes:
        r = requests.post(
            "https://doi.crossref.org/servlet/publisherUserAdmin",
            params=prefix_params(prefix) | credentials(),
        )
        soup = bs(r.text, "html.parser")
        for table in soup.find_all("tr")[4]:
            row = table.find_all("td")
            pub_id = row[1].text.split('">')[0].strip()
            prefix = row[2].contents.pop(0)["value"]
            publisher_name = row[3].contents.pop(0)["value"]
            # st.write(f"Pub ID: {pub_id}  \n  Prefix: \
            # {prefix}  \n  Publisher Name: {publisher_name}")
            pub_details.append([pub_id, prefix, publisher_name])
    return pub_details


def find_prefixes(member_id):
    r = requests.get(f"https://api.crossref.org/members/{member_id}").json()["message"]
    prefixes = r["prefixes"]
    return prefixes


def create_table(data, columns):
    df = pd.DataFrame(data, columns=columns)
    df.index += 1
    return df


def data_to_csv2(results):
    csv_data = results.to_csv(encoding="utf-8")
    return csv_data


def cite_returns_params(id: str, admin_code: str):
    return {
        "start": "0",
        "size": "20",
        "func": "modify",
        "citeId": id,
        "btype": admin_code,
        "search": "Search",
        "searchType": "contains",
    }


def title_details(table):
    return [item.find_all("input") for item in table]


def refine_title_details(title_list, admin_code):
    title_record = []
    if title_list:
        for item in title_list:
            title_record.append(item["value"])
        title_record.append(admin_code)
        if admin_code == "101":
            return list(title_record[i] for i in [5, 6, 7, 9, 11, 17])
        else:
            return list(title_record[i] for i in [6, 7, 8, 10, 12, 18])
    else:
        st.warning("No title match")


def get_title_from_cite_id(id, admin_code):
    r = requests.post(
        f"{ADMIN_BASE_URL}/metadataAdmin?func=search",
        data=cite_returns_params(id, admin_code) | credentials(),
    )
    return refine_title_details(
        title_details(bs(r.text, "html.parser").find_all("table"))[4], admin_code
    )


def cite_id_check(id, admin_code):
    depo_report_type = "B" if admin_code == "101" else "J"
    st.session_state.params_dict[f"{depo_report_type}{id}"] = get_title_from_cite_id(
        id, admin_code
    )


def main():
    st.divider()
    st.markdown("##### Registered Title Lookup")
    col1, col2, col3 = st.columns([4, 2, 2])
    title = col1.text_input("Title - Still in development", disabled=True)
    id_input = col2.text_input("ISSN/ISBN", key="id_input")
    cite_id = col3.text_input("Cite ID")
    st.session_state.params_dict = {}
    if id_input:
        st.session_state.select_title = None
        columns = [
            "Title",
            "Cite ID",
            "Total DOIs",
            "Type",
            "Owner prefix(admin)",
            "Pub ID",
            "Admin Code",
        ]
        results = (
            sort_title_dictionary(*id_check(format_id(id_input))),
            columns,
            "ISSN/ISBN",
        )
        st.table(dict_convert_to_table(results))
        get_titles(
            st.session_state.params_dict[format_id(id_input)][5],
            st.session_state.params_dict[format_id(id_input)][6],
        )
    if title:
        # TODO still need to work on the title search
        # functionality. Leaving for page aesthetics.
        pass
    if cite_id:
        cite_id_check(cite_id, "0")
        cite_id_check(cite_id, "101")
        st.text("Here are the titles returned from the Cite ID")
        st.table(
            dict_convert_to_table(
                st.session_state.params_dict,
                [
                    "Title",
                    "eISSN/ISBN",
                    "pISSN/ISBN",
                    "Title DOI",
                    "Owner Code",
                    "Admin Code",
                ],
                "Cite ID",
            )
        )
    if len(st.session_state.params_dict) > 1:
        select_title = st.selectbox(
            label="Which title are you looking for",
            options=st.session_state.params_dict.values(),
            key="select_title",
            format_func=lambda x: x[0],
        )
        if select_title:
            st.write(st.session_state.select_title)
            get_titles(
                st.session_state.select_title[4], st.session_state.select_title[5]
            )

    st.divider()
    st.markdown("##### Return Member Account name from member ID")
    member_id = st.text_input("Enter member ID")
    button = st.button("Find member establishments")
    if button:
        results = create_table(
            get_member_details(member_id), ["Publisher ID", "Prefix", "Publisher Name"]
        )
        st.table(results)
        st.download_button(
            "Download Prefix List",
            data_to_csv2(results),
            file_name=f"publisher_name_list_{member_id}.csv",
        )
