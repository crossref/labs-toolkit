import streamlit as st
import requests
import datetime
import xml.etree.cElementTree as ET
import random
import string

DETAILS = {
    "tool-name": "Delete DOIs",
    "description": "Transfer DOIs and update the metadata against them.",
}


def build_xml(dois):
    doi_batch = {
        "xmlns": "http://www.crossref.org/schema/4.4.2",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "xsi:schemaLocation": "http://www.crossref.org/schema/4.4.2 https://www.crossref.org/schemas/crossref4.4.2.xsd",
        "xmlns:jats": "http://www.ncbi.nlm.nih.gov/JATS1",
        "version": "4.4.2",
    }

    num = random.randrange(1, 10**10)
    batchID_digits = str(num).zfill(6)
    batchID_letters = random.choices(string.ascii_lowercase, k=6)
    doi_bat_id = "".join(batchID_letters) + "-" + batchID_digits
    doi_bat = ET.Element("doi_batch", doi_batch)
    head = ET.SubElement(doi_bat, "head")
    timestamp = "{:%Y%m%d%H%M%S%S%j}".format(datetime.datetime.now())
    ET.SubElement(head, "doi_batch_id").text = doi_bat_id
    ET.SubElement(head, "timestamp").text = timestamp
    depositor = ET.SubElement(head, "depositor")
    ET.SubElement(depositor, "depositor_name").text = st.session_state.email
    ET.SubElement(depositor, "email_address").text = st.session_state.email
    ET.SubElement(head, "registrant").text = "creftest"
    body = ET.SubElement(doi_bat, "body")
    tree = ET.ElementTree(doi_bat)
    journal = ET.SubElement(body, "journal")
    journal_metadata = ET.SubElement(journal, "journal_metadata", language="en")
    ET.SubElement(journal_metadata, "full_title").text = (
        "CrossRef Listing of Deleted DOIs"
    )
    ET.SubElement(journal_metadata, "abbrev_title").text = (
        "CrossRef Listing of Deleted DOIs"
    )
    ET.SubElement(journal_metadata, "issn", media_type="print").text = "0849-6757"
    for doi in dois:
        journal_article = ET.SubElement(
            journal, "journal_article", publication_type="full_text"
        )
        titles = ET.SubElement(journal_article, "titles")
        a_title = ET.SubElement(titles, "title")
        a_title.text = doi
        publication_date_a_p = ET.SubElement(
            journal_article, "publication_date", media_type="print"
        )
        ET.SubElement(publication_date_a_p, "year").text = "2000"
        doi_data = ET.SubElement(journal_article, "doi_data")
        ET.SubElement(doi_data, "doi").text = doi
        ET.SubElement(doi_data, "resource").text = (
            "http://www.crossref.org/deleted_DOI.html"
        )
        ET.SubElement(journal_article, "citation_list")
    st.session_state.xml = tree.write(
        st.session_state.filename, encoding="UTF-8", xml_declaration=True
    )
    with open(st.session_state.filename, "r") as f:
        file = {"fname": f}
        r = requests.post(
            "https://doi.crossref.org/servlet/deposit",
            files=file,
            data=st.session_state.xml_credentials,
        )
        if r.status_code == 200:
            st.success(f"{f.name} XML file was successfully sent to the queue")
        else:
            st.warning(f"{f.name} XML file was not sent to the queue")


def form_data(email, password, operation, role):
    credentials = {
        "operation": operation,
        "login_id": f"{email}/{role}",
        "login_passwd": password,
    }
    return credentials


def upload_types_dict():
    return {"metadata upload": "doMDUpload", "title transfer": "doTransferDOIsUpload"}


def timestamp_now():
    return "{:%Y-%m-%d}".format(datetime.datetime.now())


def download_xml(xml):
    return st.download_button("Download XML", xml, file_name=st.session_state.filename)


def headers():
    return {"User-Agent": "labs-toolkit-del-dois; mailto:support@crossref.org"}


def run_transfer_file(from_prefix, dois):
    header = (
        f"H:email={st.session_state.email};fromPrefix={from_prefix};toPrefix=10.5555"
    )
    f = open(f"{st.session_state.ticket_number}_delete_dois_transfer.txt", "w")
    f.write(header + "\n")
    for doi in dois:
        f.write(str(doi) + "\n")
    with open(f.name, "r") as f:
        file = {"fname": f}
        r = requests.post(
            "https://doi.crossref.org/servlet/deposit",
            files=file,
            data=st.session_state.transfer_credentials,
        )
        if r.status_code == 200:
            st.success(f"{f.name} transfer file was successfully sent to the queue")
        else:
            st.warning(f"{f.name} transfer file was not sent to the queue")


def split_dois(dois):
    dois = dois.split()
    return dois


def create_xml_file(xml_data):
    with open(st.session_state.filename, "w") as f:
        f.write(xml_data)
    return f


def main():
    tool_body = st.container()
    tool_body.text_area("Enter the DOIs here", key="doi_list")
    tool_body.text_input("Enter the ticket number", key="ticket_number")
    delete_dois = tool_body.button("Delete DOIs")
    if delete_dois:
        dois = split_dois(st.session_state.doi_list)
        for doi in dois:
            try:
                r = requests.get("http://api.crossref.org/works/" + doi.strip()).json()[
                    "message"
                ]
                from_prefix = r["prefix"]
            except Exception:
                st.warning(
                    f"There is no metadata showing in the REST API {r.url}.  \n\
                        Please check this DOI {doi} and try again."
                )
            st.session_state.transfer_credentials = form_data(
                st.session_state.email,
                st.session_state.password,
                "doTransferDOIsUpload",
                "root",
            )

        run_transfer_file(from_prefix, dois)
        st.session_state.filename = (
            f"{st.session_state.ticket_number}_delete_dois_xml_{timestamp_now()}.xml"
        )
        st.session_state.xml_credentials = form_data(
            st.session_state.email, st.session_state.password, "doMDUpload", "creftest"
        )
        build_xml(dois)
