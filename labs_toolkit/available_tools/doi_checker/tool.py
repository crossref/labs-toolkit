import streamlit as st
import requests
import datetime
import pandas as pd
import time

DETAILS = {
    "tool-name": "DOI checker",
    "description": "Check various things against a list of DOIs (DOIs registered? Who is the owner prefix? Are the DOIs current year or back year.",
}


works_url = "works?"
nl = "\n"


# EMPTY LISTS
filename_list = []


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    return df


def headers():
    return {"User-Agent": "labs-toolkit-dc; mailto:support@crossref.org"}


def split_dois(dois):
    dois = dois.split()
    return dois


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "date-parts":
            data = datetime.date(*v[0])
    return data


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def report_query(dois, report):
    if report == "Registered DOI Check":
        st.session_state.report = "reg_doi"
        doi_check(dois)
    elif report == "DOI owner check":
        st.session_state.report = "owner_check"
        owner_check(dois)
    elif report == "Current Year or Back Year":
        st.session_state.report = "cy_by"
        cy_by_check(dois)
    elif report == "Alias Check":
        st.session_state.report = "alias_check"
        alias_check(dois)


def doi_check(dois):
    output = []
    successful = []
    counter = 0
    progress = st.empty()
    for doi in dois:
        counter += 1
        progress.info(f"Checking DOI **{counter}** of **{len(dois)}**")
        time.sleep(0.1)
        api_call = "https://doi.org/api/handles/" + doi
        results = requests.get(api_call)
        if results.status_code == 404:
            output.append(doi)
        elif results.status_code == 200:
            successful.append(doi)
    st.info(
        f"Out of {len(dois)} DOIs  \n{len(successful)} \
            registered  \n{len(output)} not registered."
    )
    results_list(
        "Not registered", [output, successful], ["Not registered", "Registered"]
    )


def alias_check(dois):
    output = []
    counter = 0
    progress = st.empty()
    for doi in dois:
        counter += 1
        progress.info(f"Checking DOI **{counter}** of **{len(dois)}**")
        try:
            api_call = f"https://doi.org/api/handles/{doi}"
            results = requests.get(api_call).json()["values"]
            for value in results:
                st.json(value)
                type = value["type"]
                if type == "HS_ALIAS":
                    alias_doi = value["data"]["value"]
                    st.write(alias_doi)
                    output.append([doi, alias_doi])
        except ValueError:
            alias_doi = "No alias"
            output.append([doi, alias_doi])
    results_list("Alias Checker", output, ["Prefix"])


def owner_check(dois):
    output = []
    counter = 0
    progress = st.empty()
    for doi in dois:
        counter += 1
        progress.info(f"Checking DOI **{counter}** of **{len(dois)}**")
        try:
            api_call = "http://api.crossref.org/works/" + doi
            results = requests.get(api_call).json()["message"]
            owner = results["prefix"]
            output.append([doi, owner])
        except ValueError:
            owner = "Not registered"
            output.append([doi, owner])
    results_list("Owner Check DOIs", output, ["Prefix"])


def cy_by_check(dois):
    output = []
    cy = []
    by = []
    counter = 0
    progress = st.empty()
    for doi in dois:
        counter += 1
        progress.info(f"Checking DOI **{counter}** of **{len(dois)}**")
        try:
            time.sleep(0.1)
            api_call = "http://api.crossref.org/works/" + doi
            results = requests.get(api_call, headers=headers()).json()["message"]
            published = results["published"]["date-parts"][0][0]

            # print(published)
            if published >= 2020:
                reg_fee = "Current Year"
                cy.append([doi, published])
            else:
                reg_fee = "Back Year"
                by.append([doi, published])
            output.append([doi, str(published), reg_fee])
        except ValueError:
            reg_fee = "Not registered"
            published = "Not registered"
            output.append([doi, published, reg_fee])
    if len(cy) > 0:
        results_list("Current Year", cy, ["DOI", "Publish Date"])
    else:
        with st.expander("No Current Year DOIs to display"):
            st.write("No data to show")
    if len(by) > 0:
        results_list("Back Year", by, ["DOI", "Publish Date"])
    else:
        with st.expander("No Back Year DOIs to display"):
            st.write("No data to show")
    with st.spinner("Getting the data..."):
        results_list(
            "Current or Back Year",
            output,
            ["DOI", "Publish Year", "Current or Back Year"],
        )


def results_list(tag, data, columns):
    with st.expander(f"{tag} DOIs: {len(st.session_state.dois)}"):
        df = pd.DataFrame(data)
        if st.session_state.report == "reg_doi":
            df = df.transpose()
            df.columns = columns
        elif st.session_state.report == "cy_by":
            df.columns = columns
        df.index += 1
        st.dataframe(df)
        st.download_button(
            f"📥  Download {tag} DOIs",
            data_to_csv(data),
            mime="text/csv",
            file_name=f"{tag}_dois_{timestamp}",
            key=f"{tag}",
        )


def reports():
    reports_list = (
        "Registered DOI Check",
        "DOI owner check",
        "Current Year or Back Year",
        "Alias Check",
    )
    st.radio(
        "What report should we run",
        options=reports_list,
        key="report_select",
        horizontal=True,
    )


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def main():
    dois_input = st.text_area(
        "Enter DOIs here",
        placeholder="Example value in help icon ❓ ",
        help="https://api.crossref.org/members?rows=10",
    )
    if dois_input:
        st.session_state.dois = split_dois(dois_input)
        reports()
        if len(st.session_state.report_select) > 0:
            run_tool = st.button("Run the report")
            if run_tool:
                with st.spinner("Running the report..."):
                    report_query(st.session_state.dois, st.session_state.report_select)
        else:
            st.write(
                "No data requested for return, \
                    please select elements from list and try again"
            )
