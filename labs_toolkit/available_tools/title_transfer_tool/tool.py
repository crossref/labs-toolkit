import streamlit as st
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup as bs
import re
from labs_toolkit.settings import (
    PROD_DEPOSIT_URL,
    TEST_ADMIN_BASE_URL,
    ADMIN_BASE_URL,
    TEST_DEPOSIT_URL,
)
import copy
import time

DETAILS = {
    "tool-name": "Title transfer tool",
    "description": "Use to help transfer multiple titles at once "
    + "from the same prefix to a new prefix",
}


content_type_codes = {
    "Book": "101",
    "Conference Proceeding": "102",
    "Disseratation": "103",
    "Report": "104",
    "Standard": "105",
    "Database": "106",
    "Conference Series": "107",
    "Book Series": "108",
    "Report Series": "109",
    "Standard Series": "110",
    "Book Set": "111",
    "Posted Content": "112",
    "Grants": "113",
}


def credentials():
    return {
        "usr": f"{st.session_state.email}/root",
        "pwd": f"{st.session_state.password}",
    }


def form_data():
    return {
        "operation": "doTransferDOIsUpload",
        "login_id": f"{st.session_state.email}/root",
        "login_passwd": st.session_state.password,
    }


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8", index=False)
    return csv_data


def timestamp_now():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def convert_from_dict(data, columns, index_name):
    df = pd.DataFrame.from_dict(data, orient="index", columns=columns)
    df.index.name = index_name
    return df


def convert_from_dict2(data, index_name):
    df = pd.DataFrame.from_dict(data, orient="index")
    df.index.name = index_name
    return df


def headers():
    return {
        "User-Agent": "labs-toolkit-ttt; mailto=support@crossref.org",
        "Content-Type": "application/x-www-form-urlencoded",
    }


def as_params(data):
    return "&".join([f"{k}={v}" for k, v in data.items()])


def sort_content_type(content_type):
    return "0" if content_type == "journal" else "1"


def split_dois(dois):
    dois = dois.split()
    return dois


def split_ids(ids):
    ids = ids.replace("-", "").split()
    return ids


def get_depo_list(cite_id, content_type):
    r = requests.get(
        "https://data.crossref.org/depositorreport?"
        + f"pubid={(content_type[0]).upper()}{cite_id}"
    ).text
    df = pd.DataFrame([x.split() for x in r.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    owner_prefixes = list(df["OWNER"])
    dois = list(df["DOI"])
    if len(owner_prefixes) > 2:
        del owner_prefixes[0:1]
        del dois[0:1]
        owner_prefixes.remove(None)
        dois.remove(None)
        return dois
    else:
        error = "That doesn't look like a valid cite ID, any DOIs against \
            the title or an ID from a deleted title. Please try a different ID"
        return error


def title_params(cite_id: str, content_type_id):
    return {
        "start": "0",
        "size": "20",
        "func": "modify",
        "citeId": cite_id,
        "btype": content_type_id,
    }


def get_params(cite_id, content_type):
    r = requests.post(
        f"{test_or_prod(st.session_state.environment)}/metadataAdmin?func=search",
        data=title_params(cite_id, content_type_codes.get(content_type))
        | credentials(),
    )
    page = bs(r.text, "html.parser")
    for f in page.find_all("input"):
        name = f.get("name")
        value = f.get("value")
        if name is not None:
            if cite_id not in st.session_state.params_dict.keys():
                st.session_state.params_dict[cite_id] = {name: value}
            else:
                st.session_state.params_dict[cite_id].update({name: value})
        st.session_state.params_dict[cite_id]["mb"] = "Modify"
    (
        st.session_state.params_dict[cite_id]["current prefix"],
        st.session_state.params_dict[cite_id]["current publisher name"],
    ) = get_prefix_publisher_from_depositor_id(
        st.session_state.params_dict[cite_id]["depositor"]
    )
    st.session_state.params_dict[cite_id] = {
        k: v for k, v in st.session_state.params_dict[cite_id].items() if v
    }


def publisher_params(depositor_id):
    return {
        "start": "0",
        "size": "20",
        "publisherID": depositor_id,
        "customerID": "",
        "sf": "search",
    }


def get_prefix_publisher_from_depositor_id(depositor_id):
    r = requests.get(
        f"{test_or_prod(st.session_state.environment)}/publisherUserAdmin",
        params=publisher_params(depositor_id) | credentials(),
    )
    soup = bs(r.text, "html.parser")
    for table in soup.find_all("tr")[4]:
        row = table.find_all("td")
        prefix = row[2].contents.pop(0)["value"]
        publisher_name = row[3].contents.pop(0)["value"]
    return prefix, publisher_name


def test_or_prod(env):
    if env == "Production":
        return ADMIN_BASE_URL
    else:
        return TEST_ADMIN_BASE_URL


def test_or_prod_deposit(env):
    if env == "Production":
        return PROD_DEPOSIT_URL
    else:
        return TEST_DEPOSIT_URL


def find_ids(dois):
    progress = st.empty()
    counter = 0
    for doi in dois:
        counter += 1
        try:
            r = requests.get(
                "http://doi.crossref.org/search/doi?pid=support@crossref.org"
                + f"&format=unixsd&doi={doi}"
            )
            soup = bs(r.text, "html.parser")
            for c_type in soup.crossref:
                if isinstance(c_type.name, str):
                    if c_type.name in ["report-paper", "report", "book", "book-series"]:
                        c_type.name = "book"
                    else:
                        id = soup.find(
                            "crm-item", attrs={"name": f"{c_type.name}-id"}
                        ).text
                        st.session_state.doi_list.extend(get_depo_list(id, c_type.name))
                        st.session_state.key_letter = c_type.name[0]
                        st.session_state.content_type = c_type.name
                        get_params(id, c_type.name)
        except TypeError:
            id = "DOI not registered"
            title = "DOI not registered"
            st.session_state.params_dict[id] = {"title": title, "doi": doi}
        progress.success(f"Processing {counter}/{len(dois)}")
    with st.expander(f"The length of the DOI list is {len(st.session_state.doi_list)}"):
        st.write(st.session_state.doi_list)
    st.download_button(
        "Download DOI list",
        data_to_csv(st.session_state.doi_list),
        mime="text/csv",
        file_name=f"doi_list_{timestamp_now()}.csv",
    )


def search_title_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def post_file(f):
    st.write("Posting file - fingers crossed :fingers-crossed")
    with open(f.name, "r") as f2:
        myfile = {"fname": f2}
        r = requests.post(
            test_or_prod_deposit(st.session_state.environment),
            files=myfile,
            data=form_data(),
        )
        if r.status_code == 200:
            st.success(f"**{f.name}** was successfully sent to the queue")
        else:
            st.warning(
                f"The file **{f.name}** has failed to reach the queue, \
                    error **{r.status_code}**, please try again later"
            )


def transfer_dois():
    header = (
        f"H:email={st.session_state.email};type=transfer_owner;"
        + f"fromPrefix={st.session_state.from_prefix};toPrefix={st.session_state.to_prefix}"
    )
    with open(
        f"transfer_from_{st.session_state.from_prefix}_to_{st.session_state.to_prefix}.txt",
        "w",
    ) as f:
        f.write(f"{header}\r")
        for d in st.session_state.doi_list:
            f.write(f"{d}\r")
    post_file(f)


def title_dataframe():
    delete_key_list = [
        "modify",
        "mb",
        "delete",
        "action",
        "func",
        "citeId",
        "coden",
        "btype",
    ]
    title_dict = copy.deepcopy(st.session_state.params_dict)
    for k, v in title_dict.items():
        for i in delete_key_list:
            try:
                del title_dict[k][i]
            except KeyError:
                pass
    return title_dict


def init_form():
    st.session_state.to_publisher_tuple = ()
    st.session_state.params_dict = {}
    dois = st.text_input("Add DOIs from the titles to transfer here", key="dois")
    st.text("Or you can add ISSNs or ISBNs below")
    cite_ids = st.text_input(
        "Add ISSNs or ISBNs from the titles to transfer here", key="cite_ids"
    )
    c1, c2, c3, c4 = st.columns([2, 2, 2, 2])
    c1.text_input("From prefix", key="from_prefix")
    c2.text_input("To prefix", key="to_prefix")
    st.radio(
        "Choose environment to transfer titles on",
        options=["Production", "Test"],
        key="environment",
        horizontal=True,
    )
    return dois, cite_ids


def show_results():
    if len(st.session_state.doi_failures) > 0:
        st.warning(
            f"There are some DOIs that were not \
                owned by {st.session_state.from_prefix}."
        )
    st.info(
        "Here are titles to be transferred \
            - check depositor is the same for each"
    )
    st.dataframe(convert_from_dict2(title_dataframe(), "Cite ID"))
    init_title_tran()


def get_elements(rows):
    del rows[0]
    return select_pub_id([item.find_all("td") for item in rows])


def select_pub_id(rows):
    for row in rows:
        publisher_id = row[1].text.strip()
        publisher_name = row[3].contents.pop(0)["value"]
        prefix = row[2].contents.pop(0)["value"]
        if prefix == st.session_state.to_prefix:
            st.session_state.to_publisher_tuple = (publisher_id, publisher_name)


def get_table_rows(table):
    return get_elements([(row) for row in table("tr")])


def get_publisher_id():
    r = requests.post(
        f"{test_or_prod(st.session_state.environment)}/publisherUserAdmin",
        params=search_title_params(st.session_state.to_prefix) | credentials(),
    )
    (get_table_rows(bs(r.text, "html.parser").find_all("table")[5]))


def title_transfer_params(params_dict: dict):
    params_dict["depositor"] = st.session_state.to_publisher_tuple[0]
    params_dict["btype"] = content_type_codes.get(st.session_state.content_type)
    return params_dict


def id_returns_params(id: str, admin_code: str):
    return {
        "start": "0",
        "size": "20",
        "func": "modify",
        "issns": id,
        "btype": admin_code,
    }


def get_title(id, admin_code):
    data_list = []
    r = requests.post(
        f"{test_or_prod(st.session_state.environment)}/metadataAdmin?func=search",
        data=id_returns_params(id, admin_code) | credentials(),
    )
    page = bs(r.text, "html.parser")
    total_results = page.find("font", attrs={"color": "red"}).text
    if total_results == "1":
        for f in page.find_all("td", attrs={"class": "bafsa"}):
            data_list.append(f.text)
        try:
            cite_id = (data_list[0]).strip()
            title = data_list[1].strip()
            if admin_code == "0":
                prefix = data_list[5].strip()
                citation_type = "Journal"
            else:
                prefix = data_list[7].strip()
                citation_type = data_list[6].strip()
            return title, cite_id, prefix, citation_type
        except IndexError:
            title = "no title"
            cite_id = f"Check ID {id}"
            prefix = "no prefix returned"
            citation_type = "n/a"

    elif total_results == "0":
        title = "not registered"
        cite_id = f"Check ID {id}"
        prefix = "not registered"
        citation_type = "n/a"
    else:
        match_title_when_multiple_results(id, page)

    return title, cite_id, prefix, citation_type


def match_title_when_multiple_results(id, page):
    title_list = []
    for f in page.find_all("tr", attrs={"class": "bafsa"}):
        data_list = f.text.split("\n")
        cite_id = data_list[1].strip()
        prefix = data_list[15].strip()
        id = data_list[9].strip()
        title = data_list[3].strip()
        citation_type = data_list[13].strip()
        title_list.append([title, cite_id, id, prefix, citation_type])
    st.error(f"There are multiple titles matching ID **{id}**")
    st.warning(
        f"The transfer for ID {id} has not been completed and nor have the transfers after this.  \n\n Please find below a list of the multiple titles for ID {id}"
    )
    df = pd.DataFrame(
        title_list, columns=("Title", "Cite ID", "IDs", "Prefix", "Citation Type")
    )
    df.index += 1
    st.expander("Click here to see the titles", expanded=True).dataframe(df)
    st.stop()


def id_check(ids):
    counter = 0
    progress = st.empty()
    for id in ids:
        counter += 1
        time.sleep(0.1)
        if len(id) > 8:
            c_type = "book"
        else:
            c_type = "journal"
        title, cite_id, prefix, citation_type = get_title(id, sort_content_type(c_type))

        st.session_state.doi_list.extend(get_depo_list(cite_id, c_type))
        st.session_state.key_letter = citation_type[0]
        st.session_state.content_type = citation_type
        get_params(cite_id, citation_type)
    progress.success(f"Processing {counter}/{len(ids)}")
    with st.expander(f"The length of the DOI list is {len(st.session_state.doi_list)}"):
        st.write(st.session_state.doi_list)
    st.download_button(
        "Download DOI list",
        data_to_csv(st.session_state.doi_list),
        mime="text/csv",
        file_name=f"doi_list_{timestamp_now()}.csv",
    )


def title_transfer_form():
    progress = st.empty()
    counter = 0
    for v in st.session_state.params_dict.values():
        counter += 1
        r = requests.post(
            f"{test_or_prod(st.session_state.environment)}/metadataAdmin?"
            + f"type={st.session_state.key_letter}cite",
            params=as_params(title_transfer_params(v)),
            data=credentials(),
        )
        if r.status_code == 200:
            progress.success(
                f"Transferring title {counter} of \
                    {len(st.session_state.params_dict)}  \n\
				***{v['title']}*** from ***{st.session_state.from_prefix}*** to \
                    ***{st.session_state.to_prefix}***"
            )
        else:
            progress.warning(
                f"Transferring title {counter} failed - {r.status_code} error"
            )


def init_title_tran():
    title_tran = st.button("Transfer titles", key="transfer_titles")
    if title_tran:
        title_transfer_form()
        transfer_dois()


def main():
    st.session_state.doi_failures = []
    st.session_state.doi_list = []
    dois, issn_isbns = init_form()
    if (
        (dois or issn_isbns)
        and st.session_state.to_prefix
        and st.session_state.from_prefix
    ):
        if dois:
            find_ids(split_dois(dois))
        else:
            id_check(split_ids(issn_isbns))
        get_publisher_id()
        show_results()
