import streamlit as st
import datetime
import pandas as pd
import xml.etree.cElementTree as ET
import csv
from bs4 import BeautifulSoup as bs
from io import StringIO
import re
import labs_toolkit.common.cr_common_func as common

DETAILS = {
    "tool-name": "CSV Upload Tool",
    "description": "Upload a CSV to create an resource deposit less picky than current tool",
}


def headers():
    return {"User-Agent": "labs-toolkit-csv_upload; mailto:support@crossref.org"}


def declaration():
    return {
        "xmlns": "http://www.crossref.org/doi_resources_schema/4.4.2",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "version": "4.4.2",
        "xsi:schemaLocation": "http://www.crossref.org/doi_resources_schema/4.4.2 http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd",
    }


def write_bs():
    pretty_xml = bs(open(st.session_state.filename), "html.parser").prettify()
    download_xml(open(st.session_state.filename))
    with st.expander("Here is the XML"):
        st.code(pretty_xml)


def download_xml(xml):
    st.success("The XML has been created, click the download button.")
    return st.download_button(
        "Download XML", xml, file_name=st.session_state.filename, type="primary"
    )


def generate_funder_xml(reader, doi_batch_id):
    doi_batch = ET.Element("doi_batch", declaration())
    tree = ET.ElementTree(doi_batch)
    head = ET.SubElement(doi_batch, "head")
    ET.SubElement(head, "doi_batch_id").text = doi_batch_id
    depositor = ET.SubElement(head, "depositor")
    ET.SubElement(depositor, "depositor_name").text = st.session_state.depositor
    ET.SubElement(depositor, "email_address").text = st.session_state.depositor_email
    body = ET.SubElement(doi_batch, "body")
    next(reader, None)
    for row in reader:
        doi = row[0]
        funder_name = row[1]
        funder_id = row[2]
        award_number = row[3]
        doi_resources = ET.SubElement(body, "fundref_data")
        ET.SubElement(doi_resources, "doi").text = doi
        program = ET.SubElement(doi_resources, "program")
        program.set("xmlns", "http://www.crossref.org/fundref.xsd")
        assertion = ET.SubElement(program, "assertion", name="fundgroup")
        assertion_2 = ET.SubElement(assertion, "assertion", name="funder_name")
        assertion_2.text = funder_name
        ET.SubElement(assertion_2, "assertion", name="funder_identifier").text = (
            funder_id
        )
        ET.SubElement(assertion, "assertion", name="award_number").text = award_number
    tree.write(st.session_state.filename, encoding="UTF-8", xml_declaration=True)
    ET.indent(tree, "  ")
    write_bs()


def generate_sim_check_xml(reader, doi_batch_id):
    doi_batch = ET.Element("doi_batch", declaration())
    tree = ET.ElementTree(doi_batch)
    head = ET.SubElement(doi_batch, "head")
    ET.SubElement(head, "doi_batch_id").text = doi_batch_id
    depositor = ET.SubElement(head, "depositor")
    ET.SubElement(depositor, "depositor_name").text = st.session_state.depositor
    ET.SubElement(depositor, "email_address").text = st.session_state.depositor_email
    body = ET.SubElement(doi_batch, "body")
    next(reader, None)
    for row in reader:
        doi = row[0]
        sim_check_url = row[1]
        doi_resources = ET.SubElement(body, "doi_resources")
        ET.SubElement(doi_resources, "doi").text = doi
        collection = ET.SubElement(doi_resources, "collection")
        collection.set("property", "crawler-based")
        item = ET.SubElement(collection, "item", crawler="iParadigms")
        ET.SubElement(item, "resource").text = sim_check_url
    tree.write(st.session_state.filename, encoding="UTF-8", xml_declaration=True)
    ET.indent(tree, "  ")
    write_bs()


def sort_version(index):
    if index == 1:
        return "vor"
    elif index == 3:
        return "am"
    elif index == 5:
        return "tdm"
    pass


def date_reformat(date):
    return datetime.datetime.strptime(date, "%d/%m/%Y").strftime("%Y-%m-%d")


def generate_licence_xml(reader, doi_batch_id):
    doi_batch = ET.Element("doi_batch", declaration())
    tree = ET.ElementTree(doi_batch)
    head = ET.SubElement(doi_batch, "head")
    ET.SubElement(head, "doi_batch_id").text = doi_batch_id
    depositor = ET.SubElement(head, "depositor")
    ET.SubElement(depositor, "depositor_name").text = st.session_state.depositor
    ET.SubElement(depositor, "email_address").text = st.session_state.depositor_email
    body = ET.SubElement(doi_batch, "body")
    next(reader, None)
    for row in reader:
        doi = row[0]
        vor = row[1]
        vor_date = row[2]
        am = row[3]
        am_date = row[4]
        tdm = row[5]
        tdm_date = row[6]
        lic_ref_data = ET.SubElement(body, "lic_ref_data")
        ET.SubElement(lic_ref_data, "doi").text = doi
        program = ET.SubElement(lic_ref_data, "program")
        program.set("name", "AccessIndicators")
        program.set("xmlns", "http://www.crossref.org/AccessIndicators.xsd")
        if vor:
            ET.SubElement(
                program,
                "license_ref",
                applies_to="vor",
                start_date=date_reformat(vor_date),
            ).text = vor
        if am:
            ET.SubElement(
                program,
                "license_ref",
                applies_to="am",
                start_date=date_reformat(am_date),
            ).text = am
        if tdm:
            ET.SubElement(
                program,
                "license_ref",
                applies_to="tdm",
                start_date=date_reformat(tdm_date),
            ).text = tdm
    tree.write(st.session_state.filename, encoding="UTF-8", xml_declaration=True)
    ET.indent(tree, "  ")
    write_bs()


def csv_open(file, batch_id, upload_type):
    stringio = StringIO(file.getvalue().decode("utf-8"))
    if upload_type == "sc":
        generate_sim_check_xml(csv.reader(stringio), batch_id)
    elif upload_type == "licence":
        generate_licence_xml(csv.reader(stringio), batch_id)
    else:
        generate_funder_xml(csv.reader(stringio), batch_id)


def sort_csv(headers):
    compare = [s for s in headers if "license" in s]
    if len(compare) > 0:
        upload_type = "licence"
    elif len(headers) > 2:
        upload_type = "lf"
    else:
        upload_type = "sc"
    return upload_type


def get_header(file):
    stringio = StringIO(file.getvalue().decode("utf-8"))
    df = pd.read_csv(stringio)
    st.session_state.headers = list(df.columns.values.tolist())
    if len(st.session_state.headers) <= 1:
        st.session_state.headers = (
            st.session_state.headers[0].replace(" ", "-").replace("\t", ",").split(",")
        )
    return st.session_state.headers


def get_from_csv(file, headers):
    df = pd.read_csv(file, skiprows=[0])
    df.index += 1
    df.dropna(axis=0, how="all", inplace=True)
    try:
        df.columns = headers
    except ValueError:
        temp_headers = "".join(headers)
        df.columns = [temp_headers]
        df = df[temp_headers].str.split("\t", expand=True)
        df.columns = headers
    st.dataframe(df)
    return df, sort_csv(headers)


def init_form():
    st.text("You can download a template of the csv here")
    c1, c2, c3 = st.columns(3)
    c1.link_button(
        "Similarity check.csv template",
        "https://www.crossref.org/documentation-files/supplemental-metadata-upload.csv",
    )
    c2.link_button(
        "License.csv template",
        "https://www.crossref.org/documentation-files/license-metadata.csv",
    )
    c3.link_button(
        "Funding.csv template",
        "https://www.crossref.org/documentation-files/funding-metadata.csv",
    )


def main():
    init_form()
    file_upload = st.file_uploader(
        "Upload the csv file here",
        accept_multiple_files=False,
        key="file_upload",
        type="csv",
    )
    if file_upload:
        headers = get_header(file_upload)
        df, csv_type = get_from_csv(file_upload, headers)
        st.text_input("Add the depositor name here", key="depositor")
        email = st.text_input(
            "Add the depositor email address here", key="depositor_email"
        )
        if email:
            if not re.fullmatch(r"[^@]+@[^@]+\.[^@]+", email):
                st.warning("Email is not valid please try again")
            else:
                st.success("Valid email")
                create_xml = st.button("Create XML")
                if create_xml:
                    with st.spinner("Generating the resource deposit XML file"):
                        doi_batch_id = f"{st.session_state.email}_{common.timestamp()}"
                        st.session_state.filename = (
                            f"{csv_type}_upload_{common.timestamp()}.xml"
                        )
                        csv_open(file_upload, doi_batch_id, csv_type)
