import streamlit as st
import requests
import pandas as pd
from bs4 import BeautifulSoup as bs
from labs_toolkit.settings import API_URI, XML_API, ADMIN_BASE_URL
import datetime
import time
import re
import labs_toolkit.common.cr_common_func as common

DETAILS = {
    "tool-name": "Legacy DOI Helper Tool",
    "description": "Upload XML files for legacy DOIs and this"
    + " helper tool will identify titles and ISSNs and previous "
    + "prefix in XML, also checks against the DOI for title and current owner and prefix",
}


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results)
    return df


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def headers():
    return {"User-Agent": "labs-toolkit-ldh; mailto:support@crossref.org"}


def get_table_rows(table):
    return get_elements([(row) for row in table("tr")])


def get_elements(rows):
    del rows[0]
    return select_pub_id([item.find_all("td") for item in rows])


def select_pub_id(rows):
    return (
        rows[0][1].text.strip(),
        re.sub("[^A-Za-z0-9]+", " ", str(rows[0][3].contents).split("value")[1]),
    )


def find_publisher_details_admin(prefix):
    r = requests.post(
        f"{ADMIN_BASE_URL}/publisherUserAdmin",
        params=search_title_params(prefix) | common.credentials(),
    )
    (get_table_rows(bs(r.text, "html.parser").find_all("table")[5]))


def search_title_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def find_dois_admin_id(ids):
    for id in ids:
        if id == "N/A":
            pass
        else:
            time.sleep(0.1)
            if len(id) > 10:
                url = f"{API_URI}works?filter=isbn:{id}&rows=1"
            else:
                url = f"{API_URI}journals/{id}/works?rows=1"
            dois = return_dois(url)
            if dois:
                break
    return dois


def prefix_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def find_publisher_details(prefix):
    pub_details = []
    r = requests.post(
        "https://doi.crossref.org/servlet/publisherUserAdmin",
        params=prefix_params(prefix) | common.credentials(),
    )
    soup = bs(r.text, "html.parser")
    for table in soup.find_all("tr")[4]:
        row = table.find_all("td")
        pub_id = row[1].text.split('">')[0].strip()
        prefix = row[2].contents.pop(0)["value"]
        publisher_name = row[3].contents.pop(0)["value"]
        pub_details.append([pub_id, prefix, publisher_name])
    return pub_details[0][::2]


def find_titles(file):
    st.session_state.id_list = []
    title_list = []
    data_collection = {}
    time.sleep(0.1)
    file_details = {}
    soup = bs(file, "html.parser")
    try:
        title = soup.find("full_title").text
    except Exception as e:
        st.warning(f"{e} error on filename {file.name}")
        title = "No title found in file"
    try:
        issn_p = soup.find("issn", attrs={"media_type": "print"}).text
    except AttributeError:
        issn_p = "N/A"
    try:
        issn_e = soup.find("issn", attrs={"media_type": "electronic"}).text
    except AttributeError:
        issn_e = "N/A"
    if title in title_list:
        file_details.update(
            {
                "Title": title,
                "e-ISSN": issn_e,
                "p-ISSN": issn_p,
            }
        )
    else:
        for items in soup.find_all("journal_article"):
            prefix = items.find("doi").text.split("/")[0]
        publisher_id, publisher_name = find_publisher_details(prefix)
        title_list.append(title)
        file_details.update(
            {
                "Title": title,
                "e-ISSN": issn_e,
                "p-ISSN": issn_p,
                "Prefix": prefix,
                "Publisher name": publisher_name,
                "Publisher ID (admin)": publisher_id,
            }
        )
    st.session_state.id_list.append(issn_p)
    st.session_state.id_list.append(issn_e)
    data_collection[st.session_state.filename] = file_details
    del file_details["e-ISSN"]
    del file_details["p-ISSN"]
    st.session_state.summary[st.session_state.filename] = {"Deposit Data": file_details}
    return data_collection


def find_cite_id(doi):
    xml = requests.get(f"{XML_API}{doi}").text
    soup = bs(xml, "html.parser")
    st.session_state.cite_id = soup.find("crm-item", attrs={"name": "journal-id"}).text
    st.session_state.content_type = "Journal"
    publisher, id, title, prefix = (
        get_pub_details()[0],
        get_pub_details()[1],
        get_pub_details()[2],
        get_pub_details()[3],
    )
    st.session_state.summary[st.session_state.filename].update(
        {
            "Current Data": {
                "Title": title,
                "Prefix": prefix,
                "Publisher name": publisher,
                "Publisher ID (admin)": id,
                "Title Cite ID": st.session_state.cite_id,
            }
        }
    )
    st.info(
        f"Current Title:  **{title}**  \nCurrent Publisher:  **{publisher}** \
                \nCurrent ID:  **{id}**  \nCurrent Prefix: **{prefix}** \
                  \nLink to the title:  [{title}]({st.session_state.title_url})"
    )


def sort_content_type(c_type):
    return "0" if c_type == "Journal" else "101"


def get_title_params(c_type_int, cite_id):
    return {"func": "modify", "btype": c_type_int, "citeId": cite_id}


def publisher_params(pub_id):
    return {
        "sf": "modify",
        "publisherID": pub_id,
    }


def get_pub_details():
    soup, title_url = get_soup(
        "https://doi.crossref.org/servlet/metadataAdmin",
        get_title_params(
            sort_content_type(st.session_state.content_type), st.session_state.cite_id
        ),
    )
    st.session_state.title_url = title_url.split("&usr")[0]
    pub_id = soup.find("input", attrs={"name": "depositor"})["value"]
    title = soup.find("input", attrs={"name": "title"})["value"]
    soup, publisher_url = get_soup(
        "https://doi.crossref.org/servlet/publisherUserAdmin", publisher_params(pub_id)
    )
    pub = soup.find("input", attrs={"type": "text", "name": "desc"})["value"]
    current_prefix = soup.find("input", attrs={"type": "text", "name": "doi_data"})[
        "value"
    ]
    return pub, pub_id, title, current_prefix


def get_soup(url, params):
    r = requests.get(url, params=params | common.credentials())
    html_text = r.text
    soup = bs(html_text, "html.parser")
    return soup, r.url


def return_dois(url):
    r = requests.get(url)
    r_json = r.json()["message"]
    doi = r_json["items"][0]["DOI"]
    find_cite_id(doi)
    return doi


def change_owner():
    current_pub_id = get_soup(st.session_state.title_url).find(
        "input", attrs={"name": "depositor"}
    )["value"]
    current_pub_id.replace_with(st.session_state.previous_publisher_id)


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def main():
    st.session_state.counter = 0
    st.session_state.summary = {}
    file_processed = 0
    upload_files = st.file_uploader(
        "Upload the files to scan",
        type=["xml"],
        accept_multiple_files=True,
        key="files",
    )
    if upload_files:
        st.session_state.filename_list = []
        get_titles = st.button("Get deposit and current data")
        if get_titles:
            files_processed = st.empty()
            st.write("**Summary of files**")
            with st.spinner("Loading summary"):
                title_summary = st.container()
                st.info("Below are the individual file breakdowns")
                for file in st.session_state.files:
                    st.session_state.filename = file.name
                    results = dict_convert_to_table(find_titles(file))
                    file_processed += 1
                    files_processed.success(
                        f"Files Successfully scanned = \
                            {file_processed}/{len(st.session_state.files)}\
                            :white_check_mark:  \nFilename checking: {file.name}"
                    )
                    with st.expander(file.name, expanded=False):
                        with st.spinner("Scanning the files..."):
                            st.text("The details from the XML file are:")
                            st.table(results)
                            find_dois_admin_id(st.session_state.id_list)
            temp = []
            res = dict()
            for key, val in st.session_state.summary.items():
                if val not in temp:
                    st.session_state.counter += 1
                    temp.append(val)
                    res[st.session_state.counter] = val
            title_summary.info(f"There are {len(res)} titles to transfer")
            for k, v in res.items():
                title_summary.table(dict_convert_to_table(v))
