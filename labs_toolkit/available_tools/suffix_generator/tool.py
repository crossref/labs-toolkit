import random
import streamlit as st
import string
import pandas as pd
import datetime

DETAILS = {
    "tool-name": "Suffix Generator",
    "description": "Generate random suffixes for your DOIs to use in content registration",
}


def generate_dois(prefix, quantity):
    doi_list = []
    while True:
        if len(doi_list) < int(quantity):
            num = random.randrange(1, 10**6)
            random_digits = str(num).zfill(6)
            random_letters = "".join(random.choices(string.ascii_lowercase, k=6))
            doi_list.append(f"{prefix}/{random_digits}{random_letters}")
        else:
            break
    return doi_list


def convert_output(output):
    data = pd.DataFrame(output, columns=["DOIs"])
    csv_data = data.to_csv(index=False, encoding="utf_8")
    return csv_data


def gen_dataframe(data):
    df = pd.DataFrame(data, columns=["DOIs"])
    df.index += 1
    return df


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def main():
    tool_body = st.container()
    c1, c2 = tool_body.columns([1.5, 2])
    c1.text_input("Enter your prefix to generate suffixes against", key="prefix")
    c1.number_input(
        "Number of DOIs you want to generate",
        key="quantity",
        max_value=100,
        help="Enter number between 1-100",
    )
    if st.session_state.quantity > 0:
        create_dois = tool_body.button("Generate DOIs")
        if create_dois:
            output = generate_dois(st.session_state.prefix, st.session_state.quantity)
            st.success(
                f"Here are the {st.session_state.quantity} DOIs you have generated"
            )
            st.table(gen_dataframe(output))
            st.download_button(
                label="Download DOI List",
                data=convert_output(output),
                file_name=f"doi_list_{timestamp()}.txt",
            )
