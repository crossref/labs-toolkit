import streamlit as st
import requests
import datetime
import pandas as pd
from bs4 import BeautifulSoup as bs
from labs_toolkit.settings import TEST_ADMIN_BASE_URL
import re

DETAILS = {
    "tool-name": "Add usernames to prefix",
    "description": "Add a specific username to prefix",
}


def headers():
    return {
        "User-Agent": "labs-toolkit-auser; \
            mailto:support@crossref.org"
    }


def split_ids(ids):
    ids = ids.split()
    return ids


def data_to_csv(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    csv_data = df.to_csv(encoding="utf-8")
    return csv_data


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def get_depo_list(r):
    df = pd.DataFrame([x.split() for x in r.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    owner_prefixes = list(df["OWNER"])
    dois = list(df["DOI"])
    if len(owner_prefixes) > 2:
        del owner_prefixes[0:1]
        del dois[0:1]
        owner_prefixes.remove(None)
        dois.remove(None)
        return dois
    else:
        error = "That doesn't look like a valid cite ID, any DOIs \
            against the title or an ID from a deleted title. \
                Please try a different ID"
        return error


def credentials():
    return {
        "usr": f"{st.session_state.email}/root",
        "pwd": f"{st.session_state.password}",
    }


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index", columns=["Publisher Name"])
    df.index.name = "Publisher ID"
    return df


def publisher_id_params(id: str, username_id):
    return {"newMemberID": username_id, "publisherID": id, "addMember": "Add Member"}


def search_title_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def get_elements(rows):
    del rows[0]
    return select_pub_id([item.find_all("td") for item in rows])


def get_table_rows(table):
    return get_elements([(row) for row in table("tr")])


def select_pub_id(rows):
    st.session_state.publisher_dict[rows[0][1].text.strip()] = re.sub(
        "[^A-Za-z0-9]+", " ", str(rows[0][5].contents).split("value")[1]
    )


def get_username_elements(rows):
    del rows[0]
    username_check([item.find_all("td") for item in rows])


def username_table_rows(table):
    return get_username_elements([(row) for row in table("tr")])


def username_check(user_data):
    st.session_state.user_id = user_data[0][1].text.strip()
    st.session_state.publisher_username = user_data[0][2].text.strip()
    if st.session_state.publisher_username == st.session_state.username:
        st.success(
            f"The username entered **'{st.session_state.publisher_username}'\
                ** matches the system with ID {st.session_state.user_id}"
        )
    else:
        st.warning(
            "The username if not the same as the entered username, \
                please try again later or"
        )


def check_username_params(username: str):
    return {
        "start": "0",
        "size": "20",
        "login": username,
        "RefXPLimit": "0",
        "sf": "search",
    }


def add_username_params(user_id: str, publisher_id: str):
    return {
        "start": "0",
        "size": "20",
        "newMemberID": user_id,
        "publisherID": publisher_id,
        "addMember": "Add Member",
    }


def get_publisher_id(prefix):
    r = requests.post(
        f"{TEST_ADMIN_BASE_URL}/publisherUserAdmin",
        data=search_title_params(prefix) | credentials(),
    )
    get_table_rows(bs(r.text, "html.parser").find_all("table")[5])


def confirm_username(username):
    r = requests.post(
        f"{TEST_ADMIN_BASE_URL}/depositUserAdmin",
        data=check_username_params(username) | credentials(),
    )
    return username_table_rows(bs(r.text, "html.parser").find_all("table")[5])


def add_username():
    counter = 0
    progress_bar = st.empty()
    for key in st.session_state.publisher_dict.keys():
        counter += 1
        requests.post(
            f"{TEST_ADMIN_BASE_URL}/publisherUserAdmin?sf=addMember",
            data=add_username_params(st.session_state.user_id, key) | credentials(),
        )
        progress_bar.success(
            f"Processing {counter} of {len(st.session_state.publisher_dict)}"
        )


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def main():
    st.session_state.publisher_dict = {}
    prefixes = st.text_input("Enter prefixes here")
    c1, c2, c3, c4 = st.columns([2, 2, 2, 2])
    username = c1.text_input(
        "Enter the username to add to prefixes here", key="username"
    )
    prefixes = split_ids(prefixes)
    if prefixes:
        if username:
            confirm_username(username)
            for p in prefixes:
                get_publisher_id(p)
            st.write(
                f"Here is the list of publishers that you are adding \
                    the username {st.session_state.publisher_username} to."
            )
            st.dataframe(dict_convert_to_table(st.session_state.publisher_dict))
            if st.button("Add username"):
                add_username()
