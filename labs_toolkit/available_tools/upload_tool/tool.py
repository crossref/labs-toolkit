import streamlit as st
import requests
import datetime
from labs_toolkit.settings import (
    TEST_DEPOSIT_URL,
    PROD_DEPOSIT_URL,
)


DETAILS = {
    "tool-name": "Upload Tool",
    "description": "Bulk upload files to the system.",
}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


UPLOAD_TYPE_DICT = {
    "Metadata Upload (.xml)": "doMDUpload",
    "URL updates/DOI change (.txt)": "doTransferDOIsUpload",
}


def form_data(operation, email, password, role):
    mydata = {
        "operation": operation,
        "login_id": f"{email}/{role}",
        "login_passwd": password,
    }
    return mydata


def upload_process(mydata, environment, files):
    success = []
    failures = []
    sub_log = []
    sub_log_results = st.empty()
    for f in files:
        myfile = {"fname": f}
        r = requests.post(environment, files=myfile, data=mydata)
        if r.status_code == 200:
            success.append(f.name)
            sub_log.append(
                f"**{f.name}** was successfully \
                           sent to the queue"
            )
            sub_log_results.success(
                f"Successfully sent to queue = {len(success)}/\
                    {len(st.session_state.files)} 		:white_check_mark: "
            )  ###{nl}Failed to send = {len(failures)}/{len(upload_files)}")
        else:
            failures.append(f.name)
            st.warning(
                f"The file **{f.name}** has failed to reach\
                      the queue, please try again later"
            )


def environ_url(env):
    if env == "Test":
        return TEST_DEPOSIT_URL
    elif env == "Production":
        return PROD_DEPOSIT_URL


def headers():
    return {"User-Agent": "crossref-toolkit-ut; mailto=support@crossref.org"}


def main():
    st.radio(
        "Choose an environment to upload to (default is 'Test')",
        ["Production", "Test"],
        index=1,
        key="system_choice",
        horizontal=True,
    )
    st.radio(
        "Upload Type",
        UPLOAD_TYPE_DICT.keys(),
        key="upload_type",
        index=1,
        horizontal=True,
    )
    upload_form = st.form("Upload Tool", clear_on_submit=True)
    upload_form.file_uploader(
        "Upload the files to deposit",
        type=["xml", "txt"],
        accept_multiple_files=True,
        key="files",
    )
    upload_files = upload_form.form_submit_button("Deposit Files")
    if upload_files:
        upload_process(
            form_data(
                UPLOAD_TYPE_DICT.get(st.session_state.upload_type),
                st.session_state.email,
                st.session_state.password,
                st.session_state.role,
            ),
            environ_url(st.session_state.system_choice),
            st.session_state.files,
        )
