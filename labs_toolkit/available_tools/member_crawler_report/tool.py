import streamlit as st
import requests
import datetime
import pandas as pd
from urllib.parse import urlparse
import labs_toolkit.utils.data.members as member_list_tool

DETAILS = {
    "tool-name": "Member Crawler Report",
    "description": "Run a report against a prefix or list of DOIs to check on URLs and landing pages",
}


def timestamp():
    return "{:%Y-%m-%d}".format(datetime.datetime.now())


def yesterday():
    return "{:%Y-%m-%d}".format(datetime.datetime.now() - datetime.timedelta(days=1))


def week_before():
    return "{:%Y-%m-%d}".format(datetime.datetime.now() - datetime.timedelta(days=8))


def form_data(email, password):
    credentials = {
        "operation": "doMDUpload",
        "login_id": f"{email}/root",
        "login_passwd": password,
    }
    return credentials


def check_timestamps(doi_list):
    for doi in doi_list:
        checking = False
        while checking:
            url = f"https://hdl.handle.net/api/handles/{doi}"
            req = requests.get(url)
            if req.status_code == 200:
                handle_timestamp = req.json()["values"][1]["data"]["value"]
                if handle_timestamp == timestamp():
                    return True


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    return df


def get_results(results, filename, doi_list, submission, successes, failures):
    with st.expander(
        f"Here are the results for: {submission}:  \
              \tSuccess: {successes}   \tFailures: {failures}"
    ):
        st.write(results)
        data = data_to_csv(results)
        download_data = st.download_button(
            "📥  Download Dataframe", data, mime="text/csv", file_name=filename
        )
        joined_list = "  \n".join(doi_list)
        st.info(f"**DOI List from submission:**  {len(doi_list)}  \n{joined_list}")
        return download_data


def show_file_results(file, dois):
    with st.expander(f"Here are the DOIs for: {file}: {len(dois)} DOIs"):
        joined_list = "  \n".join(dois)
        st.info(f"**DOI List from submission:**  \n{joined_list}")


def headers():
    return {
        "User-Agent": "labs-toolkit-member_crawler; mailto=support@crossref.org",
    }


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def credentials(role):
    return {
        "usr": f"{st.session_state.email}/{role}",
        "pwd": f"{st.session_state.password}",
    }


def admin_system_headers():
    return {
        "Content-Type": "application/x-www-form-urlencoded",
        "User-Agent": "labs-toolkit",
    }


def check_url(doi, url):
    url_data = urlparse(url)
    st.write(url_data)
    r = requests.head(url)
    if r.status_code == 200:
        st.session_state.successful_urls.append(f"{doi} : {url}")
        st.write(st.session_state.successful_urls)
    elif r.status_code in [404, 400]:
        st.session_state.four_hundred_errors.append(f"{doi} : {url}")
    elif r.status_code in [500, 502]:
        st.session_state.five_hundred_errors.append(f"{doi} : {url}")


def get_data(item):
    st.empty()
    st.session_state.item_list = []
    doi = item["DOI"]
    url = item["resource"]["primary"]["URL"]
    check_url(doi, url)
    st.table(list_to_df())
    st.write(doi, url)


def api_params(cursor):
    return {"cursor": cursor, "rows": "1000"}


def get_json(prefix):
    cursor = "*"
    while True:
        r = requests.get(
            f"https://api.crossref.org/prefixes/{prefix}/works?",
            headers=headers(),
            params=api_params(cursor),
        )
        try:
            if r.status_code == 200:
                body = r.json()["message"]
                items = body["items"]
                if not items:
                    break
                cursor = str(body["next-cursor"])
                for i in items:
                    get_data(i)
        except TypeError:
            st.warning(
                "The API query is not valid or no results \
                    available, please clear form and try again"
            )
            st.session_state.error = True
            break


def member_sort(member_details):
    for m in member_details:
        get_json(m[0])


def make_clickable(link, text):
    return f'<a target="_blank" href="{link}">{text}</a>'


def list_to_df():
    st.write(st.session_state.dataframe_list)
    df = pd.DataFrame(
        st.session_state.dataframe_list, columns=st.session_state.columns
    ).T
    return df


def main():
    _, member_list_json, _, _ = member_list_tool.download_members_list()
    st.session_state.columns = ["URL Resolves", "400 Errors", "500 Errors"]
    st.session_state.four_hundred_errors = []
    st.session_state.successful_urls = []
    st.session_state.five_hundred_errors = []
    st.session_state.dataframe_list = []
    st.session_state.dataframe_list.append(
        [
            len(st.session_state.four_hundred_errors),
            len(st.session_state.successful_urls),
            len(st.session_state.five_hundred_errors),
        ]
    )

    st.write("**Run crawler report against member**")
    member = st.multiselect(
        "Select prefix/member to return submissions from",
        member_list_json.items(),
        format_func=lambda x: f"{x[0]} : {x[1]}",
        default=None,
    )
    c1, c2 = st.columns([6, 1])
    st.markdown("***")
    st.text("Or add a list of DOIs to report against below")
    st.text_area("Add DOIs to report against here")
    if member:
        member_sort(member)
