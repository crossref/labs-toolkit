import streamlit as st
import requests
from labs_toolkit.settings import API_URI_WORKS
import labs_toolkit.common.cr_common_func as common

DETAILS = {
    "tool-name": "Crossref Dashboard",
    "description": "Display useful information from the REST API about Crossref",
}


def headers():
    return {"User-Agent": "labs-toolkit-reports; mailto:support@crossref.org"}


st.session_state.api_content_type_dict = {
    "Number of Journal articles": "journal-article",
    "Number of Preprints": "posted-content",
    "Number of Reports": "report",
    "Number of Peer reviews": "peer-review",
    "Number of Standards": "standard",
    "Number of Components": "component",
    "Number of Database-related records": "dataset",
    "Number of Dissertations": "dissertation",
    "Number of Grants": "grant",
}


st.session_state.api_filter_query_dict = {
    "Total records registered": "",
    "Records with references": "has-references:t,",
    "No of Preprint-to-article links": "relation.type:is-preprint-of,",
    "No of unique records with Funder IDs": "has-funder-doi:t,",
    "Records with ORCID iDs": "has-orcid:t,",
    "Records with abstracts": "has-abstract:t,",
    "Records with ROR IDs": "has-ror-id:t,",
    "Records with Crossmark": "has-update-policy:t,",
    "Crossmark status updates": "is-update:t,",
}

st.session_state.api_facet_query = {
    "Members registering references": "has-references:t,",
    "Members registering abstracts": "has-abstract:t,",
    "Members participating in Crossmark": "is-update:t,",
    "Members registering ORCID iDs": "has-orcid:t,",
    "Members registering ROR IDs": "has-ror-id:t,",
}


def get_total_results(url, params):
    r = requests.get(url, params=params)
    if r.status_code == 200:
        results = r.json()["message"]["total-results"]
        return results
    else:
        st.error("API call is not successful")


def get_facet_publisher_count(url, params):
    r = requests.get(url, params=params)
    if r.status_code == 200:
        facet_results = r.json()["message"]["facets"]["publisher-name"]["value-count"]
        return facet_results
    else:
        st.error("API call(2) is not successful")


def show_metrics(results, last_year_results, yesterday_results, k):
    change = f"{(results - last_year_results)/last_year_results*100}"
    st.session_state.col1.metric(k, ("{:,}".format(results)), delta=f"{change[:5]}%")
    st.session_state.col2.metric(
        f"{k} for previous year",
        ("{:,}".format(last_year_results)),
        delta=f"{common.year_earlier(st.session_state.until_date)}",
        delta_color="off",
    )
    st.session_state.col3.metric(
        f"{k} registered yesterday",
        ("{:,}".format(yesterday_results)),
        delta=f"{common.yesterday()}",
        delta_color="off",
    )


def type_params(v, until_date):
    return {"filter": f"type:{v},until-created-date:{until_date}"}


def type_params_from(v, until_date, from_date):
    return {
        "filter": f"type:{v},\
            from-created-date:{from_date},\
                -created-date:{until_date}"
    }


def facet_params(v, until_date):
    return {
        "filter": f"type:{v},until-created-date:{until_date}",
        "facet": "publisher-name:*",
        "rows": "0",
    }


def facet_params_from(v, until_date, from_date):
    return {
        "filter": f"type:{v},from-created-date:{from_date},\
            until-created-date:{until_date}",
        "facet": "publisher-name:*",
        "rows": "0",
    }


def main():
    col1, col2, col3 = st.columns(3)
    until_date = col1.date_input("Enter the until created date", key="until_date")
    run_report = col1.button("Run report")
    if run_report:
        (
            st.session_state.col1,
            st.session_state.col2,
            st.session_state.col3,
        ) = st.columns(3)
        st.session_state.col1.subheader("Totals")
        st.session_state.col2.subheader("Total Previous Year")
        st.session_state.col3.subheader("Totals for Yesterday")
        st.divider()

        # full_dict = st.session_state.api_content_type_dict |
        # st.session_state.api_filter_query_dict | st.session_state.api_facet_query
        for k, v in st.session_state.api_content_type_dict.items():
            results = get_total_results(API_URI_WORKS, type_params(v, until_date))
            last_year_results = get_total_results(
                API_URI_WORKS, type_params(v, common.year_earlier(until_date))
            )
            yesterday_results = get_total_results(
                API_URI_WORKS,
                type_params_from(
                    v, common.year_earlier(until_date), common.yesterday()
                ),
            )
            show_metrics(results, last_year_results, yesterday_results, k)
        for k, v in st.session_state.api_filter_query_dict.items():
            results = get_total_results(API_URI_WORKS, type_params(v, until_date))
            last_year_results = get_total_results(
                API_URI_WORKS, type_params(v, common.year_earlier(until_date))
            )
            yesterday_results = get_total_results(
                API_URI_WORKS,
                type_params_from(
                    v, common.year_earlier(until_date), common.yesterday()
                ),
            )
            show_metrics(results, last_year_results, yesterday_results, k)
        for k, v in st.session_state.api_facet_query.items():
            results = get_facet_publisher_count(
                API_URI_WORKS, facet_params(v, until_date)
            )
            last_year_results = get_facet_publisher_count(
                API_URI_WORKS, facet_params(v, common.year_earlier(until_date))
            )
            yesterday_results = get_facet_publisher_count(
                API_URI_WORKS,
                facet_params_from(
                    v, common.year_earlier(until_date), common.yesterday()
                ),
            )
            show_metrics(results, last_year_results, yesterday_results, k)
