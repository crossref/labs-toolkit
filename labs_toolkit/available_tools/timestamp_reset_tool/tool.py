import streamlit as st
import requests
import datetime
from bs4 import BeautifulSoup as bs

DETAILS = {
    "tool-name": "Timestamp Reset Tool",
    "description": "Reset the timestamps against a list of DOIs from a submission ID.",
}


def update_timestamps(doi_list, credentials):
    filename = f"{st.session_state.submission_id}_timer_reset.txt"
    header = (
        f"H:email={st.session_state.email};"
        + f"type=reset_timestamps;timestamp={small_timestamp()}"
    )
    with open(filename, "w") as f:
        f.write(f"{header}  \n")
        for doi in doi_list:
            f.writelines(f"{doi}\n")

    with open(filename, "r") as f2:
        file = {"fname": f2}
        r = requests.post(
            "https://doi.crossref.org/servlet/deposit", files=file, data=credentials
        )
        if r.status_code == 200:
            st.success(f"**{filename}** was successfully sent to the queue")


def small_timestamp():
    return "{:%Y%m%d}".format(datetime.datetime.now())


def form_data(email, password, role):
    if role == "root":
        credentials = {
            "operation": "doTransferDOIsUpload",
            "login_id": f"{email}/{role}",
            "login_passwd": password,
        }
    else:
        st.warning("Change your role to root")
    return credentials


def rerun_submission(submission):
    sub_url = "https://doi.crossref.org/servlet/submissionAdmin"
    requests.post(sub_url, data={"sf": "rerun", "submissionID": submission})


def headers():
    return {"User-Agent": "labs-toolkit-tt; mailto=support@crossref.org"}


def check_timestamps(doi_list):
    for doi in doi_list:
        checking = False
        while checking:
            url = f"https://hdl.handle.net/api/handles/{doi}"
            req = requests.get(url, headers=headers)
            if req.status_code == 200:
                handle_timestamp = req.json()["values"][1]["data"]["value"]
                if handle_timestamp == small_timestamp():
                    return True


def main():
    # st.write(st.session_state)
    st.session_state.doi_list = []
    submission = st.text_input(
        "Enter the submission ID to reset timestamps for the DOIs", key="submission_id"
    )
    button = st.button("Reset timestamps against DOIs on submission ID")
    if button:
        with st.spinner(
            "The DOIs are gettings the timestamps updated, once \
                updated a rerun of the submission will happen"
        ):
            url = (
                "https://doi.crossref.org/servlet/submissionAdmin?"
                + f"sf=detail&submissionID={submission}&usr={st.session_state.email}/"
                + f"{st.session_state.role}&pwd={st.session_state.password}"
            )
            xml = requests.get(url)
            soup = bs(xml.content, "xml")
            if xml.status_code == 200:
                soup = bs(xml.text, "html.parser")
                td = soup.find("textarea", attrs={"name": "noteMessage"})
                message = f"{td}"
                soup2 = bs(message, "html.parser")
                for rd in soup2.find_all("record_diagnostic"):
                    status = rd.get("status")
                    msg_id = rd.get("msg_id")
                    doi = rd.find("doi").text
                    if status == "Failure" and msg_id == "4":
                        st.session_state.doi_list.append(doi)
                doi_list = " ".join(st.session_state.doi_list)
                st.write(f"There are timestamp errors against DOIs:  \n {doi_list}")
                update_timestamps(
                    st.session_state.doi_list,
                    form_data(
                        st.session_state.email,
                        st.session_state.password,
                        st.session_state.role,
                    ),
                )

                if check_timestamps(st.session_state.doi_list):
                    rerun_submission(submission)
