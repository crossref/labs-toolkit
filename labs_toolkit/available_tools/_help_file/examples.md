##### Using each tool

###### API Query URL Download Tool
There is a limit of 10,000 results that can be returned via this tool at the moment, while we monitor the load on the API and the backend of the tool.

* A query to return all the DOIs, titles and URLs against a prefix:
```https://api.crossref.org/prefixes/{YOUR PREFIX HERE}/works?select=DOI,title,URL```



###### Search on DOIs
* If you have a list of DOIs that you would like to return metadata from then you can enter the list in this tool and select what metadata elements you would like returned from the REST API for each DOI. If they are present in the metadata they will be returned.

* The DOI list should be separated by a space for the tool to work correctly.



###### Suffix Generator

* Create random opaque 