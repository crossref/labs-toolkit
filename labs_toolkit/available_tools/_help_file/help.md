##### What have we here?

We have created a selection of tools which we hope will help with querying against some of our services including but not exclusively our REST API.

We have tools to help convert API outputs into CSV files, easier ways to create opaque DOIs, and tools to help extract metadata from the API from a list of DOIs. 

We hope to add more tools as we go along and help allow our members to return information for themselves.

##### Why have we done it?

We are really keen to get our members tools to allow them to extract information from our REST API which can be a more technical task, especially if there are more than 1000 rows of results. 

Reducing 'toil'. Who doesn't want more efficient ways of doing things? Or a nicer UI to help identify issues or help with manual tasks.

##### More information.

More information on each tool can be found in the below table and in the future we will start adding more and more examples of things you can do in each tool itself.