"""Home page shown when the user enters the application"""

import streamlit as st
import pandas as pd
import labs_toolkit.common.cr_common_func as common
import labs_toolkit.available_tools as tools

DETAILS = {
    "tool-name": " - Help File",
    "description": "Explains the functions of each tool in the collection.",
}


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index", columns=["Tool Description"])
    df.columns.name = "DOI"
    return df


def sort_dict(dictionary):
    sorted_keys = list(dictionary.keys())
    sorted_keys.sort()
    sorted_dict = {i: dictionary[i] for i in sorted_keys}
    return sorted_dict


def tool_name_actual(tool_name):
    return getattr(tools, tool_name)


def tool_names():
    return dict(
        zip(
            [
                tool_name_actual(x).tool.DETAILS["tool-name"]
                for x in st.session_state.tools_allowed
            ],
            [
                tool_name_actual(x).tool.DETAILS["description"]
                for x in st.session_state.tools_allowed
            ],
        )
    )


def main():
    """Used to write the page in the app.py file"""
    with st.spinner("Loading Home ..."):
        st.markdown(common.load_file("labs_toolkit/available_tools/_help_file/help.md"))
        with st.expander("Tools List"):

            st.table(tool_names())
        with st.expander("Examples"):
            st.markdown(
                common.load_file("labs_toolkit/available_tools/_help_file/examples.md")
            )
