import streamlit as st
import requests
import labs_toolkit.common.cr_common_func as common
from urllib import parse


DETAILS = {
    "tool-name": "Content Negotiation Tool",
    "description": "Return the citation from a DOI choosing different styles",
}


def headers():
    return {"Accept": sort_style()}


def sort_style():
    if st.session_state.style == "APA":
        return "text/x-bibliography; style=apa"
    elif st.session_state.style == "BibTex":
        return "application/x-bibtex"
    elif st.session_state.style == "Crossref Unixref":
        return "application/vnd.crossref.unixref+xml"
    elif st.session_state.style == "Harvard":
        return "text/x-bibliography; style=harvard3"


def negotiate(doi):
    doi = parse.quote_plus(doi)
    r = requests.get(f"https://doi.org/{doi}", headers=headers())
    citation = r.content.decode("utf-8")
    st.markdown(citation)
    return citation


def main():
    dois = st.text_area("Add the DOIs to retrieve citations from")
    st.text("You can only search on one style per DOI list")
    st.radio(
        "Choose the citation style to be returned",
        ["APA", "Bibtex", "Crossref Unixref"],
        key="style",
        horizontal=True,
        index=0,
    )  # ,"Harvard"]
    start = st.button("Start")
    if start:
        citation_list = [
            (
                st.warning(
                    "Please make sure you are adding the DOI only,\
                      not the full link starting with https://doi.org"
                )
                if not doi.startswith("10.")
                else negotiate(doi)
            )
            for doi in common.split_ids(dois)
        ]
        with st.expander("Full citation list"):
            st.write(citation_list)
            # st.download_button("Download citation list",citation_list,\
            # file_name=f"{common.timestamp()}_ref_list.txt",mime="text/csv")
