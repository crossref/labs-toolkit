import streamlit as st
import datetime
import pandas as pd
import xml.etree.cElementTree as ET
import csv
from bs4 import BeautifulSoup as bs
from io import StringIO
import re


DETAILS = {
    "tool-name": "Multiple Resolution Tool (Secondary URLs)",
    "description": "A tool to create the XML file to add the secondary URL and label against a DOI",
}


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    return df


def headers():
    return {"User-Agent": "labs-toolkit-mrt; mailto:support@crossref.org"}


def split_data(data):
    data = data.split()
    for d in data:
        data.strip()
    return data


def convert_from_dict(data):
    for (
        k,
        v,
    ) in data.items():
        if k == "date-parts":
            data = datetime.date(*v[0])
    return data


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def declaration():
    return {
        "xmlns": "http://www.crossref.org/doi_resources_schema/4.4.2",
        "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "version": "4.4.2",
        "xsi:schemaLocation": "http://www.crossref.org/doi_resources_schema/4.4.2\
              http://www.crossref.org/schema/deposit/doi_resources4.4.2.xsd",
    }


def write_bs():
    pretty_xml = bs(open(st.session_state.filename), "html.parser").prettify()
    download_xml(open(st.session_state.filename))
    with st.expander("Here is the XML"):
        st.code(pretty_xml)


def download_xml(xml):
    st.success("The XML has been created, click the download button.")
    return st.download_button("Download XML", xml, file_name=st.session_state.filename)


def generate_label_xml(reader, doi_batch_id):
    doi_batch = ET.Element("doi_batch", declaration())
    tree = ET.ElementTree(doi_batch)
    head = ET.SubElement(doi_batch, "head")
    ET.SubElement(head, "doi_batch_id").text = doi_batch_id
    depositor = ET.SubElement(head, "depositor")
    ET.SubElement(depositor, "depositor_name").text = st.session_state.depositor
    ET.SubElement(depositor, "email_address").text = st.session_state.depositor_email
    body = ET.SubElement(doi_batch, "body")
    next(reader, None)
    for row in reader:
        doi = row[0]
        secondary_url = row[1]
        label = row[2]
        doi_resources = ET.SubElement(body, "doi_resources")
        ET.SubElement(doi_resources, "doi").text = doi
        collection = ET.SubElement(doi_resources, "collection")
        collection.set("property", "list-based")
        item = ET.SubElement(collection, "item", label=label)
        ET.SubElement(item, "resource").text = secondary_url
    tree.write(st.session_state.filename, encoding="UTF-8", xml_declaration=True)
    ET.indent(tree, "  ")
    write_bs()


def csv_open(file, batch_id):
    stringio = StringIO(file.getvalue().decode("utf-8"))
    generate_label_xml(csv.reader(stringio), batch_id)


def get_from_csv(file_upload):
    df = pd.read_csv(file_upload, index_col=False)[["DOI", "Secondary URL", "Label"]]
    df.set_index("DOI", inplace=True)
    st.dataframe(df)


def main():
    st.text("You can download a template of the csv here")
    st.download_button(
        "Download a .csv template",
        data=open(
            "labs_toolkit/available_tools/multiple_resolution_sec_unlock/secondary_url_template.csv"
        ),
        file_name="secondary_url_template.csv",
    )
    file_upload = st.file_uploader(
        "Upload the csv file here", accept_multiple_files=False, key="file_upload"
    )
    if file_upload:
        get_from_csv(file_upload)
        st.text_input("Add the depositor name here", key="depositor")
        email = st.text_input(
            "Add the depositor email address here", key="depositor_email"
        )
        if email:
            if not re.fullmatch(r"[^@]+@[^@]+\.[^@]+", email):
                st.warning("Email is not valid please try again")
            else:
                create_xml = st.button("Create XML")
                if create_xml:
                    with st.spinner("Generating the secondary URL XML file"):
                        doi_batch_id = f"{email}_{timestamp()}"
                        st.session_state.filename = f"mr_label_file_{timestamp()}.xml"
                        csv_open(file_upload, doi_batch_id)
