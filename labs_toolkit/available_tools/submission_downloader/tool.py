import streamlit as st
import requests
import datetime
from bs4 import BeautifulSoup as bs
from zipfile import ZipFile
import labs_toolkit.common.cr_common_func as common

DETAILS = {
    "tool-name": "Submission Downloader",
    "description": "Download the submission XML files from a list of submission IDs.",
}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def submission_get_params(submission):
    return {
        "sf": "content",
        "submissionID": submission,
    }


def submission_get(submission, file_id):
    st.session_state.filename = f"{file_id}_{submission}.xml"
    r = requests.get(
        "https://doi.crossref.org/servlet/submissionAdmin",
        params=submission_get_params(submission) | common.credentials(),
    )
    xml = r.content
    soup = bs(xml, "xml")
    if f"{soup}" == "submission content not available to user":
        st.error(
            (
                "You do not have permission to view this submission"
                + "{submission} with username {st.session_state.role}"
            )
        )
        pass
    else:
        return create_xml_file(f"{soup}")


def create_xml_file(xml_data):
    with open(st.session_state.filename, "w") as f:
        f.write(xml_data)
        st.session_state.files.append(f)
    return f


def split_submissions(submissions):
    submissions = submissions.replace(",", " ").split()
    return submissions


def main():
    st.session_state.files = []
    form_body = st.form(key="sub_form", clear_on_submit=True)
    file_id = form_body.text_input("Enter an ID to append to the filenames")
    submissions = form_body.text_area("Enter the submission IDs here")
    st.session_state.submissions = split_submissions(submissions)
    download_files = form_body.form_submit_button("Download XML files")
    if download_files:
        with st.spinner("Getting XML files..."):
            zip_name = f"download_tool_{timestamp()}.zip"
            with ZipFile(zip_name, "w") as zip_file:
                for submission in st.session_state.submissions:
                    file = submission_get(submission, file_id)
                    if not file:
                        pass
                    else:
                        zip_file.write(file.name)
            # TODO Check filesize and stop button showing if nothing to download
            st.success("Files ready to download, click button below")
            with open(zip_name, "rb") as f:
                st.download_button("Download Zip File", f, file_name=zip_name)
