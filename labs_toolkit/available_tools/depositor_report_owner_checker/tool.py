import streamlit as st
import requests
import pandas as pd
from collections import Counter
from bs4 import BeautifulSoup as bs


DETAILS = {
    "tool-name": "Depositor Report Owner Checker",
    "description": "From a list of Crossref title IDs return the DOIs from the depositor report and which prefixes they belong to.",
}


def get_data(id, r_type):
    if st.session_state.report_type == "Journal":
        r_type = "J"
    elif st.session_state.report_type == "Book":
        r_type = "B"
    else:
        r_type = "S"
    url = f"https://data.crossref.org/depositorreport?pubid={r_type}{id}"
    page = requests.get(url).text
    df = pd.DataFrame([x.split() for x in page.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    owner_prefixes = list(df["OWNER"])
    dois = list(df["DOI"])
    # st.write(owner_prefixes)
    if len(owner_prefixes) > 2:
        del owner_prefixes[0:1]
        del dois[0:1]
        owner_prefixes.remove(None)
        dois.remove(None)
        counter = Counter(owner_prefixes)
        return dict(counter), dois, url
    else:
        error = "That doesn't look like a valid cite ID, any DOIs against \
            the title or an ID from a deleted title. Please try a different ID"
        return error, 0, url


def c_types_dict():
    content_types = {
        "101": "Non journal",
        "0": "Journal",
    }
    return content_types


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8", index=False)
    return csv_data


def get_title(id):
    if st.session_state.report_type == "Journal":
        c_type = "0"
    else:
        c_type = "101"
    url = f"https://doi.crossref.org/servlet/metadataAdmin?func=modify&btype={c_type}&citeId={id}&usr={st.session_state.email}/root&pwd={st.session_state.password}"
    html_text = requests.get(url).text
    soup = bs(html_text, "html.parser")
    title = soup.find("input", attrs={"name": "title"})["value"]
    return title


def split_ids(ids):
    ids = ids.split()
    return ids


def main():
    cite_ids = st.text_area("Enter the cite IDs", key="cite_ids")
    st.radio(
        "Which report type will it be",
        options=["Journal", "Book", "Series"],
        index=0,
        horizontal=True,
        key="report_type",
    )
    run_form = st.button("Run Form")
    if run_form:
        if not cite_ids:
            st.warning("Please enter a cite ID")
        else:
            st.session_state.ids = split_ids(cite_ids)
            with st.spinner("Checking against the IDs"):
                for id in st.session_state.ids:
                    data, dois, url = get_data(id, st.session_state.report_type)
                    if data:
                        if not dois == 0:
                            with st.container():
                                title = get_title(id)
                                st.success(
                                    f"Depositor report found and\
                                         prefixes below for \
                                            \n**{title}**"
                                )
                                df = pd.DataFrame(
                                    list(data.items()),
                                    columns=["Owner Prefix", "No of DOIs"],
                                )
                                df.index += 1
                                st.dataframe(df)
                                st.write(url)
                                data = data_to_csv(dois)
                                joined_list = "  \n".join(dois)
                                st.info(
                                    f"**{len(dois)}** DOIs against \
                                        **{title}**: Download below"
                                )
                                st.download_button(
                                    "📥  Download Dataframe",
                                    data,
                                    mime="text/csv",
                                    file_name=f"dois from {title}.csv",
                                )
                                with st.expander("DOIs"):
                                    st.write(joined_list)
                        else:
                            st.warning(data)
                            st.write(url)
