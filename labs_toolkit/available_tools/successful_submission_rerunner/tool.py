import streamlit as st
import requests
import datetime
from bs4 import BeautifulSoup as bs
import urllib.request as urllib2

DETAILS = {
    "tool-name": "Successful Submission Rerun",
    "description": "Increase the timestamp by 1 and rerun a submission ID.",
}


def find_username_filename(email, password, submission):
    data = {}
    url = f"https://doi.crossref.org/servlet/submissionAdmin?sf=detail&submissionID={submission}&usr={email}/root&pwd={password}"
    xml = urllib2.urlopen(url).read()
    soup = bs(xml, "xml")
    for table in soup.find_all("td", {"class": "tabContent2"}):
        sections = table.find_all("tr")
        for s in sections[0:9]:
            k, v = (s.get_text()).split(":", 1)
            data[k] = v
    username = data["Depositor"]
    filename = data["Name"]
    return username, filename


def form_data(email, password):
    credentials = {
        "operation": "doMDUpload",
        "login_id": f"{email}/{st.session_state_username}",
        "login_passwd": password,
    }
    return credentials


def update_timestamp(xml):
    soup = bs(xml, "xml")
    new_timestamp = int(soup.find("timestamp").text) + 1
    timestamp = soup.timestamp
    timestamp.string = str(new_timestamp)
    download_xml(f"{soup}")
    return f"{soup}"


def timestamp_now():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def submission_get(email, password, submission):
    url = f"https://doi.crossref.org/servlet/submissionAdmin?sf=content&submissionID={submission}&usr={email}/root&pwd={password}"
    xml = requests.get(url).content
    updated_xml = update_timestamp(xml)
    return create_xml_file(updated_xml)


def check_details(credentials):
    st.session_state
    for k, v in credentials.items():
        if not v:
            st.warning(f"{k} has not been entered, please fill in and try again")
        check = False
    else:
        check = True
    return check


def deposit_file(mydata, file):
    deposit_file_button = st.button("Deposit New File", key="deposit_file_button")
    if deposit_file_button:
        upload_process(mydata, file)


def download_xml(xml):
    return st.download_button("Download XML", xml, file_name=st.session_state.filename)


def headers():
    return {"User-Agent": "labs-toolkit-ss; mailto=support@crossref.org"}


def upload_process(credentials, xml_file):
    success = []
    failures = []
    sub_log_results = st.empty()
    with open(st.session_state.filename, "r") as f:
        file = {"fname": f}
        r = requests.post(
            "https://doi.crossref.org/servlet/deposit", files=file, data=credentials
        )
        if r.status_code == 200:
            success.append(xml_file.name)
            sub_log_results.success(
                f"Successfully sent to queue = {xml_file.name}		:white_check_mark: "
            )
        else:
            failures.append(xml_file.name)
            st.warning(
                f"The file ##{xml_file.name}## has failed \
                to reach the queue, please try again later"
            )


def split_submissions(submissions):
    submissions = submissions.split()
    return submissions


def create_xml_file(xml_data):
    with open(st.session_state.filename, "w") as f:
        f.write(xml_data)
    return f


def main():
    tool_body = st.container()
    submission = tool_body.text_area(
        "Enter the submission IDs here", key="submission_ids"
    ).strip()
    if submission:
        submissions = split_submissions(st.session_state.submission_ids)
        for submission in submissions:
            st.session_state_username, st.session_state.filename = (
                find_username_filename(
                    st.session_state.email, st.session_state.password, submission
                )[0],
                find_username_filename(
                    st.session_state.email, st.session_state.password, submission
                )[1],
            )
            updated_xml = submission_get(
                st.session_state.email, st.session_state.password, submission
            )
            st.success(
                "XML File and timestamp updated and \
                    ready to be downloaded or deposited"
            )
            if st.session_state_username in st.session_state.available_roles:
                st.info(
                    f'You have permissions for username \
                        **"{st.session_state_username}"**\
                          on your account.  \n Change to \
                            {st.session_state_username} then \
                            click "Deposit New File" button \
                                below to deposit new file.'
                )
            else:
                st.warning(
                    f'Username **"{st.session_state_username}"**\
                          is not assigned against your \
                        auth account, please add it before clicking \
                            "Deposit New File" button below.'
                )
            my_data = form_data(st.session_state.email, st.session_state.password)
            deposit_file(my_data, updated_xml)
