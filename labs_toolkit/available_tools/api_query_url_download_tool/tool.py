import logging
import streamlit as st
import requests
import datetime
import pandas as pd
from urllib.parse import unquote, urlparse, parse_qs
from labs_toolkit.settings import (
    API_URI_NO_FS,
    PUB_THRESHOLD,
    INT_THRESHOLD,
)


logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)


DETAILS = {
    "tool-name": "API Query URL Download tool",
    "description": "Use your own API query URL in this tool to be able to download the results to .csv.",
}


def headers():
    return {"User-Agent": "labs-toolkit-au; mailto:support@crossref.org"}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def convert_output(output):
    data = pd.DataFrame(output)
    csv_data = data.to_csv(index=False, encoding="utf_8")
    return csv_data


def display_results(output, url):
    st.subheader("Your results are here for query")
    with st.expander(f"Results: {st.session_state.total_results}"):
        st.json(output)
        filename = f"user_url_query_{timestamp()}.csv"
        data = convert_output(output)
    st.download_button("Download Ouput", data, mime="text/csv", file_name=filename)
    st.markdown("***")


def data_request(url):
    logger.info(f"**** {url}")
    url_check = requests.get(url, headers=headers())
    if url_check.status_code == 200:
        st.session_state.message_type = url_check.json()["message-type"]
        if st.session_state.message_type == "validation-failure":
            error_message = url_check.json()["message"]["name"]
            has_error(url, error_message)
            st.session_state.has_error = True
        elif st.session_state.message_type == "exception":
            error_message = url_check.json()["message"]["name"]
            has_error(url, error_message)
            st.session_state.has_error = True
        elif st.session_state.message_type == "work":
            st.session_state.total_results = "DOI data returned"
            return work_request(url)
        elif st.session_state.message_type in ["member", "prefix"]:
            return work_request(url)
        else:
            st.session_state.route, st.session_state.route_type = (
                f"{st.session_state.message_type.split('-')[0]}s",
                f"{st.session_state.message_type.split('-')[1]}s",
            )
            return list_request(url)
    else:
        st.session_state.has_error = True
        st.error(
            f"{url_check.status_code} Error returned...the API could\
                  be down or the query is not valid"
        )


def work_request(url):
    item_list = []
    res = requests.get(url, headers=headers())
    item_list.append(res.json()["message"])
    return item_list


# def encode_url(url):
# 	return urllib.parse.urlencode(url, safe=":+&*[]',")


def list_request(url):
    logger.info(f"**** {url}")
    parsed_url = urlparse(url)
    st.session_state.url_route = parsed_url.path
    st.session_state.params = parse_qs(parsed_url.query)
    if "mailto" in st.session_state.params:
        del st.session_state.params["mailto"]
    if "sample" not in st.session_state.params:
        if "rows" in st.session_state.params:
            st.session_state.params["rows"] = "1"
    url_check = requests.get(
        f"{API_URI_NO_FS}{st.session_state.url_route}",
        params=st.session_state.params,
        headers=headers(),
    )
    st.write(unquote(url_check.url))
    if url_check.status_code == 200:
        st.session_state.total_results = url_check.json()["message"]["total-results"]
        if st.session_state.total_results > st.session_state.threshold:
            st.session_state.has_error = True
            has_error(
                f"This tool can return result sets of no more than\
                      {st.session_state.threshold}",
                f"This query returns {st.session_state.total_results}",
            )
        else:
            item_list = []
            cursor = "*"
            if st.session_state.total_results >= 1000:
                st.session_state.params["cursor"] = cursor
                st.session_state.params["rows"] = 1000
                st.success("Got JSON...checking results")
                while True:
                    st.session_state.params["cursor"] = cursor
                    logger.info(f">>>> {headers()}")
                    logger.info(f">>>> {url}")
                    # st.write(st.session_state.params)
                    res = requests.get(
                        f"{API_URI_NO_FS}{st.session_state.url_route}",
                        headers=headers(),
                        params=st.session_state.params,
                    )
                    logger.info(f">>>>> {res.status_code}")
                    try:
                        body = res.json()["message"]
                        items = body["items"]
                        if not items:
                            break
                        cursor = str(body["next-cursor"])
                        # cursor = urllib.parse.quote(cursorRaw)
                        for i in items:
                            item_list.append(i)
                    except TypeError:
                        st.warning(
                            "The API query is not valid or no results \
                                available, please clear form and try again"
                        )
                        break
                return item_list
            else:
                try:
                    if "sample" not in st.session_state.params:
                        st.session_state.params["rows"] = st.session_state.total_results
                    res = requests.get(
                        f"{API_URI_NO_FS}{st.session_state.url_route}",
                        headers=headers(),
                        params=st.session_state.params,
                    )
                    st.success("Got JSON...checking results")
                    body = res.json()["message"]
                    items = body["items"]
                    for i in items:
                        item_list.append(i)
                    # st.write(item_list)
                    return item_list
                except TypeError as e:
                    st.warning(
                        f"The API query is not valid or no results available, \
                            please clear form and try again  \n{e}"
                    )
    elif str(url_check.status_code).startswith("5"):
        st.error(
            f"{url_check.status_code} Error returned...the API \
                could be down or the query is not valid"
        )
    else:
        st.warning(f"Error code: {url_check.status_code}")
        st.warning("Valid JSON has not been retrieved")


def has_error(url: str, error_message):
    st.error(f"An error has occurred  \n{url}")
    st.code(f"Error message: {error_message}")


def determine_threshold():
    if st.session_state.login_type in ["public", "member"]:
        st.session_state.threshold = PUB_THRESHOLD
    elif st.session_state.login_type == "internal":
        st.session_state.threshold = INT_THRESHOLD


def main():
    st.session_state.has_error = False
    determine_threshold()
    # st.session_state.threshold = False
    st.session_state.total_results = None
    st.info("Add your own API links below to retrieve results from the API")
    st.text_input(
        "Add the URL here",
        key="user_url",
        placeholder="Example link in the help section ❓",
        help="https://api.crossref.org/works?filter=isbn:9789004505148",
    )
    # url_list = st.text_area("Add URLs here",key="multiple_urls")
    # urls = split_urls(url_list)
    run_query = st.button("Run API Query")
    if run_query:
        if st.session_state.user_url:
            if st.session_state.user_url.split(".org")[0] not in [
                "https://api.crossref",
                "http://api.crossref",
                "https://api.labs.crossref",
            ]:
                st.write(
                    f"This is not a valid Crossref API query \
                         \n {st.session_state.user_url}"
                )
            else:
                with st.spinner("Retrieving your JSON..."):
                    output = data_request(st.session_state.user_url)
                    if not st.session_state.has_error:
                        if st.session_state.total_results == 0:
                            st.write(
                                f"There are no results to return from this query \
                                    {st.session_state.user_url}"
                            )
                            pass
                        elif st.session_state.route_type == "lists":
                            st.info(
                                f"**The total results from that query are \
                                    {st.session_state.total_results}**"
                            )
                            display_results(output, st.session_state.user_url)
                        elif st.session_state.message_type == "work":
                            display_results(output, st.session_state.user_url)
                    else:
                        pass
        else:
            st.warning("User URL field is missing a value.")
