import streamlit as st
import requests
import datetime
import pandas as pd
from labs_toolkit.settings import API_URI

DETAILS = {
    "tool-name": "ISSN Return Tool",
    "description": "Return ISSNs and other actions from prefix",
}


def back_year_calc():
    return int(datetime.datetime.now().year) - 2


def headers():
    return {"User-Agent": "labs-toolkit-irt; mailto:support@crossref.org"}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def report_options():
    reports = {
        "has references": ",has-references:t",
        "has abstract": ",has-abstract:t",
        "has ORCID": ",has-orcid:t",
        "has funder": ",has-funder:t",
        "has similarity check URL": ",has-full-text:t,\
                full-text.application:similarity-checking",
    }
    return reports


def published_options():
    published = {
        "backfile": f",until-pub-date:{back_year_calc()-1}",
        "current": f",from-pub-date:{back_year_calc()}",
    }
    return published


def add_by_cy():
    # st.session_state.columns = []
    for k, v in published_options().items():
        # st.session_state.columns.append(f"{k} total dois")
        # st.write(st.session_state.columns)
        st.session_state.params["filter"] += published_options().get(k)
        res = requests.get(
            f"{st.session_state.api_url}rows=0",
            headers=headers(),
            params=st.session_state.params,
        )
        # st.write(unquote(res.url))
        try:
            body = res.json()["message"]
            current_dict = dict(body["facets"]["container-title"]["values"])
            for title, dois in current_dict.items():
                st.session_state.prefix_dict[title][f"{k} total dois"] = dois
        except KeyError:
            False
        current_filter = st.session_state.params.get("filter")
        new_filter = current_filter.replace(v, "")
        st.session_state.params["filter"] = new_filter


def find_member():
    try:
        r = requests.get(f"{API_URI}prefixes/{st.session_state.id}", headers=headers())
        st.session_state.member = r.json()["message"]["name"]
        st.session_state.api_url = f"{API_URI}prefixes/{st.session_state.id}/works?"
        member = f"Searching on member {st.session_state.member}  \
            \nSearch ID: {st.session_state.id}"
        st.info(member)
    except Exception:
        st.warning(f"Error: No member against {st.session_state.id}  \n{r.url}")
        st.session_state.has_error = True


def show_download(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    st.button("Download Dataframe", csv_data)


def convert_results(data):
    df = pd.DataFrame.from_dict(data, orient="index")
    df.fillna(0, inplace=True)
    temp_cols = df.columns.tolist()
    df = df.astype({x: "int" for x in temp_cols})
    new_cols = temp_cols[-1:] + temp_cols[:-1]
    df = df[new_cols]
    try:
        column_to_move = df.pop("total dois")
        df.insert(0, "total dois", column_to_move)
    except Exception:
        pass
    st.dataframe(df)
    show_download(df)


def calculate_percentage(report, published):
    for title, data in st.session_state.prefix_dict.items():
        try:
            percentage = (
                data[f"{published} {report}"] / data[f"{published} total dois"] * 100
            )
        except KeyError:
            percentage = 0
        st.session_state.prefix_dict[title][f"{published} {report} %"] = percentage


def get_title_list():
    res = requests.get(
        f"{st.session_state.api_url}rows=0",
        headers=headers(),
        params=st.session_state.params,
    )
    try:
        body = res.json()["message"]
        report_results_dict = dict(body["facets"]["container-title"]["values"])
        for k, v in report_results_dict.items():
            st.session_state.prefix_dict[k] = {"total dois": v}
    except Exception as e:
        st.warning(f"Error on data gathering  \n{e}")


def add_report_data(reports):
    for k, v in published_options().items():
        st.session_state.params["filter"] += published_options().get(k)
        for c in reports:
            # st.session_state.columns.append(f"{k} {c}")
            st.session_state.params["filter"] += report_options().get(c)
            res = requests.get(
                f"{st.session_state.api_url}rows=0",
                headers=headers(),
                params=st.session_state.params,
            )
            # st.write(unquote(res.url))
            try:
                body = res.json()["message"]
                report_results_dict = body["facets"]["container-title"]["values"]
                if report_results_dict:
                    for title, results in report_results_dict.items():
                        st.session_state.prefix_dict[title][f"{k} {c}"] = results
                    calculate_percentage(c, k)
            except TypeError as e:
                st.warning(
                    f"The API query is not valid or no results \
                        available, please clear form and try again  \n{e}"
                )
        current_filter = st.session_state.params.get("filter")
        new_filter = current_filter.replace(v, "").replace(report_options().get(c), "")
        st.session_state.params["filter"] = new_filter


def main():
    st.session_state.published_time = ["back year", "current year"]
    st.session_state.prefix_dict = {}
    st.session_state.params = {
        "facet": "container-title:*",
        "filter": "type:journal-article",
    }
    st.session_state.has_error = False
    st.session_state.threshold = False
    st.session_state.total_results = None
    c1, c2 = st.columns([2, 2])
    prefix = c1.text_input(
        "Add member prefix here",
        key="id",
        placeholder="Examples in the help section ❓",
        help="prefix = 10.5555 or memberID = 1002",
    )
    if prefix:
        find_member()
        if not st.session_state.has_error:
            choose_reports = st.multiselect(
                "Choose Journal percentages to return", options=report_options().keys()
            )
            run_report = st.button("Run reports")
            if choose_reports:
                if run_report:
                    with st.spinner("Getting data...please hold on"):
                        get_title_list()
                        add_by_cy()
                        if st.session_state.prefix_dict:
                            add_report_data(choose_reports)
                            convert_results(st.session_state.prefix_dict)
                        else:
                            st.warning("No data to return")
            else:
                get_title_list()
                add_by_cy()
                if st.session_state.prefix_dict:
                    convert_results(st.session_state.prefix_dict)
                else:
                    st.warning("No data to return")
