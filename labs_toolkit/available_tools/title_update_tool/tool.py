import streamlit as st
import requests
import datetime
import time
import pandas as pd
from bs4 import BeautifulSoup as bs
from labs_toolkit.settings import TEST_ADMIN_BASE_URL, ADMIN_BASE_URL

DETAILS = {
    "tool-name": "Title update tool",
    "description": "Update a bulk list of titles in the admin tool",
}


content_type_codes = {
    "Book": "101",
    "Conference Proceeding": "102",
    "Disseratation": "103",
    "Report": "104",
    "Standard": "105",
    "Database": "106",
    "Conference Series": "107",
    "Book Series": "108",
    "Report Series": "109",
    "Standard Series": "110",
    "Book Set": "111",
    "Posted Content": "112",
    "Grants": "113",
}


def test_or_prod(env):
    if env == "Production":
        return ADMIN_BASE_URL
    else:
        return TEST_ADMIN_BASE_URL


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def data_to_csv2(results):
    df = pd.DataFrame.from_dict(
        results,
        orient="index",
        columns=[
            "Title",
            "New title",
            "Cite ID",
            "Type",
            "Owner prefix(admin)",
            "Content Type",
        ],
    )
    df.index.name = "ISSN/ISBN"
    csv_data = df.to_csv(encoding="utf-8")
    return csv_data


def headers():
    return {
        "User-Agent": "labs-toolkit-tut; mailto=support@crossref.org",
        "Content-Type": "application/x-www-form-urlencoded",
    }


def credentials():
    return {
        "usr": f"{st.session_state.email}/root",
        "pwd": st.session_state.password,
    }


def convert_from_dict(data, columns, index_name):
    df = pd.DataFrame.from_dict(data, orient="index", columns=columns)
    df.index.name = index_name
    return df


def convert_from_dict2(data):
    df = pd.DataFrame.from_dict(data, orient="index")
    return df


def split_ids(ids):
    ids = ids.replace("-", "").split()
    return ids


def data_to_csv(results):
    df = pd.DataFrame.from_dict(
        results,
        orient="index",
        columns=[
            "Title",
            "Cite ID",
            "Total DOIs",
            "Type",
            "Depositor Report",
            "Owner prefix(admin)",
        ],
    )
    df.index.name = "ISSN/ISBN"
    csv_data = df.to_csv(encoding="utf-8")
    return csv_data


def id_returns_params(id: str, admin_code: str):
    return {
        "start": "0",
        "size": "20",
        "func": "modify",
        "issns": id,
        "btype": admin_code,
        "search": "Search",
        "searchType": "contains",
    }


def get_title(id, admin_code):
    data_list = []
    r = requests.post(
        f"{test_or_prod(st.session_state.environment)}/metadataAdmin?func=search",
        data=id_returns_params(id, admin_code) | credentials(),
    )
    page = bs(r.text, "html.parser")
    results = (page.find_all("b")[2].text).split(":")[1].strip()
    if str(results) == "0":
        title = prefix = f"No title matching {id} in {st.session_state.environment}"
        cite_id = ""
        content_type = ""
    else:
        for f in page.find_all("td", attrs={"class": "bafsa"}):
            data_list.append(f.text)
        content_type = data_list[6].strip()
        cite_id = (data_list[0]).strip()
        title = data_list[1].strip()
        if admin_code == "0":
            prefix = data_list[5].strip()
        else:
            prefix = data_list[7].strip()
    return title, cite_id, prefix, content_type


def clean_up_id(id):
    return id.strip().replace(".0", "").replace("-", "")


def id_check(dictionary):
    st.session_state.title_dict = {}
    counter = 0
    progress_bar = st.progress(0, text="Collecting current titles")
    for k, v in dictionary.items():
        counter += 1
        progress_bar.progress(
            counter / len(dictionary),
            text=f"Collecting current titles {counter}/{len(dictionary)}",
        )
        time.sleep(0.1)
        if k.startswith("10."):
            id_type = "DOI"
            title, cite_id, prefix = get_data_from_doi(k)
        elif len(k) > 8:
            id_type = "ISBN"
            admin_code = "1"
            title, cite_id, prefix, content_type = get_title(clean_up_id(k), admin_code)
        else:
            id_type = "ISSN"
            admin_code = "0"
            title, cite_id, prefix, content_type = get_title(clean_up_id(k), admin_code)

        st.session_state.title_dict[clean_up_id(k)] = [
            title,
            v,
            cite_id,
            id_type,
            prefix,
            content_type,
        ]
    return st.session_state.title_dict


def get_title_prefix():
    pass


def sort_content_type(id_type, content_type):
    if id_type == "ISSN":
        return "0"
    else:
        return content_type_codes.get(content_type)


def as_params(data):
    return "&".join([f"{k}={v}" for k, v in data.items()])


def title_change_params(params_dict: dict, new_title, cite_id, id, content_type):
    params_dict[cite_id]["title"] = new_title
    params_dict[cite_id]["btype"] = content_type_codes.get(content_type)
    return params_dict[cite_id]


def book_journal_sort(content_type):
    return "b" if content_type == "ISBN" else "j"


def update_title_params(data_dict, id_type, content_type):
    data_dict["btype"] = sort_content_type(id_type, content_type)
    data_dict["type"] = f"{book_journal_sort(id_type)}cite"
    return data_dict


def update_title(data_dict):
    update_info = st.empty()
    for k, v in data_dict.items():
        title, new_title, cite_id, id_type, prefix, content_type = v
        if id_type == "ISSN":
            content_type = "Journal"
            if cite_id:
                data = title_change_params(
                    get_params(cite_id, id_type, content_type),
                    new_title,
                    cite_id,
                    id_type,
                    content_type,
                )
                r = requests.post(
                    f"{test_or_prod(st.session_state.environment)}/\
                        metadataAdmin",
                    data=credentials(),
                    params=update_title_params(data, id_type, content_type),
                    headers=headers(),
                )
                if r.status_code == 200:
                    update_info.success(
                        f"Cite ID: {cite_id} - Title updated to **{new_title}**"
                    )
                    st.session_state.titles.append((title, new_title))
            else:
                st.session_state.titles_not_updated.append((k, new_title))


def xml_api_params(doi):
    return {"pid": "support@crossref.org", "format": "unixsd", "doi": doi}


def get_data_from_doi(doi):
    title = ""
    prefix = ""
    try:
        r = requests.get(
            "http://doi.crossref.org/search/doi", params=xml_api_params(doi)
        )
        soup = bs(r.text, "html.parser")
        for c_type in soup.crossref:
            if isinstance(c_type.name, str):
                if c_type.name in [
                    "report-paper",
                    "report",
                    "book",
                    "book-series",
                ]:
                    content_type = "book"
                elif c_type.name == "journal":
                    content_type = "journal"
                id = soup.find("crm-item", attrs={"name": f"{content_type}-id"}).text
                prefix = soup.find("crm-item", attrs={"name": "owner-prefix"}).text
                title = soup.find(content_type).find("title").text
    except TypeError:
        id = "DOI not registered"
    return title, id, prefix


def read_csv(file):
    # Check MIME type of the uploaded file
    if file.type == "text/csv":
        df = pd.read_csv(file, dtype=str)
        df.dropna(axis=0, how="all", inplace=True)
    else:
        df = pd.read_excel(file, dtype=str)
        df.dropna(axis=0, how="all", inplace=True)
    dataframe_to_dict(df)


def get_existing_params_params(cite_id: str, content_type_id):
    return {
        "func": "search",
        "start": "0",
        "size": "20",
        "citeId": cite_id,
        "btype": content_type_id,
    }


def get_params(cite_id, id_type, content_type):
    params_dict = {}
    r = requests.post(
        f"{test_or_prod(st.session_state.environment)}/metadataAdmin",
        params=get_existing_params_params(
            cite_id, sort_content_type(id_type, content_type)
        ),
        data=credentials(),
    )
    page = bs(r.text, "html.parser")
    for f in page.find_all("input"):
        name = f.get("name")
        value = f.get("value")
        if name is not None:
            if cite_id not in params_dict.keys():
                params_dict[cite_id] = {name: value}
            else:
                params_dict[cite_id].update({name: value})
        params_dict[cite_id]["mb"] = "Modify"
    return params_dict


def dataframe_to_dict(df):
    df = df.astype(str)
    st.session_state.title_update_dict = {
        key: value.strip() for key, value in dict(df.values).items()
    }
    id_check(st.session_state.title_update_dict)


def get_current_titles(title_dict):
    for v in title_dict.values():
        if v[3] == "ISSN":
            v[5] = "Journal"
    return st.dataframe(
        convert_from_dict(
            title_dict,
            ["Title", "New title", "Cite ID", "ID Type", "Prefix", "Content type"],
            "ID",
        )
    )


def clear_title_dict_session_state():
    if "title_dict" not in st.session_state or not st.session_state.check:
        st.session_state.title_dict = {}


def main():
    st.session_state.titles = []
    st.session_state.title_update_dict = {}
    st.session_state.params_dict = {}
    st.session_state.titles_not_updated = []
    st.session_state.check = False
    st.warning(
        "This tool will only work with ISSNs for journals, of you are wanting to update series titles then please use DOIs as the IDs"
    )
    st.text("If you don't have a template, download the 'title update csv' below")
    st.download_button(
        "Download title update .csv template",
        data=open(
            "labs_toolkit/available_tools/title_update_tool/title_update_template.csv"
        ),
        file_name="title_update_template.csv",
    )
    st.radio(
        "Test or Production", ["Test", "Production"], horizontal=True, key="environment"
    )
    st.text("Add the spreadsheet with the ISSNs/ISBNs and titles")
    title_list = st.file_uploader(
        "Upload title list",
        key="title_list",
        type=["csv", "xls", "xlsx"],
        on_change=clear_title_dict_session_state,
    )
    if title_list:
        if not st.session_state.title_dict:
            st.session_state.check = True
            with st.spinner("Reading csv file and checking current titles..."):
                read_csv(title_list)
                get_current_titles(st.session_state.title_dict)
        update_titles = st.button("Update Titles")
        if update_titles:
            update_title(st.session_state.title_dict)
            st.download_button(
                "Download Data",
                data_to_csv2(st.session_state.title_dict),
                file_name=f"title_updates_{timestamp()}.csv",
            )
