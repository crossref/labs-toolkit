import streamlit as st
import requests
import pandas as pd
from bs4 import BeautifulSoup as bs
from datetime import datetime
from labs_toolkit.settings import PROD_DEPOSIT_URL, ADMIN_BASE_URL, DEPOSITOR_REPORT
import labs_toolkit.common.cr_common_func as common
import re


DETAILS = {
    "tool-name": "Merge titles tool",
    "description": "Merge titles in the system if there are duplicates",
}


CONTENT_TYPES = {
    "101": "Non journal",
    "0": "Journal",
}


@st.cache_data
def get_data(id):
    r = requests.get(DEPOSITOR_REPORT, params=depositor_params(id))
    page = r.text
    df = pd.DataFrame([x.split() for x in page.split("\n")])
    df.columns = df.iloc[0]
    df = df.iloc[:-1, :]
    try:
        dois = list(df["DOI"])
    except Exception:
        st.error(
            "It doesn't look like a valid cite ID has been entered. Please check that you  \
                 have entered the correct ID including J,B or S at the start"
        )
    if len(dois) > 2:
        del dois[0:1]
        dois.remove(None)
        return dois, r.url
    else:
        st.error(
            (
                f"That doesn't look like a valid cite ID {id},"
                + "there are no DOIs against the title or it is an ID from a deleted title."
                + "\n\n  Please try a different ID"
            )
        )
        dois = ""
        return dois, r.url


def depositor_params(id):
    return {"pubid": id}


def credentials():
    return {"usr": f"{st.session_state.email}/root", "pwd": st.session_state.password}


def data_to_csv(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    return csv_data


def content_type(content_type):
    return "0" if content_type[0] in ["J", "j"] else "101"


def search_params(cite_id: str):
    return {
        "start": "0",
        "size": "20",
        "citeId": cite_id[1:],
        "btype": content_type(cite_id),
        "search": "Search",
        "searchType": "contains",
        "type": "Search",
        "func": "search",
    }


def split_ids(ids):
    ids = ids.replace(",", " ").split()
    return ids


def identify_content_type(cite_id):
    return "Journal" if cite_id[0] in ["J", "j"] else "Book"


def create_header():
    return f"H:email={st.session_state.email};type=transfer_title;to{identify_content_type(st.session_state.primary_cite)}={st.session_state.primary_cite[1:]}"


def timestamp():
    return "{:%Y%m%d}".format(datetime.datetime.now())


def create_text_file(dois):
    header = create_header()
    st.session_state.filename = f"merge_file_{st.session_state.content_type}_to_{st.session_state.primary_cite}.txt"
    with open(st.session_state.filename, "w") as f:
        f.write(f"{header}\n")
        for d in dois:
            f.write(f"{d}\n")


def form_data(email, password):
    mydata = {
        "operation": "doTransferDOIsUpload",
        "login_id": f"{email}/root",
        "login_passwd": password,
    }
    return mydata


def headers():
    return {
        "User-Agent": "labs-toolkit-ttt; mailto=support@crossref.org",
        "Content-Type": "application/x-www-form-urlencoded",
    }


def as_params(data):
    return "&".join([f"{k}={v}" for k, v in data.items()])


def member_params(mem_id: str):
    return {"start": "0", "size": "20", "publisherID": mem_id, "sf": "search"}


def title_params(cite_id: str, content_type_id):
    return {
        "start": "0",
        "size": "20",
        "func": "modify",
        "citeId": cite_id[1:],
        "btype": content_type_id,
    }


def get_params(cite_id):
    title_params_dict = {}
    r = requests.get(
        f"{ADMIN_BASE_URL}metadataAdmin",
        data=title_params(cite_id, content_type(cite_id)) | credentials(),
    )
    page = bs(r.text, "html.parser")
    for f in page.find_all("input"):
        name = f.get("name")
        value = f.get("value")
        if name is not None:
            if cite_id not in title_params_dict:
                title_params_dict[cite_id] = {name: value}
            else:
                title_params_dict[cite_id].update({name: value})
    return title_params_dict


def which_content_type(content_type):
    content_type_dict = {"Book": "b", "Journal": "j"}
    return content_type_dict.get(content_type, "s")

    # return "b" if content_type == "Book" else "j" if content_type == "Journal" else "s"


def get_title(id):
    html_text = requests.post(
        f"{ADMIN_BASE_URL}/metadataAdmin",
        data=search_params(id) | common.credentials(),
    ).text
    soup = bs(html_text, "html.parser")
    title = soup.find("input", attrs={"name": "title"})["value"]
    member_id = soup.find("input", attrs={"name": "depositor"})["value"]
    member_name = find_publisher_details_admin(member_id)[1]
    return title, member_id, member_name


def get_elements(rows):
    del rows[0]
    return select_pub_id([line.find_all("td") for line in rows])


def select_pub_id(rows):
    return (
        rows[0][1].text.strip(),
        re.sub("[^A-Za-z0-9]+", " ", str(rows[0][3].contents).split("value")[1]),
    )


def get_table_rows(table):
    return get_elements([(row) for row in table("tr")])


def find_publisher_details_admin(id):
    r = requests.post(
        f"{ADMIN_BASE_URL}/publisherUserAdmin",
        params=member_params(id) | common.credentials(),
    )
    return get_table_rows(bs(r.text, "html.parser").find_all("table")[5])


def update_title_params(type_name, cite):
    return {"type": type_name, "cite": cite}


def update_title(params_dict):
    params_dict["mb"] = "Delete"
    params_dict["title"] = f"{params_dict.get('title')}-DELETED"
    r = requests.get(
        "https://doi.crossref.org/servlet/metadataAdmin",
        params=update_title_params(
            which_content_type(st.session_state.content_type), as_params(params_dict)
        ),
        data=credentials(),
        headers=common.headers(st.session_state.tool_name),
    )
    if r.status_code == 200:
        st.write(f"Title updated to {params_dict.get('title')}")
    else:
        st.warning(f"{r.status_code} code - Check again")


def update_titles_delete():
    for id in st.session_state.merge_ids:
        data = get_params(id, st.session_state.content_type)
        st.write(data)
        update_title(data)


def dois_expander(dois, member_name, title, id):
    st.session_state.doi_list.extend(dois)
    with st.expander(
        f"Title - {title}  Cite ID - {id}  Owner - {member_name}  No of DOIs - {len(dois)}"
    ):
        st.write(dois)


def merge_titles():
    st.button("Merge titles?", key="merge")
    if st.session_state.merge:
        update_titles_delete()
        with open(st.session_state.filename, "r") as file:
            myfile = {"fname": file}
            r = requests.post(
                PROD_DEPOSIT_URL,
                files=myfile,
                data=form_data(st.session_state.email, st.session_state.password),
            )
            if r.status_code == 200:
                st.success(
                    f"Text file created and deposited **{file.name}** was successfully sent to the queue  :white_check_mark"
                )
                st.download_button(
                    "Download Text File", file, file_name=st.session_state.filename
                )
            else:
                st.warning(
                    f"The file **{file.name}** has failed to reach the queue, please try again later"
                )


def dois_added(dois, member_name, title, id):
    dois_expander(dois, member_name, title, id)
    create_text_file(st.session_state.doi_list)
    st.success("Success - DOIs collected")
    merge_titles()


def main():
    if "merge" not in st.session_state:
        st.session_state.merge = False
    if not st.session_state["merge"]:
        st.session_state.doi_list = []
        c1, c2 = st.columns([3, 8])
        st.text(
            "Please enter the cite ID including the content type first letter e.g. J12345, B12345"
        )
        cite_ids = c1.text_input("Enter the primary cite ID", key="primary_cite")

        titles_for_merge = c2.text_input(
            "Enter the cite IDs for the titles to merge (separate with space or comma)",
            key="titles_for_merge",
        )
        st.button("Run Form", key="run_form")
        if st.session_state.run_form:
            if not cite_ids:
                st.warning("Please enter primary cite ID")
            else:
                st.session_state.merge_ids = split_ids(titles_for_merge)
                with st.spinner("Collect DOIs from merging titles"):
                    primary_title, primary_mem_id, primary_mem_name = get_title(
                        st.session_state.primary_cite
                    )
                    st.info(
                        (
                            "Merging into:  \n"
                            + f"Title: **{primary_title}**:"
                            + f"Title ID: **{st.session_state.primary_cite}** "
                            + f"\nCurrently owned by **{primary_mem_name.strip()}**"
                        )
                    )
                    for st.session_state.id in st.session_state.merge_ids:
                        st.session_state.dois, url = get_data(st.session_state.id)
                        (
                            st.session_state.title,
                            st.session_state.member_id,
                            st.session_state.member_name,
                        ) = get_title(st.session_state.id)
                        if primary_mem_id != st.session_state.member_id:
                            st.warning(
                                (
                                    f"**{st.session_state.title}**: {st.session_state.id}  \n"
                                    + f"Is under the ownership of: {st.session_state.member_name}.  \n"
                                    + f"It is not under the same ownership as {primary_title}: {st.session_state.primary_cite}   \n"
                                    + f"which is under the ownership of {primary_mem_name}.   \n\n"
                                    + "DOIs have not been added to merge list"
                                )
                            )
                            proceed = st.button("Do you want to proceed anyway?")
                            if proceed:
                                st.session_state["merge"] = True
                                dois_added(
                                    st.session_state.dois,
                                    st.session_state.member_name,
                                    st.session_state.title,
                                    st.session_state.id,
                                )
                        else:
                            st.session_state["merge"] = True
                            dois_added(
                                st.session_state.dois,
                                st.session_state.member_name,
                                st.session_state.title,
                                st.session_state.id,
                            )
