import streamlit as st
import datetime
import pdfplumber
import pandas as pd
import labs_toolkit.common.cr_common_func as common
import requests

DETAILS = {
    "tool-name": "Invoice Splitter",
    "description": "Tool to split invoices from Crossref into a line and prefix breakdwon there invoices by line and download into CSV",
}


columns = [
    "Item Code",
    "Quantity",
    "Cost per item",
    "Total cost",
    "Item Description",
    "Month Deposited",
    "Prefix",
    "Username",
]


def dict_convert_to_table(results):
    df = pd.DataFrame.from_dict(results, orient="index")
    df.index.name = "Prefix"
    return df


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def extract_data(feed):
    data = []
    with pdfplumber.open(feed) as pdf:
        counter = 0
        pages = pdf.pages
        invoice = []
        st.session_state.member = extract_member(pages)
        progress_bar = st.progress(0, text="Collating the data in the invoice")
        for p in pages:
            progress_bar.progress(counter + 1, text="Collating the data in the invoice")
            items = p.extract_table()
            if items:
                data.append(p.chars)
                invoice.append(items)
        progress_bar.empty()
        items = [item for sublist in invoice for item in sublist]
        search_invoice(items)
        st.write(
            f"""Here is a breakdown
            for the invoice from 
            ***{st.session_state.member}***"""
        )


def extract_member(pages):
    bounding_box = (0, 240, 300, 242)
    crop_area = pages[0].crop(bounding_box)
    crop_text = crop_area.extract_text().split("\n")
    member = crop_text[0]
    return member


def search_invoice(item_lists):
    item_lists = item_lists[1:-2]
    desired_list = merge_page_break(item_lists)
    item_codes = [
        "40801",
        "40802",
        "40870",
        "40851",
        "40850",
        "40821",
        "40816",
        "40830",
        "40861",
        "40810",
        "40895",
        "40812",
        "40808",
        "40899",
        "40808",
        "40805",
        "40811",
        "40806",
    ]
    counter = 0
    for item_list in desired_list:
        counter += 1
        if any(item in item_codes for item in item_list):
            new_list = item_list + split_info_line(item_list[1])
            del new_list[1:3]
            del new_list[7]
            st.session_state.invoice_lines.append(new_list)


def merge_page_break(original_list):
    merge = []
    desired_list = []
    for x in original_list:
        if merge:
            merge[0][1] = merge[0][1] + "\n" + x[1]
            desired_list.append(merge[0])
            merge = []
        if len(x[1].split("\n")) == 1 and x[0]:
            merge.append(x)
        else:
            desired_list.append(x)
    return desired_list


def split_info_line(line):
    data = []
    line = line.split("\n")
    data.append(line[0])
    for i in line[1].replace(" ", "").replace("(", "").replace(")", "").split(":"):
        data.append(i)
    return data


def sort_by_prefix():
    prefix_dict = {}
    for s in st.session_state.invoice_lines:
        if s[6] not in prefix_dict:
            prefix_dict[s[6]] = [s]
        else:
            prefix_dict[s[6]].append(s)
    return accumulate_prefix(prefix_dict)


def prefix_params(prefix):
    return {"start": "0", "size": "20", "doi": prefix, "sf": "search"}


def get_member_name_api(prefix):
    r = requests.get(
        f"https://api.crossref.org/prefixes/{prefix}",
        headers=common.headers("invoice_splitter"),
    )
    name = r.json()["message"]["name"] if r.status_code == 200 else "No publisher name"
    return name


def accumulate_prefix(prefix_dict):
    st.session_state.prefix_dict = {}
    for k, v in prefix_dict.items():
        member_name = get_member_name_api(v[0][6])
        quantity_total = sum(int(line[1]) for line in v)
        cost_total = sum(float((line[3][1:]).replace(",", "")) for line in v)
        st.session_state.prefix_dict[k] = {
            "Member Name": member_name,
            "Total Items": quantity_total,
            "Total Cost": f"${cost_total}",
        }
    add_totals()
    return st.session_state.prefix_dict


def add_totals():
    total_items = sum(
        int(d["Total Items"]) for d in st.session_state.prefix_dict.values() if d
    )
    total_cost = sum(
        float(d["Total Cost"][1:]) for d in st.session_state.prefix_dict.values() if d
    )
    st.session_state.prefix_dict["Totals"] = {
        "Member Name": "Totals",
        "Total Items": total_items,
        "Total Cost": f"${total_cost}",
    }


def full_date_func(month, year):
    if month in ["01", "02", "03"]:
        date = f"{year}-02-01"
        quarter = "Q1"
    elif month in ["04", "05", "06"]:
        date = f"{year}-05-01"
        quarter = "Q2"
    elif month in ["07", "08", "09"]:
        date = f"{year}-08-01"
        quarter = "Q3"
    else:
        date = f"{year}-11-01"
        quarter = "Q4"
    return date, quarter


def main():
    columns = [
        "Item Code",
        "Quantity",
        "Cost per item",
        "Total cost",
        "Item Description",
        "Month Deposited",
        "Prefix",
        "Username",
    ]
    st.session_state.invoice_lines = []
    file_upload = st.file_uploader(
        "Upload the PDF invoice to split",
        key="file_upload",
        accept_multiple_files=False,
        type="pdf",
    )
    get_reports = st.button("Get Reports")
    if get_reports:
        if file_upload:
            with st.spinner("Splitting the invoice..."):
                if file_upload is not None:
                    extract_data(file_upload)
                    df = pd.DataFrame(st.session_state.invoice_lines, columns=columns)
                    df.index += 1
                    with st.expander("Prefix Breakdown"):
                        prefix_breakdown = dict_convert_to_table(sort_by_prefix())
                        st.download_button(
                            "Download Prefix Breakdown",
                            data=common.results_to_csv(
                                prefix_breakdown,
                                ["Member Name", "Total Items", "Total Cost"],
                                True,
                            ),
                            file_name=f"{common.timestamp_now()}_prefix_breakdown.csv",
                        )
                        st.dataframe(prefix_breakdown)

                    st.dataframe(df)
                    st.download_button(
                        "Download CSV",
                        data=common.results_to_csv(df, columns, False),
                        file_name=f"{common.timestamp_now()}_invoice.csv",
                    )
        else:
            st.warning("Please add a PDF to search on")
