###Crossref Colors###

CR_PRIMARY_COLORS = {
    "CR_PRIMARY_RED": "#ef3340",
    "CR_PRIMARY_BLUE": "#3eb1c8",
    "CR_PRIMARY_YELLOW": "#ffc72c",
    "CR_PRIMARY_LT_GREY": "#d8d2c4",
    "CR_PRIMARY_DK_GREY": "#4f5858",
    "CR_PRIMARY_DK_BLUE": "#017698",
}


###Crossref URLs###

API_URI_NO_FS = "https://api.crossref.org"
API_URI = "https://api.crossref.org/"
API_URI_WORKS = "https://api.crossref.org/works"
API_URI_WORKS_WITH_FS = "https://api.crossref.org/works/"
TEST_DEPOSIT_URL = "https://test.crossref.org/servlet/deposit"
PROD_DEPOSIT_URL = "https://doi.crossref.org/servlet/deposit"
PROD_LOGIN_URL = "https://doi.crossref.org/servlet/login"
XML_API = (
    "http://doi.crossref.org/search/doi?pid=support@crossref.org&format=unixsd&doi="
)
ADMIN_BASE_URL = "https://doi.crossref.org/servlet"
TEST_ADMIN_BASE_URL = "https://test.crossref.org/servlet"
DEPOSITOR_REPORT = "https://data.crossref.org/depositorreport"
CR_AUTH_ENDPOINT = "https://doi.crossref.org/servlet/login"

###Assets###
CROSSREF_LABS_LOGO = "labs_toolkit/utils/labs-logo-ribbon4.png"
CROSSREF_LOGO_REAL = "https://assets.crossref.org/logo/crossref-logo-landscape-200.png"
CROSSREF_ICON = "https://assets.crossref.org/logo/crossref-logo-200.svg"


###Third Party URLs###
CLEARBIT_API = "https://logo.clearbit.com/"

###Thresholds###
INT_THRESHOLD = 3000000
PUB_THRESHOLD = 10000


###Data###

DEPOSITOR_REPORT = "https://data.crossref.org/depositorreport"


###Tool environment breakdown###

PUBLIC_ONLY = [
    "invoice_splitter",
    "multiple_resolution_sec_unlock",
    "search_on_dois",
    "api_query_url_download_tool",
    "_help_file",
    "multiple_resolution_unlock_dois",
    "suffix_generator",
    "country_prep_tool",
]

MEMBERS_ONLY = [
    "submission_downloader",
    "upload_tool",
    "cited_by_matches_retrieval_tool",
    "conflict_report_tool",
    "doi_checker",
]

INTERNAL_ONLY = [
    "issn_isbn_query_tool",
    "title_transfer_tool",
    "add_usernames",
    "membership_tools",
    "member_crawler_report",
    "delete_dois",
    "successful_submission_rerunner",
    "title_update_tool",
    "issn_return_tool",
    "legacy_doi_helper_tool",
    "api_item_downloader",
    "submission_rerunner_tool",
    "timestamp_reset_tool",
    "reports_tool",
    "similarity_check_eligibility_tool",
    "csv_upload_tool",
    "submission_diagnostic_tool",
    "cite_id_retrieval_tool",
    "content_negotiation",
    "depositor_report_owner_checker",
    "quarterly_reports_tool",
    "crossref_dashboard",
    "merge_titles_tool",
]


DEFAULT_STATE = {
    "inited": False,
    "tool_name": "_help_file",
    "login_type": "public",
    "email": "Public User",
}


PARAMS_TO_SAVE = ["login_type", "tool_name"]
