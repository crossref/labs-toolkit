"""Home page shown when the user enters the application"""

import streamlit as st


# pylint: disable=line-too-long
def main():
    """Used to write the page in the app.py file"""
    with st.spinner("Loading Home ..."):
        st.write(
            """

			### Home Page for tool collection

			Welcome to the tool collection for members with\
                  various Labs tools to try and maximise the user
			for our members of the Crossref services.
"""
        )
