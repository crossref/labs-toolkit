import streamlit as st
import requests
import datetime
import pandas as pd
from labs_toolkit.settings import API_URI
from collections import Counter


def back_year_calc():
    return int(datetime.datetime.now().year) - 2


def year_options():
    this_year = int(datetime.datetime.now().year)
    return range(this_year, this_year - 10, -1)


def headers():
    return {"User-Agent": "labs-toolkit-db-reports; mailto:support@crossref.org"}


def timestamp():
    return "{:%Y-%m-%d-%H-%M}".format(datetime.datetime.now())


def convert_output(output):
    data = pd.DataFrame(output)
    csv_data = data.to_csv(index=False, encoding="utf_8")
    return csv_data


def get_reports():
    return f"https://api.crossref.org/prefixes/{st.session_state.id}/works?filter=from-created-date:{st.session_state.from_date},until-created-date:{st.session_state.until_date}"


def define_quarters():
    if st.session_state.quarter_pick == "Quarter 1":
        from_date = f"{st.session_state.year}-01-01"
        until_date = f"{st.session_state.year}-03-31"
    elif st.session_state.quarter_pick == "Quarter 2":
        from_date = f"{st.session_state.year}-04-01"
        until_date = f"{st.session_state.year}-06-30"
    elif st.session_state.quarter_pick == "Quarter 3":
        from_date = f"{st.session_state.year}-07-01"
        until_date = f"{st.session_state.year}-09-30"
    else:
        from_date = f"{st.session_state.year}-10-01"
        until_date = f"{st.session_state.year}-12-31"
    st.session_state.from_date = from_date
    st.session_state.until_date = until_date


def data_request(url):
    item_list = []
    cursor = "*"
    rows = 1000
    url_check = requests.head(f"{url}", headers=headers()).status_code
    if url_check == 200:
        while True:
            res = requests.get(f"{url}&cursor={cursor}&rows={rows}", headers=headers())
            try:
                body = res.json()["message"]
                items = body["items"]
                if not items:
                    break
                cursor = str(body["next-cursor"])
                for i in items:
                    item_list.append(i)
            except TypeError as e:
                st.warning(
                    f"The API query is not valid or no results\
                          available, please clear form and try again  \n{e}"
                )
    elif str(url_check.status_code).startswith("5"):
        st.error(
            f"{url_check.status_code} Error returned...the API could\
                  be down or the query is not valid"
        )
    else:
        st.warning(f"Error code: {url_check.status_code}")
        st.warning("Valid JSON has not been retrieved")
    return item_list


def find_member():
    try:
        r = requests.get(f"{API_URI}prefixes/{st.session_state.id}", headers=headers())
        st.session_state.member = r.json()["message"]["name"]
        st.session_state.api_url = f"{API_URI}prefixes/{st.session_state.id}/works?"
        member = f"Searching on member {st.session_state.member}  \
            \nSearch ID: {st.session_state.id}"
        st.info(member)
    except Exception:
        st.warning(f"Error: No member against {st.session_state.id}  \n{r.url}")


def create_list(item_list):
    report_list = []
    for i in item_list:
        try:
            content_type = i["type"]
            if content_type in ["journal", "book", "component", "conference"]:
                continue
            else:
                title = i["container-title"][0]
                doi = i["DOI"]
                published_year = i["published"]["date-parts"][0][0]
                if published_year < back_year_calc():
                    fee = "Back Year"
                else:
                    fee = "Current Year"
        except KeyError:
            title = "No title Data"
            doi = i["DOI"]
        report_list.append([content_type, title, doi, published_year, fee])
    # st.write(report_list)
    return report_list


def sort_list(report_list):
    journal_articles = []
    book_chapters = []
    proceedings = []
    dissertations = []
    components = []
    for r in report_list:
        if r[0] == "journal-article":
            journal_articles.append(r)
        elif r[0] == "book-chapter":
            book_chapters.append(r)
        elif r[0] == "proceedings-article":
            proceedings.append(r)
        elif r[0] == "dissertation":
            dissertations.append(r)
        elif r[0] == "component":
            components.append(r)

    if journal_articles:
        make_df(journal_articles, "Journal Articles")
    if book_chapters:
        make_df(book_chapters, "Book Chapters")
    if proceedings:
        make_df(proceedings, "Proceedings")
    if dissertations:
        make_df(dissertations, "Dissertations")


def make_df(data, content_type):
    title_counts = {}
    title_dictionary = {}
    # st.dataframe(data)
    for d in data:
        title = d[1]
        b_c = d[4]
        if title in title_counts.keys():
            title_counts[title].append(f"{content_type} {b_c}")
        else:
            title_counts[title] = [f"{content_type} {b_c}"]
    for k, v in title_counts.items():
        title_dictionary[k] = Counter(v)
    # st.write(title_dictionary)
    show_report(title_dictionary, content_type)


def show_report(data, content_type):
    with st.expander(
        f"Report for {content_type} - \
            {st.session_state.quarter_pick}: \
                {st.session_state.year}",
        expanded=False,
    ):
        df = pd.DataFrame.from_dict(
            data,
            orient="index",
            columns=[f"{content_type} Back Year", f"{content_type} Current Year"],
        )
        df.fillna(0, inplace=True)
        df = df.astype(
            {f"{content_type} Back Year": "int", f"{content_type} Current Year": "int"}
        )
        st.dataframe(df)
        show_download(df)


def show_download(results):
    data = pd.DataFrame(results)
    csv_data = data.to_csv(encoding="utf-8")
    st.download_button(
        "Download Dataframe", csv_data, file_name=f"quarter_report_{timestamp()}.csv"
    )


def main():
    # st.write(st.session_state)
    st.session_state.has_error = False
    st.session_state.threshold = False
    st.session_state.total_results = None
    c1, c2 = st.columns([2, 2])
    c1.text_input(
        "Add member prefix here",
        key="id",
        placeholder="Examples in the help section ❓",
        help="prefix = 10.5555 or memberID = 1002",
        on_change=find_member,
    )
    year = c2.selectbox(
        "Add the year you want the reports from", key="year", options=year_options()
    )
    if year:
        st.radio(
            "Which quarter do you want the report for",
            key="quarter_pick",
            options=["Quarter 1", "Quarter 2", "Quarter 3", "Quarter 4"],
            horizontal=True,
            format_func=lambda x: f"{str(x)} - {year}",
        )
    reports_button = st.button("Get Billing Reports")
    if reports_button:
        define_quarters()
        report_list = create_list(data_request(get_reports()))
        sort_list(report_list)
        # else:
    # 	st.warning("User URL field is missing a value.")


if __name__ == "__main__":
    main()
