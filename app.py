import streamlit as st
import labs_toolkit.cr_auth as cr_auth
from labs_toolkit.settings import (
    CROSSREF_LABS_LOGO,
    PARAMS_TO_SAVE,
    DEFAULT_STATE,
    PUBLIC_ONLY,
    MEMBERS_ONLY,
    INTERNAL_ONLY,
)
import labs_toolkit.available_tools as tools
import logging
import labs_toolkit.common.cr_common_func as common


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger()


st.set_page_config(
    page_title="Labs Toolkit",
    page_icon=CROSSREF_LABS_LOGO,
)


# Main
def cast_param_value(param, value):
    """Cast param value to correct type"""
    # Streamlit treats all params as lists, even if they are single values
    # So we need to convert them back to single values when appropriate
    # we use the values in DEFAULT_SHARED_STATE to determine if a param
    # should be a list or a single value
    truthy_values = {"true", "1", "t", "y", "yes", "yeah", "yup", "certainly", "uh-huh"}
    if isinstance(DEFAULT_STATE[param], list):
        return value
    if isinstance(DEFAULT_STATE[param], bool):
        return value[0].lower() in truthy_values
    return value[0]


def params_to_state():
    """Copy params from URL to session state"""
    for k in st.query_params:
        if k in PARAMS_TO_SAVE:
            st.session_state[k] = cast_param_value(k, st.query_params.get_all(k))


def state_to_params():
    """Copy session state to URL"""
    for k, v in st.session_state.items():
        if k in PARAMS_TO_SAVE:
            st.query_params[k] = v


def new_session():
    """Initialize session state"""
    st.session_state.update(DEFAULT_STATE)


def persist_widget_value(key):
    st.session_state[key] = st.session_state[f"_{key}"]


def restore_widget_value(key):
    st.session_state[f"_{key}"] = st.session_state[key]


def strike(text):
    result = ""
    for c in text:
        result = result + c + "\u0336"
    return result


def show_sidebar():
    with st.sidebar:
        st.markdown(
            """<style>.css-hxt7ib {padding-top: 1rem;} </style>""",
            unsafe_allow_html=True,
        )
        cr_auth.show_logged_in_as()
        st.write("**Tool Choice**")
        restore_widget_value("tool_name")
        st.selectbox(
            "Select Tool from dropdown",
            options=sorted(list(st.session_state.tools_allowed)),
            key="_tool_name",
            format_func=lambda x: tool_name_actual(x).tool.DETAILS["tool-name"],
            on_change=persist_widget_value,
            args=["tool_name"],
        )
        st.markdown("***")
        st.write(
            f"**Tool Summary - {tool_name_actual(st.session_state.tool_name).tool.DETAILS['tool-name']}**"
        )
        if st.session_state.tool_name:
            st.info(
                tool_name_actual(st.session_state.tool_name).tool.DETAILS["description"]
            )
        else:
            st.session_state.internal_tool_table_dict.get(" - Help File")


def tools_allowed():
    if st.session_state.login_type == "public":
        st.session_state["tools_allowed"] = sorted(PUBLIC_ONLY)
    elif st.session_state.login_type == "member":
        st.session_state["tools_allowed"] = sorted(PUBLIC_ONLY + MEMBERS_ONLY)
    elif st.session_state.login_type == "internal":
        st.session_state["tools_allowed"] = sorted(
            PUBLIC_ONLY + MEMBERS_ONLY + INTERNAL_ONLY
        )
    for file_name in st.session_state["tools_allowed"]:
        __import__(f"labs_toolkit.available_tools.{file_name}.tool")


def tool_name_actual(tool_name):
    return getattr(tools, tool_name)


### This is where the main app logic starts
def main():
    tools_allowed()
    show_sidebar()
    try:
        st.session_state.tool_module = tool_name_actual(st.session_state.tool_name)
    except Exception:
        st.error(
            f"There is no tool called **{st.session_state.tool_module.tool.DETAILS['tool-name']}** in this environment  \n\n"
            + "Please find available tools in the dropdown on the left"
        )
        st.stop()
    st.subheader(st.session_state.tool_module.tool.DETAILS["tool-name"])
    logger.info(
        f"{common.timestamp_now()} \
            {st.session_state.email.split('@')[0]} \
                accessed tool {st.session_state.tool_name}"
    )
    st.session_state.tool_module.tool.main()


if "inited" not in st.session_state:
    new_session()
    params_to_state()
state_to_params()

if st.session_state.login_type == "public":
    main()
else:
    if cr_auth.first_login():
        # set user and password state variables to none
        cr_auth.init_login()

    if cr_auth.all_credenitials_provided():
        if "root" in st.session_state.available_roles:
            # if we have all our credentials, run the app
            st.session_state.login_type = "internal"
        else:
            st.session_state.login_type = "member"
        main()
    elif not cr_auth.authenticated():
        # if we have not authenticated, then show login screen
        cr_auth.show_login()
    else:
        # if we have authenticated but not yet seleted a role, show role selector
        cr_auth.show_role_selector()
