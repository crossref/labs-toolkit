import datetime
import labs_toolkit.available_tools.upload_tool.tool as tool


def test_timestamp():
    assert tool.timestamp() == datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")


def test_form_data():
    operation = "doMDUpload"
    email = "test@example.com"
    password = "password"
    role = "role"
    expected_data = {
        "operation": operation,
        "login_id": f"{email}/{role}",
        "login_passwd": password,
    }
    assert tool.form_data(operation, email, password, role) == expected_data


def test_environ_url():
    assert tool.environ_url("Test") == tool.TEST_DEPOSIT_URL
    assert tool.environ_url("Production") == tool.PROD_DEPOSIT_URL
