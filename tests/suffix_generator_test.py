from labs_toolkit.available_tools.suffix_generator.tool import generate_dois

test_prefix = "10.5555"


def test_generate_dois():
    dois = generate_dois(test_prefix, 5)
    assert len(dois) == 5
    for doi in dois:
        assert doi.startswith(test_prefix)
