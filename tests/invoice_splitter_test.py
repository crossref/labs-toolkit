import pytest
from unittest.mock import MagicMock
from labs_toolkit.available_tools.invoice_splitter.tool import (
    extract_member,
    split_info_line,
)


@pytest.fixture
def mock_pdfplumber():
    return MagicMock()


def test_extract_member(mock_pdfplumber):
    mock_pages = [MagicMock()]
    mock_pages[0].crop.return_value.extract_text.return_value = (
        "Member123\nSome other text"
    )
    member = extract_member(mock_pages)
    assert member == "Member123"


def test_split_info_line():
    line = "Some text\nKey: Value"
    split_data = split_info_line(line)
    expected_data = ["Some text", "Key", "Value"]
    assert split_data == expected_data
