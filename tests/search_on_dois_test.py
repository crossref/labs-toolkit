from unittest.mock import patch, MagicMock
from labs_toolkit.available_tools.search_on_dois.tool import (
    custom_route,
    split_dois,
)


def test_split_dois():
    assert split_dois("doi1 doi2 doi3") == ["doi1", "doi2", "doi3"]
    assert split_dois("") == []


# Define custom_list for testing
custom_list = {"Title": "title", "Authors": "author", "ISSN": "ISSN"}


def test_custom_route_api_success():
    dois = ["10.1234/5678", "10.9876/5432"]
    custom_choice = ["Title", "Authors"]

    with patch("requests.get") as mock_get:
        # Mock the response
        mock_response = MagicMock()
        mock_response.json.return_value = {
            "message": {"title": "Test Title", "author": [{"name": "Paul Davis"}]}
        }
        mock_get.return_value = mock_response

        output_dict = custom_route(dois, custom_choice)

        # Assertions
        assert len(output_dict) == 2
        assert output_dict == {
            "10.1234/5678": {"Title": "Test Title", "Authors": {"name": "Paul Davis"}},
            "10.9876/5432": {"Title": "Test Title", "Authors": {"name": "Paul Davis"}},
        }
