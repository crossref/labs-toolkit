import unittest
from unittest.mock import patch
import labs_toolkit.available_tools.country_prep_tool.tool as tool


class TestCountryParticipationReport(unittest.TestCase):

    @patch("countryflag.getflag", return_value="🇺🇸")
    def test_get_flag_valid_country(self, mock_getflag):
        self.assertEqual(tool.get_flag("US"), "🇺🇸")

    @patch("countryflag.getflag", side_effect=ValueError)
    def test_get_flag_invalid_country(self, mock_getflag):
        self.assertEqual(tool.get_flag("INVALID"), "noflag")

    def test_collate_resource_type_reports(self):
        data_dict = {
            "journal-article": {"affiliations": 50, "orcids": 40},
            "book-chapter": {"affiliations": 70, "licenses": 60},
        }
        result = tool.collate_resource_type_reports(data_dict)
        expected = {"affiliations": [50, 70], "orcids": [40], "licenses": [60]}
        self.assertDictEqual(result, expected)

    def test_average_the_lists(self):
        data_dict = {"affiliations": [0.50, 0.60], "orcids": [0.40, 0.50]}
        result = tool.average_the_lists(data_dict, "Test Country")
        expected = {"average_Test Country": {"affiliations": 55, "orcids": 45}}
        self.assertDictEqual(result, expected)

    def test_average_the_lists_2(self):
        test_data = {
            "affiliations": [0.8201, 0, 0, 0.5379, 0],
            "abstracts": [0.8489, 1, 0.9375, 0.9303, 1],
            "orcids": [
                0.6978,
                0.3181,
                0.5,
                0.9810,
                0.0476,
            ],
            "licenses": [
                0.9856,
                0.2272,
                0.9375,
                0.9936,
                1,
            ],
            "references": [0.8489, 0, 0, 0.8417, 0.6],
            "funders": [0, 0, 0, 0, 0],
            "similarity-checking": [
                1,
                0.7727,
                1,
                0.9240,
                0.9809,
            ],
            "award-numbers": [0, 0, 0, 0, 0],
            "ror-ids": [0, 0, 0, 0, 0],
            "update-policies": [0, 0, 0, 0, 0],
            "resource-links": [
                0.1438,
                0.7727,
                1,
                0.4367,
                0.9809,
            ],
            "descriptions": [0, 0, 0, 0, 0],
        }
        average_report = tool.average_the_lists(test_data, "Toolkit Test Country")
        assert average_report == {
            "average_Toolkit Test Country": {
                "affiliations": 27,
                "abstracts": 94,
                "orcids": 50,
                "licenses": 82,
                "references": 45,
                "funders": 0,
                "similarity-checking": 93,
                "award-numbers": 0,
                "ror-ids": 0,
                "update-policies": 0,
                "resource-links": 66,
                "descriptions": 0,
            }
        }

    @patch("plotly.express.bar")
    @patch("streamlit.plotly_chart")
    def test_display_graph(self, mock_plotly_chart, mock_px_bar):
        data = {"country": {"affiliations": [50], "orcids": [40]}}
        index = ["affiliations", "orcids"]
        ids = [1]

        tool.display_graph(data, index, ids)

        mock_px_bar.assert_called_once()
        mock_plotly_chart.assert_called_once()
