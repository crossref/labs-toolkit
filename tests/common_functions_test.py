import datetime
import labs_toolkit.common.cr_common_func as cr_common_func


def test_timestamp_now():
    assert cr_common_func.timestamp_now() == datetime.datetime.now().strftime(
        "%Y-%m-%d"
    )


def test_yesterday():
    assert cr_common_func.yesterday() == (
        datetime.datetime.now() - datetime.timedelta(days=1)
    ).strftime("%Y-%m-%d")


def test_year_earlier():
    date = datetime.datetime.now() - datetime.timedelta(days=365)
    assert cr_common_func.year_earlier(date) == (
        date - datetime.timedelta(days=365)
    ).strftime("%Y-%m-%d")


def test_timestamp():
    assert cr_common_func.timestamp() == datetime.datetime.now().strftime(
        "%Y-%m-%d-%H-%M"
    )


def test_load_file(tmp_path):
    test_file = tmp_path / "test.txt"
    test_file.write_text("Test data")
    assert cr_common_func.load_file(test_file) == "Test data"


def test_split_ids():
    ids = "123-456, 789 , 1 101112"
    assert cr_common_func.split_ids(ids) == [
        "123456",
        "789",
        "1101112",
    ]


def test_results_to_csv():
    results = [{"A": 1, "B": 2}, {"A": 3, "B": 4}]
    columns = ["A", "B"]
    index = False
    expected_csv = "A,B\n1,2\n3,4\n"
    assert cr_common_func.results_to_csv(results, columns, index) == expected_csv


def test_headers():
    app_name = "your_app_name"
    expected_headers = {
        "User-Agent": f"labs-toolkit-{app_name}; mailto=support@crossref.org",
        "Content-Type": "application/x-www-form-urlencoded",
    }
    assert cr_common_func.headers(app_name) == expected_headers
