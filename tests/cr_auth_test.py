import pytest
from unittest.mock import patch
import streamlit as st

import requests
from labs_toolkit.settings import CR_AUTH_ENDPOINT
from labs_toolkit.cr_auth import (
    tarpit,
    first_login,
    authenticated,
    role_selected,
    all_credenitials_provided,
    init_login,
    cr_authenticate,
    fake_authenticate,
    authenticate,
    select_role,
    show_role_selector,
)


@pytest.fixture
def setup_session_state():
    st.session_state.clear()
    st.session_state.update(
        {
            "login_type": "public",
            "auth": False,
            "role_selected": False,
            "tarpit": 0,
            "email": "test@example.com",
            "available_roles": ["user"],
            "role": "user",
        }
    )


def test_tarpit(setup_session_state):
    with patch("time.sleep", return_value=None):
        tarpit()
        assert st.session_state.tarpit == 2


def test_first_login(setup_session_state):
    assert not first_login()


def test_authenticated(setup_session_state):
    assert not authenticated()
    st.session_state.auth = True
    assert authenticated()


def test_role_selected(setup_session_state):
    assert not role_selected()
    st.session_state.role_selected = True
    assert role_selected()


def test_all_credenitials_provided(setup_session_state):
    assert not all_credenitials_provided()
    st.session_state.auth = True
    st.session_state.role_selected = True
    assert all_credenitials_provided()


def test_init_login(setup_session_state):
    init_login()
    assert not st.session_state.auth
    assert not st.session_state.role_selected
    assert st.session_state.tarpit == 0


def test_cr_authenticate_success(requests_mock, setup_session_state):
    requests_mock.post(
        CR_AUTH_ENDPOINT, json={"authenticated": True, "roles": ["admin"]}
    )
    assert cr_authenticate("user", "pass")
    assert st.session_state.auth
    assert st.session_state.email == "user"
    assert st.session_state.available_roles == ["admin"]


def test_cr_authenticate_failure(requests_mock, setup_session_state):
    requests_mock.post(CR_AUTH_ENDPOINT, json={"authenticated": False})
    assert not cr_authenticate("user", "pass")
    assert not st.session_state.auth


def test_cr_authenticate_network_error(setup_session_state):
    with patch("requests.post", side_effect=requests.exceptions.ConnectionError):
        assert not cr_authenticate("user", "pass")
        assert not st.session_state.auth


def test_fake_authenticate(setup_session_state):
    with patch("time.sleep", return_value=None):
        assert fake_authenticate("user", "pass")
        assert st.session_state.auth
        assert st.session_state.email == "user"


def test_authenticate(setup_session_state):
    with patch("labs_toolkit.cr_auth.cr_authenticate", return_value=True):
        authenticate("user", "pass")
        assert not st.session_state.auth


def test_select_role(setup_session_state):
    select_role()
    assert st.session_state.role_selected


def test_show_role_selector(setup_session_state):
    with patch("streamlit.selectbox") as mock_selectbox:
        show_role_selector()
        assert mock_selectbox.called
